﻿using UnityEngine;
using System;
using GooglePlayGames;
using Commons;
using GooglePlayGames.BasicApi.Multiplayer;
using Assets.Scripts.Game;
using TicTacToeXAML;
using UnityEngine.SceneManagement;
using Assets.Scripts;
using Assets.Scripts.Common.Logging;

public class GamePlayService : MonoBehaviour {
	public static TurnBasedMatch match;
	public static Invitation[] invites;
	// Use this for initialization
	void Start () {    }
	void Update () {	}

    internal static void ShowAchievementsUI()
    {
        Social.ShowAchievementsUI();
    }

    internal static void ReportProgress(string achievementID)
    {
        if (ApplicationService.Instance.PlayGamesPlatformActivated)
        {
            Social.ReportProgress(achievementID, 100.0f, (bool success1) =>
            {
                Debug.Log("ReportProgress "+ achievementID +", res: " + success1);
            });        
		}
        else
            Debug.Log("Can not ReportProgress,  PlayGamesPlatformActivated = false");
    }
    
    internal static void Init()
    {
        if (ApplicationService.Instance.ReleaseInfo.ShopName != ReleaseInfo.Shop.Google)
            return;
        try
        {
			Debug.Log("PlayGamesPlatform Init!");
            ////// recommended for debugging:
            PlayGamesPlatform.DebugLogEnabled = true;
            // Activate the Google Play Games platform
            PlayGamesPlatform.Activate();

            // authenticate user:
            Social.localUser.Authenticate((bool success) =>
            {
                ApplicationService.Instance.PlayGamesPlatformActivated = success;
                Debug.Log("Result of Authenticate " + success);
				if(success)
				{
	                //PlayGamesPlatform.Instance.TurnBased.AcceptFromInbox(OnMatchStarted);
	                PlayGamesPlatform.Instance.RegisterInvitationDelegate(OnGotInvitation);
	                if(PlayGamesPlatform.Instance.TurnBased != null)
	                    PlayGamesPlatform.Instance.TurnBased.RegisterMatchDelegate(OnGotMatch);

					GetAllInvitations();

                    Debug.Log("Social.localUser.id =" + Social.localUser.id + ", name = " + Social.localUser.userName);

                }
            });
                
        }
        catch (Exception exc)
        {
            Debug.LogError(exc);

        }
    }
	public static EventHandler<EventArgs<Invitation, bool>> InvitationHandler;

	static void LoadGameScene (bool deserializeMatchAfterLoad)
	{
		GameSettings.Instance.Gametype = TicTacToeXAML.GameType.TwoPlayersPlayServices;
		if (deserializeMatchAfterLoad)
			ApplicationService.Instance.DeserializeMatchAfterLoad = true;
		ApplicationService.Instance.LoadLevel (1);

			
	}

	//It 's a 1st callback to send to the second player.
    protected static void OnGotInvitation(Invitation invitation, bool shouldAutoAccept)
    {
		Debug.Log("OnGotInvitation! Inviter.DisplayName = "+invitation.Inviter.DisplayName);
		Debug.Log ("shouldAutoAccept = " + shouldAutoAccept);
        if (invitation.InvitationType != Invitation.InvType.TurnBased)
        {
            Debug.Log("wrong type of invitation!");
            return;
        }

		bool acceptByCustomDialog = true;
		if (acceptByCustomDialog) 
		{

			NetworkGameProposal.Instance.OnHide += (object sender, EventArgs e) => {
				if (NetworkGameProposal.Instance.Accepted)
					PlayGamesPlatform.Instance.TurnBased.AcceptInvitation (invitation.InvitationId, OnMatchStarted);
			};
			NetworkGameProposal.Instance.Show (invitation);
		} 
		else 
		{
			if (SceneManager.GetActiveScene ().name == "MainMenu") {
				LoadGameScene(false);
			} 
			else if (InvitationHandler != null)//handled in GameManager - clears board
				InvitationHandler (null, new EventArgs<Invitation, bool> (invitation, shouldAutoAccept));
			
			PlayGamesPlatform.Instance.TurnBased.AcceptInvitation (invitation.InvitationId, OnMatchStarted);
		}
    }

    public static void InviteFriend()
    {
        Debug.Log("InviteFriend ...");
		try
		{
        	PlayGamesPlatform.Instance.TurnBased.CreateWithInvitationScreen(1, 1, 0, OnMatchStarted);
		}
		catch(Exception exc) 
		{
			Log.AddError(exc);
		}
    }

	static bool MoveBeingSend;
    public static bool CanPlay()
    {
        if (match == null)
            return false;
        // I can only make a move if the match is active and it's my turn!
        bool canPlay = (match.Status == TurnBasedMatch.MatchStatus.Active &&
            match.TurnStatus == TurnBasedMatch.MatchTurnStatus.MyTurn
			&& !MoveBeingSend);
        //Debug.Log("OnPlayTurn canPlay = " + canPlay);
        return canPlay;
    }

    public static void OnPlayTurn()
    {
		Debug.Log ("On play turn start");
        var canPlay = CanPlay();

        if (!canPlay)
            return;
		MoveBeingSend = true;
		byte[] myData = TicTacToeGrid.Instance.Serialize("OnPlayTurn");
        string whoIsNext = DecideWhoIsNext2p(match);
        Debug.Log("whoIsNext = "+ whoIsNext + ", match.SelfParticipantId = "+ match.SelfParticipantId);
        Debug.Log("calling PlayGamesPlatform.Instance.TurnBased.TakeTurn...");
        PlayGamesPlatform.Instance.TurnBased.TakeTurn(match, myData, whoIsNext, (bool success) =>
        {
            if (success)
            {
                Debug.Log("turn successfully submitted!");
                match = null;
				MoveBeingSend = false;
            }
            else
                Debug.Log("TakeTurn error :/");
        });
    }
    
	public static void OnGotMatch(TurnBasedMatch match, bool shouldAutoLaunch)
	{
		Debug.Log("OnGotMatch!!!!");
		HandleMatch (match, shouldAutoLaunch, false);
	}

	public static void HandleMatchOnGameScene(TurnBasedMatch match, bool shouldAutoLaunch, bool calledFromRematch)
	{
		if (true)//!calledFromRematch) 
		{
			Debug.Log ("HandleMatch calling Deserialize... TicTacToeGrid.Instance ="+TicTacToeGrid.Instance);
			if (TicTacToeGrid.Instance != null) 
			{
				var matchServiceEntity = TicTacToeGrid.Instance.Deserialize (match.Data, "HandleMatch shouldAutoLaunch = " + shouldAutoLaunch);
				// determine if I'm the 'X' or the 'O' player
				matchServiceEntity.ElemType = matchServiceEntity.GetMyMark (match.SelfParticipantId);
				Debug.Log ("HandleMatch matchServiceEntity.ElemType  = " + matchServiceEntity.ElemType);
			} else
				Debug.Log ("HandleMatch TicTacToeGrid.Instance == null! deserialization not called!!!!");
		} 
		// if the match is in the completed state, acknowledge it
		if (match.Status == TurnBasedMatch.MatchStatus.Complete) {
			Debug.Log ("sending AcknowledgeFinished...");
			PlayGamesPlatform.Instance.TurnBased.AcknowledgeFinished (match,
				(bool AcknowledgeFinished) => {

					Debug.LogError ("acknowledging match finish: " + AcknowledgeFinished);

				});
			//you lost
			if (SceneManager.GetActiveScene ().name != "MainMenu")
				TicTacToeGrid.Instance.EndGameIfShould (TicTacToeGrid.Instance.GetPlayerGameForNetworkGameEndCheck ());
		} 
	}

	static void HandleMatch(TurnBasedMatch match, bool shouldAutoLaunch, bool calledFromRematch, bool deserializeAfterLoad = false)
	{
		GamePlayService.match = match;
		MoveBeingSend = false;
		Debug.Log("HandleMatch checking GetActiveScene, match = "+match);
		if (SceneManager.GetActiveScene ().name == "MainMenu") 
		{
			Debug.Log ("HandleMatch Loading game scene...");
			LoadGameScene(deserializeAfterLoad);
		}
		else
			HandleMatchOnGameScene (match, shouldAutoLaunch, calledFromRematch);
		Debug.Log ("HandleMatch ends");
	}
	// Callback called when we agree to invitation (we accpted AcceptInvitation)
    static void OnMatchStarted(bool success, TurnBasedMatch match)
    {
		Debug.Log ("OnMatchStarted begins match = "+match);
		if (match == null)
			return;
		var canPlay = CanPlay();
		var dataDesc = match.Data == null ? "match.data = null" : "match.data Length = "+match.Data.Length;
		dataDesc += " canPlay = " + canPlay + " TicTacToeGrid.Instance = "+TicTacToeGrid.Instance;
        if (success)
        {
			Debug.Log("OnMatchStarted OK! " + dataDesc);
			HandleMatch (match, true, false, true);
        }
        else
        {
			Debug.Log("OnMatchStarted error :/ " + dataDesc);
        }
    }

    static string DecideWhoIsNext2p(TurnBasedMatch match)
    {
        if (match.AvailableAutomatchSlots > 0)
            return null;

        // who is my opponent?
        foreach (Participant p in match.Participants)
        {
            if (!p.ParticipantId.Equals(match.SelfParticipantId))
            {
                return p.ParticipantId;
            }
        }
        return null;
    }
	public static void DeclineInv(Invitation invite)
	{
		PlayGamesPlatform.Instance.TurnBased.DeclineInvitation (invite.InvitationId);
	}


	static void DeclineAllInv (Invitation[] invites)
	{
		Debug.Log ("PrintAllInvitations Got " + invites.Length + " invites");
		foreach (Invitation invite in invites) {
			DeclineInv (invite);
		}
	}

	static void PrintAllInv (Invitation[] invites)
	{
		Debug.Log ("PrintAllInvitations Got " + invites.Length + " invites");
		string logMessage = "";
		foreach (Invitation invite in invites) {
			logMessage += " " + invite.InvitationId + " (" + invite.InvitationType + ") from " + invite.Inviter + "\n";
		}
		Debug.Log (logMessage);
	}

	public static void DeleteAllInvitations()
	{
		PlayGamesPlatform.Instance.TurnBased.GetAllInvitations(
			(invites) =>
			{
				DeclineAllInv (invites);
				GetAllInvitations();
			});
	}

	static void OnGotRematch(bool accepted, TurnBasedMatch match)
	{
		Debug.Log ("OnGotRematch accepted = " + accepted);
		if (accepted) 
		{
			//OnGotMatch (match, true);
			HandleMatch (match, true, true, true);
			//if (SceneManager.GetActiveScene ().name == "MainMenu")
			ApplicationService.Instance.LoadLevel (1);
		}
	}


	internal static void Rematch()
	{
		PlayGamesPlatform.Instance.TurnBased.Rematch (match, OnGotRematch);
	}

	public static void GetAllInvitations()
	{
        if (PlayGamesPlatform.Instance == null || PlayGamesPlatform.Instance.TurnBased == null)
            return;
		 PlayGamesPlatform.Instance.TurnBased.GetAllInvitations(
        (invites) =>
        {
			GamePlayService.invites = invites;
			PrintAllInv (invites);
    	});
	}

	public static void ShowInvitations()
	{
		if(ApplicationService.Instance.PlayGamesPlatformActivated)
			PlayGamesPlatform.Instance.TurnBased.AcceptFromInbox (OnMatchStarted);
		else
			Debug.Log ("ShowInvitations - not active :/");
	}

	static string GetAdversaryParticipantId() {
		foreach (Participant p in match.Participants) {
			if (!p.ParticipantId.Equals(match.SelfParticipantId)) {
				return p.ParticipantId;
			}
		}
		Debug.LogError("Match has no adversary (bug)");
		return null;
	}


	public static void FinishMatch(GridElemType winner)
	{
		try{
		bool winnerIsMe = winner == TicTacToeGrid.Instance.MatchEntity.ElemType;
		// define the match's outcome
		MatchOutcome outcome = new MatchOutcome();
		outcome.SetParticipantResult(match.SelfParticipantId,
			winnerIsMe ? MatchOutcome.ParticipantResult.Win : MatchOutcome.ParticipantResult.Loss);
		outcome.SetParticipantResult(GetAdversaryParticipantId(),
			winnerIsMe ? MatchOutcome.ParticipantResult.Loss : MatchOutcome.ParticipantResult.Win);
		
		// finish the match
		PlayGamesPlatform.Instance.TurnBased.Finish(match, TicTacToeGrid.Instance.Serialize("gameEnd"), outcome, (bool success) => 
				{
			if (success) {
				Debug.Log("TurnBased.Finish ok");
			} else {
				Debug.Log("TurnBased.Finish error");
			}
			});

		}catch(Exception exc) {
			Debug.Log (exc);
		}
	}
}
