﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

//using System.Xml;

namespace CommonsPC.IO
{
    class XmlSerializeHelper
    {
        /// ---- SerializeAnObject -----------------------------
        /// <summary>
        /// Serializes an object to an XML string
        /// </summary>
        /// <param name="AnObject">The Object to serialize</param>
        /// <returns>XML string</returns>

        public static string SerializeAnObject<T>(T AnObject)
        {

            var xmlSerializer = new XmlSerializer(typeof(T));
            var xmlWriter = new StringWriter();

            xmlSerializer.Serialize(xmlWriter, AnObject);
            return xmlWriter.ToString();

        }

        public static T DeSerializeAnObject<T>(string XmlOfAnObject) where T : class
        {
            var StrReader = new StringReader(XmlOfAnObject);
            var Xml_Serializer = new XmlSerializer(typeof(T));
            var xmlReader = XmlReader.Create(new StringReader(XmlOfAnObject));
            try
            {
                T AnObject = Xml_Serializer.Deserialize(xmlReader) as T;
                return AnObject;
            }
            finally
            {
                xmlReader.Close();
                StrReader.Dispose();
            }
        }
    }
}
