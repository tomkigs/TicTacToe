﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Game.Core
{
    public struct Point 
    {
        public int X;
        public int Y;

        public Point(int x, int y)
        {
            X = x;
            Y = y;

        }

        static public bool operator ==(Point p1, Point p2)
        {
            return p1.X == p2.X && p1.Y == p2.Y;
        }

        static public bool operator !=(Point p1, Point p2)
        {
            return !(p1==p2);
        }


		public override int GetHashCode ()
		{
			return base.GetHashCode ();
		}
		public override bool Equals (object obj)
		{
			var secP = (Point)obj;
			return this == secP;
		}

		public override string ToString ()
		{
			return "{X:"+X+ " " + "Y:"+Y+"}";
		}

    }
}
