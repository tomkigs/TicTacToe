﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Common.GUI;
using System;
using System.Linq;
using Commons;
using Common;
using System.Collections.Generic;

namespace Commons
{
    public class Gui : MonoBehaviour
    {
        public static float DesignedWidth = 720;
        public static Gui Instance;

        protected bool levelFinished = false;
        protected TimeTracker trLevelFinished = new TimeTracker();
		List<BaseForm> forms = new List<BaseForm>();

        protected void OnAwake()
        {
            ApplicationService.Instance.EnsureDataConsistency();
            Instance = this;
            GetComponent<CanvasScaler>().scaleFactor = GetScaleFactor();
        }

        protected T InitForm<T>(string name) where T : BaseForm
        {
            var form = GameObject.Find(name);
            if (form == null)
                return null;
            var formC = form.GetComponent<T>();
            if(formC != null)
                formC.gameObject.SetActive(false);
			forms.Add(formC);
            return formC;
        }

        void Start()
        {
           
        }
        public static void RescaleTransform(Transform transform)
        {
            if (Screen.width < Gui.DesignedWidth)
            {
                transform.localScale = new Vector3(GetScaleFactor() * .9f, GetScaleFactor() * .9f, 1);
            }
        }

        public static float GetScaleFactor()
        {
            return Screen.width / DesignedWidth;
        }

        protected BaseForm shownForm;
        
        protected void ShowForm(BaseForm form)
        {
            //Tutorial.Instance.restoreTut = false;
            //if (Tutorial.Instance.gameObject.activeInHierarchy)
            //{
            //    Tutorial.Instance.restoreTut = true;
            //    Tutorial.Instance.gameObject.SetActive(false);
            //}
            form.gameObject.SetActive(true);
            form.Show();
            shownForm = form;
        }

		public virtual bool AnyFormShown(){return shownForm != null && shownForm.Visible && shownForm.gameObject.activeInHierarchy;}
    }
}

