﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Commons.Effects;

public class LineMessage : MonoBehaviour {
	Text txt;

	public static LineMessage Instance;
	// Use this for initialization
	void Start () {
		Instance = this;
		txt = GetComponent<Text> ();
	}

	public void Show(string text)
	{
		var col = this.txt.color;
		col.a = 255;
		this.txt.color = col;
		//AlphaChanger.ChangeAlpha(
		txt.text = text;
	}
	
	// Update is called once per frame
	void Update () {
		var col = this.txt.color;
		col.a -= 1f;
		this.txt.color = col;
	}
}
