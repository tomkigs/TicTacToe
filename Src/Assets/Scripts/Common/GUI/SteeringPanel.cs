﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


namespace Commons
{
	namespace GUI
	{
		public class SteeringPanel : MonoBehaviour {

			public static SteeringPanel Instance;
			public interface ITarget
			{
				Vector2 Direction { get; set; }
			}

			public ITarget Target;
			public Vector2 DefaultDirection = Vector2.zero;
			bool rightDown;
			bool upDown;
			bool leftDown;
			bool downDown;

			void Awake()
			{
				Instance = this;
			}
			void Start () {	}

			public bool IsAnyDown(){
				return Target != null && Target.Direction != Vector2.zero;//HACK
			}

			// Update is called once per frame
			void Update () 
			{
				if (Target == null)
					return;
				Vector2 direction = DefaultDirection;
				if(rightDown)
					direction.x = 1;
				else if(leftDown)
					direction.x = -1;
				if(upDown)
					direction.y = 1;
				else if(downDown)
					direction.y = -1;

				Target.Direction = direction;
			}

			public void OnUpDown()
			{
				upDown = true;
			}

			public void OnLeftDown()
			{
				leftDown = true;
			}
			public void OnRightDown()
			{
				rightDown = true;

			}
			public void OnRightUp()
			{
				rightDown = false;
			}

			public void OnLeftUp()
			{
				leftDown = false;
			}
			public void OnUpUp()
			{
				upDown = false;
			}

			public void OnDownDown()
			{
				downDown = true;
			}

			public void OnDownUp()
			{
				downDown = false;
			}
		}
	}
}
