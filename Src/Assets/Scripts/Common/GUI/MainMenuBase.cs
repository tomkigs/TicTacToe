﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Commons
{
	namespace GUI
	{
		public class MainMenuBase : MonoBehaviour {
			public string AppName;
            public string SlideMeAppName;
            protected Transform loading;
			// Use this for initialization
			protected void OnStart () { 
                //SetMusicSoundGuiState ();
                GetComponent<CanvasScaler>().scaleFactor = Gui.GetScaleFactor();
                //loading = GameObject.Find("loadingTxt").transform;
                //loading.gameObject.SetActive(false);
                if (GoogleMobileAdsDemoScript.Instance != null)
                {
                    GoogleMobileAdsDemoScript.Instance.ShowBanner();//buffer it
                    GoogleMobileAdsDemoScript.Instance.HideBanner();//Hide it, ready to show later
                }
			}

			// Update is called once per frame
			protected void OnUpdate () {
				if (Input.GetKeyDown (KeyCode.Escape)) 
				{
					Application.Quit();
				}
			}
			public void ShowLoading ()
			{
                if (loading != null) 
				{
                    //loading.GetComponent<Text>().text = "Loading...";
                    loading.gameObject.SetActive(true);
				}
			}

			public void OnNewGame() 
			{
				ShowLoading ();
				ApplicationService.Instance.LoadLevel (1);
			}

            public virtual void OnContinueGame()
            {
                ShowLoading();
                ApplicationService.Instance.ContinueGame();
            }
			
			public void OnMoreGames()
			{
                Debug.Log("OnMoreGames");
				#if UNITY_ANDROID
				if(ApplicationService.Instance.ReleaseInfo.ShopName == Commons.ReleaseInfo.Shop.Google)
					Application.OpenURL("https://play.google.com/store/apps/developer?id=Koto+Games");
				else if(ApplicationService.Instance.ReleaseInfo.ShopName == Commons.ReleaseInfo.Shop.SlideMe)
					Application.OpenURL("http://slideme.org/applications/kotogames");
				#elif UNITY_IPHONE || UNITY_EDITOR
				Application.OpenURL ("http://itunes.com/apps/bozenakot");
				#endif
			}
			
			public void OnRateMe()
			{
				#if UNITY_ANDROID
				var storePath = "";
				if (ApplicationService.Instance.ReleaseInfo.ShopName == Commons.ReleaseInfo.Shop.Google) 
					storePath = "https://play.google.com/store/apps/details?id=com.kotogames."+AppName;
				else
                    storePath = "http://slideme.org/application/" + SlideMeAppName;

				Application.OpenURL (storePath);
				#elif UNITY_IPHONE || UNITY_EDITOR
				Application.OpenURL("itms-apps://itunes.apple.com/app/id898541965");
				#endif
			}
			public void OnPause()
			{
			}
			/*
			public void OnSound()
			{
				AudioServiceScript.SoundOn = !AudioServiceScript.SoundOn;
				SetMusicSoundGuiState ();
			}
			public void OnMusic()
			{
				AudioServiceScriptBase.MusicOn = !AudioServiceScript.MusicOn;
				SetMusicSoundGuiState ();
			}

			void SetMusicSoundGuiState()
			{
				var music = transform.FindChild ("musicBtn").transform.FindChild ("off");
				music.gameObject.SetActive (!AudioServiceScriptBase.MusicOn);
				
				var snd = transform.FindChild ("soundBtn").transform.FindChild ("off");
				snd.gameObject.SetActive (!AudioServiceScript.SoundOn);
			}
			*/
		}
	}
}
