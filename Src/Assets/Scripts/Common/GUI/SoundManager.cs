﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Common.GUI
{
    class SoundManager : MonoBehaviour
    {
        GameObject musicOff;
        GameObject soundOff;

        void Awake()
        {
            Func<string, GameObject> getOff = (string parent) => { return transform.Find(parent).Find("off").gameObject; };

            musicOff = getOff("Music");
            SetMusicState();

            soundOff = getOff("Sound");
            SetSoundState();
        }

        private void SetSoundState()
        {
            soundOff.SetActive(!AudioService.SoundOn);
        }

        private void SetMusicState()
        {
            musicOff.SetActive(!AudioService.MusicOn);
        }

        public void SwitchMusic() 
        {
            AudioService.MusicOn = !AudioService.MusicOn;
            SetMusicState();
        }

        public void SwitchSound() 
        {
            AudioService.SoundOn = !AudioService.SoundOn;
            SetSoundState();
        }
    }
}
