﻿using UnityEngine;
using System.Collections;
using Commons;
using Commons.GUI;

namespace Common.GUI
{
    public class PauseBaseForm : BaseForm
    {
        public bool showFullScreenAds = false;
        protected bool FullEnginePause = false;
		TimeTracker tr = new TimeTracker();
       
        protected void OnAwake()
        {
            appearMode = AppearMode.Alpha;
			tr.Reset ();
        }

        protected void SetTitle(string titlePhase)
        {
            LanguageManager.SetChildText(this.gameObject, "title", titlePhase);
        }

        protected ApplicationService App
        {
            get { return ApplicationService.Instance; }
        }

        public override void Show()
        {
            //it sucks, but timeScale affects also Particle System which we want to work.
			tr.Reset ();
            if (FullEnginePause)
                App.Pause();
            App.GamePaused = true;
            base.Show();
            GoogleMobileAdsDemoScript.Instance.ShowBanner();
            if(showFullScreenAds)
                GoogleMobileAdsDemoScript.Instance.RequestInterstitial();
        }

        public override void Hide()
        {
			if (tr.TotalSeconds < 0.5)
				return;//accidental close ?
            Resume();
            base.Hide();
        }

        protected void Resume()
        {
            //GoogleMobileAdsDemoScript.Instance.HideBanner();
            App.GamePaused = false;
            if(FullEnginePause)
                App.Resume();
            //if (showFullScreenAds)
            //{
            //    GoogleMobileAdsDemoScript.Instance.ShowInterstitial();
            //}
        }

        public virtual void Reload()
        {
            App.ReloadLevel();
        }

        public void MainMenu()
        {
            Resume();
            App.ShowMainMenu();

        }

        public override void Update()
        {
            
            base.Update();
        }
    }
}
