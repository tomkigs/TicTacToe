﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Common.GUI;

public class MessageBox : BaseForm {
	Text txt;
	public static MessageBox Instance;
	public bool AutoScale;


	// Use this for initialization
	void Start () {
		Instance = this;
		txt = transform.Find("Content").GetComponent<Text> ();
		Hide ();

		disappearMode = AppearMode.Alpha;

	}

	public void Show(string text, bool hideInstantlyByAlpha = false)
	{
		if(AutoScale)
			SetScale();
		txt.text = text;
		base.Show ();
		if (hideInstantlyByAlpha)
			Invoke ("Hide", .75f);
	}
}
