﻿using UnityEngine;
using System.Collections;
using Commons;
using Zybex.GUI;
using System;

namespace Common.GUI
{
    public class BaseForm : MonoBehaviour
    {
        CanvasGroup canvasGroup;
		protected bool allowGoBack;
        public float AlphaIncrease = 0.05f;

        public bool Visible
        {
            get;
            private set;
        }
        public enum AppearMode { Normal, Alpha, Animation };
        protected AppearMode appearMode;
		protected AppearMode disappearMode;
        void Awake()
        {

        }

        public virtual void Show()
        {
            Visible = true;
			disappearing = false;
            gameObject.SetActive(true);
            if (appearMode == AppearMode.Alpha)
            {
                EnsureCanvasGroup();
                if (canvasGroup != null)
                    canvasGroup.alpha = 0 ;
            }
			if (disappearMode == AppearMode.Alpha) {
				EnsureCanvasGroup();
				if (canvasGroup != null)
					canvasGroup.alpha = 1;
			}
        }

        // Update is called once per frame
        public virtual void Update()
        {
            if (Visible && appearMode == AppearMode.Alpha)
            {
                EnsureCanvasGroup();
                if (canvasGroup != null && canvasGroup.alpha < 1)
                    canvasGroup.alpha += AlphaIncrease;
            }
			if (disappearing) {
				EnsureCanvasGroup();
				if (canvasGroup.alpha > 0)
					canvasGroup.alpha -= AlphaIncrease;
				else {
					Visible = false;
					gameObject.SetActive (false);
				}
			}
			if (allowGoBack && Input.GetKeyDown(KeyCode.Escape))
			{
				Hide();
			}
        }

        private void EnsureCanvasGroup()
        {
            if (canvasGroup == null)
                canvasGroup = GetComponent<CanvasGroup>();//can not be set in ctor :/
            Debug.Assert(canvasGroup != null);
        }
		bool disappearing = false;

		public EventHandler OnHide;
        public virtual void Hide()
        {
            Debug.Log("Hide "+this);
			Visible = false;
			Forms.TrSinceClosed.Reset ();
			if (disappearMode == AppearMode.Alpha) {
				disappearing = true;
				return;
			}
            if (Visible && appearMode == AppearMode.Alpha)
            {
                Debug.Assert(canvasGroup != null);
                if (canvasGroup != null)
                    canvasGroup.alpha = 0;
                
            }
            gameObject.SetActive(false);
			if (OnHide != null)
				OnHide (this, EventArgs.Empty);
        }
        protected void SetScale()
        {
            var ls = GetComponent<RectTransform>().localScale;
            var sf = Gui.GetScaleFactor();
            if (sf < 0.5)
                sf = 0.5f;
            ls.x = sf;
            ls.y = sf; ;
            Debug.Log("NewGameForm ls.x = " + ls.x);
            GetComponent<RectTransform>().localScale = ls;
        }
    }
}
