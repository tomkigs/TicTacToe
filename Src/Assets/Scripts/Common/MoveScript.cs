﻿using UnityEngine;

namespace Commons
{
	/// <summary>
	/// Simply moves the current game object
	/// </summary>
	public class MoveScript : MonoBehaviour
	{
		public Vector2 Speed = new Vector2(1, 1);
		public enum MoveType {Rigidbody2D, TransformDirection};
		Rigidbody2D rigid;
        public bool CanUseRigidBody = true;
		/// <summary>
		/// Moving direction
		/// </summary>
		//public Vector2 Direction = new Vector2(-1, 0);
		public MoveType moveType = MoveType.Rigidbody2D;

		void Start () {
			if (moveType == MoveType.Rigidbody2D)
				rigid = GetComponent<Rigidbody2D> ();
		}

		void FixedUpdate()
		{
            if (ApplicationService.Instance.GamePaused)
            {
                if (rigid != null)
                    rigid.velocity = new Vector2(0, 0);
                return;
            }
			var movement = new Vector2(Speed.x , Speed.y );
			//Debug.Log ("MoveScript movement ="+movement+ ", rigid.velocity ="+ rigid.velocity);
			if (moveType == MoveType.Rigidbody2D)
            {
                if (CanUseRigidBody && rigid.velocity != movement)
                    rigid.velocity = movement;
			}
			else
				transform.position += transform.TransformDirection (movement);


		}
	}
}