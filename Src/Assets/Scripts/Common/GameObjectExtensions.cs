﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Commons.Effects;

namespace Commons
{
	public static class GameObjectExtensions
	{
        
        public static void AddExplosionForce(this Rigidbody2D body, float explosionForce, Vector3 explosionPosition, float explosionRadius)
        {
            var dir = (body.transform.position - explosionPosition);
            float wearoff = 1 - (dir.magnitude / explosionRadius);
            var force = dir.normalized * explosionForce * wearoff;
            body.AddForce(force);
            Debug.Log("AddExplosionForce " + force);
        }

        public static void AddExplosionForce(this Rigidbody2D body, float explosionForce, Vector3 explosionPosition, float explosionRadius, float upliftModifier)
        {
            var dir = (body.transform.position - explosionPosition);
            float wearoff = 1 - (dir.magnitude / explosionRadius);
            Vector3 baseForce = dir.normalized * explosionForce * wearoff;
            body.AddForce(baseForce);

            float upliftWearoff = 1 - upliftModifier / explosionRadius;
            Vector3 upliftForce = Vector2.up * explosionForce * upliftWearoff;
            body.AddForce(upliftForce);
        }

        public static void ResetRotator(this Transform transform)
        {
            var r = transform.GetComponent<Rotator>();
            if (r != null)
            {
                r.enabled = false;
                var rot = transform.localRotation;
                rot.z = 0;
                rot.x = 0;
                rot.y = 0;
                transform.localRotation = rot;
            }
        }

		public static bool IsVisibleFrom(this Renderer renderer, Camera camera)
		{
			Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
			return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);
		}

		public static  void SetAlpha(this SpriteRenderer sr, float amount)
		{
			var col = sr.color;
			col.a = amount;
			sr.color = col;
		}

        public static bool IsChildActive(this Transform startFrom, string childName)
        {
            return startFrom.GetChildByName(childName).gameObject.activeInHierarchy;
        }

        public static void SetChildActivity(this Transform startFrom, string childName, bool visible)
        {
            startFrom.GetChildByName(childName).gameObject.SetActive(visible);
        }

        public static List<Transform> GetChildrenByNameStart(this Transform startFrom, string nameStart)
        {
            List<Transform> ret = new List<Transform>();
            for (int i = 0; i < startFrom.childCount; i++)
            {
                //Debug.Log("GetChildrenByNameStart i = " + startFrom.GetChild(i).name);
                if (startFrom.GetChild(i).name.StartsWith(nameStart))
                    ret.Add(startFrom.GetChild(i).transform);

                ret.AddRange(startFrom.GetChild(i).transform.GetChildrenByNameStart(nameStart));
            }
            return ret;
        }

        public static void MoveToXY(this Transform tr, float x, float y)
        {
            var pos = tr.position;
            pos.x = x;
            pos.y = y;
            tr.position = pos; 
        }
        public static void MoveTo(this Transform tr, float x, float y, float z)
        {
            var pos = tr.position;
            pos.x = x;
            pos.y = y;
            pos.z = z;
            tr.position = pos;
        }


        public static Transform GetChildByName(this Transform startFrom, string name)
		{
			Transform ret = null;
			//foreach (Transform trans in GetComponentsInChildren())
			for(int i=0; i< startFrom.childCount; i++)
			{
				//Debug.Log("GetChildByName i = "+startFrom.GetChild(i).name);
				if(name == startFrom.GetChild(i).name)
					return startFrom.GetChild(i);
				ret = GetChildByName(startFrom.GetChild(i), name);
				if(ret !=  null)
					return ret;
				
			}        
			return null;
		}
	}
}