﻿using UnityEngine;
using System.Collections;

namespace Commons
{
	public class Factory : MonoBehaviour {

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

        public static GameObject Instantiate(GameObject prefab, GameObject owner, float destroyAfterTime = 0)
        {
            return Instantiate(prefab, owner.transform.position, destroyAfterTime);
        }

        public static GameObject Instantiate(GameObject prefab, Vector3 position, float destroyAfterTime = 0)
        {
            var dest = Instantiate(prefab, position, Quaternion.identity) as GameObject;
            if (destroyAfterTime > 0)
                Destroy(dest, destroyAfterTime);
            return dest;
        }
	}
}
