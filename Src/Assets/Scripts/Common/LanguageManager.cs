﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Common
{
    class LanguageManager
    {
        public static void SetLanguage(string lang)
        {
            //Lean.LeanLocalization.GetInstance().SetLanguage(lang);
        }

        public static void SetText(string parentName, string phaseName)
        {
            SetChildText(GameObject.Find(parentName), "Text", phaseName);
        }

        public static void SetChildText(GameObject parent, string childName, string phaseName, string suffix = "")
        {
            var txt = parent.transform.Find(childName).gameObject;
            SetText(txt, phaseName, suffix);
        }

        public static void SetText(GameObject txt, string phaseName, string suffix = "")
        {
            //txt.GetComponent<Text>().text = Lean.LeanLocalization.GetTranslationText(phaseName) + suffix;
        }

        public static void SetText(Text txt, string phaseName, string suffix = "")
        {
            if (txt == null)
                return;
            //txt.GetComponent<Text>().text = Lean.LeanLocalization.GetTranslationText(phaseName) + suffix;
            var txtC = txt.GetComponent<Text>();
            if(txtC != null)
                txtC.text = phaseName + suffix;
        }
    }
}
