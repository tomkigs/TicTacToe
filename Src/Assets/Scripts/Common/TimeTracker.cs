﻿using UnityEngine;
using System.Collections;
using System;

namespace Commons
{
	public class TimeTracker  {
		DateTime start = DateTime.Now;
        double pausedSeconds;
        DateTime pauseStart = DateTime.MinValue;
		//bool TickBased;
        // Use this for initialization
        void Start () {
            

        }

        bool thisPaused = false;

        public void Update()
        {
            if (ApplicationService.Instance.GamePaused || thisPaused)
            {
                if (pauseStart == DateTime.MinValue)
                {
                    //pausedSeconds = 0;
                    pauseStart = DateTime.Now;
                }
            }
            else if (pauseStart != DateTime.MinValue)
            {
                pausedSeconds += (DateTime.Now - pauseStart).TotalSeconds;
                pauseStart = DateTime.MinValue;
            }
        }

        public string GetTotalTime()
        {
            //TimeSpan ts = Lasting();
            //return stopWatch.Elapsed.TotalMinutes.ToString() + "." + stopWatch.Elapsed.Seconds.ToString();
            //return String.Format("{0:00}:{1:00}:{2:00}",  ts.Hours, ts.Minutes, ts.Seconds);
			return String.Format("{0:00}:{1:00}:{2:00}",  TotalSeconds/3600f, TotalSeconds/60f, TotalSeconds%60);
        }

        internal void Pause()
        {
            thisPaused = true;
        }

        public void Reset ()
		{
			start = DateTime.Now;
			pausedSeconds = 0;
			pauseStart = DateTime.MinValue;
		}

		public double TotalSeconds
		{
			get
            {
                return (Lasting()).TotalSeconds - pausedSeconds;
            }
        }

        private TimeSpan Lasting()
        {
            return DateTime.Now - start;
        }

        public double Milliseconds
        {
            get
            {
                return TotalSeconds/1000;
            }
        }
    }
}
