﻿using System.Collections.Generic;
using System.Linq;

namespace CommonsPC.Leaderboards
{
    public class LiderboardSet
    {
        public string Name;
        public List<LiderboardEntity> Data = new List<LiderboardEntity>();

        internal bool ShouldAdd(LiderboardEntity entity)
        {
			if (Data.Contains (entity))
				return false;
            if (Data.Any() && Data.Count >= 10)
            {
                var maxScore = Data.Max(i => i.Score);
                if (maxScore > entity.Score)
                    return true;
                return false;
            }

            return true;
        }

        internal void Reorder()
        {
            Data = Data.OrderBy(i => i.Score).ToList();
        }

        internal void RemoveAboutLimit()
        {
            for (int i = Data.Count - 1; i > 9; i--)
            {
                Data.RemoveAt(i);
            }
        }

        internal bool Contains(LiderboardEntity liderboardEntity)
        {
            return Data.Any(i => i.Equals(liderboardEntity));
        }
    }

    public class LiderboardEntity
    {
        public string Name = "";
        public string ScoreDisplay;
        public float Score;
        public string SetName;
        public string CreatedAt = "";
        public int Platform;
        public string Extra;
        public int NumberOfMoves;//hack

        public override bool Equals(object obj)
        {
            if (obj is LiderboardEntity)
            {
                var other = obj as LiderboardEntity;
                if (other.GetHash() == GetHash())
                    return true;
            }
            return base.Equals(obj);
        }

        string GetHash()
        {
            return Name + "-" + Score.ToString() + "-" + CreatedAt + "-" + SetName + "-" + Platform;
        }

        public override int GetHashCode()
        {
            return GetHash().GetHashCode();
        }

		public override string ToString ()
		{
			return Name + " "+Score;
		}
    }
}
