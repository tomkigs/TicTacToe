﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonsPC.IO;
using Commons;
using Assets.Scripts.Common.Logging;
//using TicTacToe.Android.kotogames.com;
//using Commons.KotoG;
//using CommonsPC.ServiceReference1;
//using CommonsPC.com.kotogames;
#if WINDOWS_8

#elif ANDROID
using TicTacToe.Android.kotogames.com;
#endif
namespace CommonsPC.Leaderboards
{
    class RealLiderboardDataProvider : ILiderboardManagerDataProvider
    {
		#if WINDOWS_8
        private WebService client;
        #elif WINDOWS_PHONE
        //private WebService client;
		#else
		//SimpleService client;
		#endif
        public string GameName { get; set; }

        public RealLiderboardDataProvider()
        {
			#if WINDOWS_8
            client = new WebService();
#elif WINDOWS_PHONE
           // client = new WebService();
#else
			//client = new SimpleService();

#endif
        }
        string GetLeaderFileFullPath(string name)
        {
            return GameName + "/" + name + ".xml";
        }
        public LiderboardSet GetSet(string name)
        {
			var str = "";
			var fn = GetLeaderFileFullPath (name);
			#if WINDOWS_8
			str = client.GetLevelContent(fn);
            //Thread.Sleep(50);  
#elif WINDOWS_PHONE
            //str = client.GetLevelContent(fn);
#else
			for(int i=0;i<3;i++)
            {
			    try
			    {
				   // str = client.GetFileContent(fn);
                    break;
			    }
			    catch(Exception exc)
			    {
				    Log.AddError(exc);
					System.Threading.Thread.Sleep(50);

			    }
                 
            }
#endif
            if (str.Length > 0)
                return XmlSerializeHelper.DeSerializeAnObject<LiderboardSet>(str);

            return null;
        }

        public void Save(string name, LiderboardSet set)
        {
            var str = XmlSerializeHelper.SerializeAnObject(set);
            var path = GetLeaderFileFullPath(set.Name);
            #if WINDOWS_8
           	client.SetFileContent(path, str);
#endif
        }
    }
}
