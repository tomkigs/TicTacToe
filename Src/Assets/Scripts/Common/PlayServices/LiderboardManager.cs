﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Commons;
using System.Threading;
using UnityEngine;
using Assets.Scripts.Common.Logging;
using CommonsPC.Leaderboards;
using TicTacToeXAML;

namespace CommonsPC
{
  public interface ILiderboardManagerDataProvider
  {
    LiderboardSet GetSet(string name);
    void Save(string name, LiderboardSet set);
    string GameName { get; set; }
  }

  public class LiderboardManager
  {
    public static LiderboardManager instance;
    private ILiderboardManagerDataProvider dataProvider = new RealLiderboardDataProvider();
    //private ILiderboardManagerDataProvider dataProvider = new LiderboardManagerFake();
    Dictionary<string, LiderboardSet> sets = new Dictionary<string, LiderboardSet>();
    private IEnumerable<string> setNames;

    public static LiderboardManager Instance
    {
      get
      {
        if (instance == null)
          instance = new LiderboardManager(new string[] { GameDefines.ModeEasy, GameDefines.ModeNormal }, "TicTacToe");
        return instance;
      }
    }

    public string GameName
    {
      //get { return dataProvider.GameName; }
      set { dataProvider.GameName = value; }
    }

    public LiderboardManager(IEnumerable<string> setNames, string gameName)
    {
      this.setNames = setNames;
      GameName = gameName;

      //GameBase.Instance.CreateFakeLevel(1);
    }

    public void ReloadAsync()
    {
      Thread oThread = new Thread(new ThreadStart(Reload));

      // Start the thread
      oThread.Start();
    }
    public bool EverLoaded;
    bool active = false;//TODO new web page
    public void Reload()
    {
      try
      {
        if (!active)
          return;
        sets.Clear();
        foreach (var name in setNames)
        {
          sets[name] = GetSet(name);
        }
        EverLoaded = true;
      }
      catch (Exception exc)
      {
        Debug.LogError(exc.Message);
      }
    }
    static object lockSet = new object();
    public LiderboardSet GetSet(string name)
    {
      //Debug.Log("GetSet "+ name + " starts");
      if (Monitor.TryEnter(lockSet, 200))
      {
        try
        {
          if (sets.ContainsKey(name))
            return sets[name];
          var set = dataProvider.GetSet(name);//get data from net

          if (set == null)
          {
            //set = new LiderboardSet() { Name = name };
            return null;
          }
          set.Reorder();

          sets[name] = set;
          //Debug.Log("GetSet " + name + " set = "+ set.Data.Count);
          return set;
        }
        finally
        {
          Monitor.Exit(lockSet);
        }
      }

      return null;
    }
    public LiderboardSet GetSet(bool normal)
    {
      var name = normal ? GameDefines.ModeNormal : GameDefines.ModeEasy;
      return GetSet(name);
    }
    public bool ShouldAddToList(LiderboardEntity entity)
    {
      if (entity == null)
        return false;
      var set = GetSet(entity.SetName);
      if (set != null)
      {
        return set.ShouldAdd(entity);

      }
      return false;
    }
    public LiderboardEntity CreateEntity(float gameTime, string displayScore, string setName)
    {
      var now = DateTime.Now;
      return new LiderboardEntity()
      {
        Score = gameTime,
        ScoreDisplay = displayScore,
        SetName = setName,
        //CreatedAt = now.Year.ToString()+(now.Month < 10 ? "0"+now.Month.ToString() : now.Month.ToString())+now.Day+now.Hour+now.Minute,
        CreatedAt = now.ToString("s")
      };//HACK todo!
    }
    public enum Platform { Windows8, Android };
    public void AddToList(LiderboardEntity liderboardEntity)
    {
      try
      {
        var set = GetSet(liderboardEntity.SetName);
        if (set != null)
        {
          if (!set.Contains(liderboardEntity))
          {
            set.Data.Insert(0, liderboardEntity);
            set.Reorder();
            set.RemoveAboutLimit();
            dataProvider.Save(set.Name, set);
          }
        }
      }
      catch (Exception exc)
      {
        Log.AddError(exc);
      }
    }
  }


}
