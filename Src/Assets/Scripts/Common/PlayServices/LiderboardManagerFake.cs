﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonsPC.IO;
using Assets.Scripts.Common.Logging;

namespace CommonsPC.Leaderboards
{
    public class LiderboardManagerFake : ILiderboardManagerDataProvider
    {
        public static int ch = 0;


        public void Save(string name, LiderboardSet set)
        {
            ConvToString(set);
        }
        public string GameName { get; set; }
        private static string ConvToString(LiderboardSet set)
        {
            var setString = XmlSerializeHelper.SerializeAnObject(set);
            Log.AddInfo(setString);
            return setString;
        }

        public LiderboardSet GetSet(string name)
        {
            if (ch == 0)
                ch++;
            var set = new LiderboardSet();
            set.Name = name;
            for (int i = 0; i < 10; i++)
            {
                var val = (int)(RandHelper.GetRandomDouble() * 10 + i);
                
				set.Data.Add(new LiderboardEntity() { Name = "player "+name, Score = val, ScoreDisplay = val.ToString(), SetName = name, CreatedAt = "20140627" });
                if (i == 2)
                {
                    set.Data[1].Name = "Jerzy brzeczyszczykiewicz";
                }
            }
            var ser = ConvToString(set);

            return XmlSerializeHelper.DeSerializeAnObject<LiderboardSet>(ser);
        }
    }
}
