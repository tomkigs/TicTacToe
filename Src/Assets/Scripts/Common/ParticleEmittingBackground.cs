﻿using UnityEngine;
using System.Collections;

namespace Common
{
	public class ParticleEmittingBackground : MonoBehaviour {
		//ParticleSystem par;
		float xDistToHero;
		public GameObject hero;

		void Start () {
			//par = this.GetComponent<ParticleSystem> ();
			//Debug.Log ("par = "+par);
			if(hero != null)
				xDistToHero = transform.position.x - hero.transform.position.x;
            //			Debug.Log ("init xDistToHero = "+xDistToHero);
		}

		// Update is called once per frame
		void Update () {
			if (hero == null)
				return;
			var xCurrDistToHero = transform.position.x - hero.transform.position.x;
			//Debug.Log ("xCurrDistToHero = "+xCurrDistToHero);
			transform.position = new Vector2(transform.position.x + xDistToHero - xCurrDistToHero, transform.position.y);
		}
	}
}
