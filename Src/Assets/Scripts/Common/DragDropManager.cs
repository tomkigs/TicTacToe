﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Lean;
using Commons;
using System;

namespace Commons
{
    public class DragDropManager : MonoBehaviour
    {
        public interface IOwner
        {
            bool CanBeDragged(GameObject obj);
        }

        public IOwner owner;
        public string[] objectsTags;
        List<GameObject> objects = new List<GameObject>();
        GameObject dragged;
        
        Vector3 dragValue;
        public EventHandler<EventArgs<GameObject>> OnObjectDragged;
        public EventHandler<EventArgs<GameObject>> OnObjectDraggEnded;

        public GameObject Dragged { get { return dragged; } }

        void Start()
        {

            foreach (var tag in objectsTags)
            {
                if (string.IsNullOrEmpty(tag))
                    continue;
                var objs = GameObject.FindGameObjectsWithTag(tag).ToList();
                objs.ForEach(i =>
                {
                    if (i.GetComponent<Collider2D>() == null)
                        Debug.LogError("component " + i.name + " has no Collider2D!");
                });
                objects.AddRange(objs);
            }

        }
        public void AddDragable(GameObject obj)
        {
            objects.Add(obj);
        }

        protected virtual void OnEnable()
        {
            LeanTouch.OnFingerDown += OnFingerDown;
            LeanTouch.OnFingerUp += OnFingerUp;
            LeanTouch.OnDrag += OnDrag;
        }

        protected virtual void OnDisable()
        {
            LeanTouch.OnDrag -= OnDrag;
            LeanTouch.OnFingerDown -= OnFingerDown;
        }
        private void OnFingerUp(LeanFinger finger)
        {
            //Debug.Log("mgr OnFingerUp dragged = " + dragged);
            if (dragged != null && OnObjectDraggEnded != null)
                OnObjectDraggEnded(this, new EventArgs<GameObject>(dragged));
        }


        private void OnFingerDown(LeanFinger finger)
        {
            var sp = Camera.main.ScreenToWorldPoint(finger.ScreenPosition);
            //Debug.Log("OnFingerDown " + sp);
            Vector2 touchPos = new Vector2(sp.x, sp.y);
            dragged = null;
            foreach (var i in objects)
            {
                if (owner != null)
                {
                    if (!owner.CanBeDragged(i))
                        continue;
                }
                if (i.GetComponent<Collider2D>() == Physics2D.OverlapPoint(touchPos))
                {
                    //Debug.Log("OverlapPoint !  ");
                    //balls.ForEach(i => i.isDragged = false);
                    dragged = i;
                    break;
                }
            }
            //Debug.Log("OnFingerDown end  dragged = " + dragged);
        }

        public void OnDrag(Vector2 pixels)
        {
            if (dragged == null)
                return;
            if (owner != null)
            {
                if (!owner.CanBeDragged(dragged))
                    return;
            }
            if (LeanTouch.Fingers.Any())
            {
                dragValue = CameraHelper.Instance.GetWorldDelta(pixels);
                dragValue.z = 0;
                //Debug.Log("ball moved pixels " + pixels + ", world dragValue = " + dragValue);
                dragged.transform.position = dragged.transform.position.MoveBy(dragValue);
                if (OnObjectDragged != null)
                    OnObjectDragged(this, new EventArgs<GameObject>(dragged));

                //Debug.Log("pos aft = " + dragged.transform.position);
            }

        }
        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Camera.main.transform.position = Camera.main.transform.position - new Vector3(1, 0, 0);
            }
        }
    }
}