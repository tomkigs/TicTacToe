﻿using UnityEngine;
using System.Collections;
using System;
using Common;

public interface Path {
	float Speed { get; set; }
	float TimeOffset { get; set; }
	void Reset();
	Vector2 GetNextStep();
}

public abstract class PathBase : Path
{
	protected float speed = 0f;
	public float Speed { get {return speed; } set { speed = value; } }
	public float TimeOffset { get; set; }
	public abstract void Reset ();
	public abstract Vector2 GetNextStep ();
}

public class XYToRTConverter
{
	public static float GetR(float x, float y)
	{
		return Mathf.Sqrt (x * x + y * y);
	}

	public static float GetT(float x, float y)
	{
		if (x == 0)
			return y > 0 ? Mathf.PI / 2 : -Mathf.PI / 2;
		return Mathf.Atan (y / x);
	}

	public static float GetX(float r, float t)
	{
		return r * Mathf.Cos (t);
	}

	public static float GetY(float r, float t)
	{
		return r * Mathf.Sin (t);
	}
}

public class CirclePath : PathBase {
	private float r = .05f;
	public float CurrentTime { get; set; }
	public float Radius { get { return r; } set { r = value; } }
	public Vector2 PositionOffset { get; set; }
	
	public override void Reset()
	{
		CurrentTime = TimeOffset;
	}

	public override Vector2 GetNextStep()
	{
		Vector2 result = new Vector2 (XYToRTConverter.GetX (Radius, CurrentTime) + PositionOffset.x, XYToRTConverter.GetY (Radius, CurrentTime) + PositionOffset.y);
		//Debug.Log ("Vec=" + result);
		CurrentTime += Speed;
		return result;
	}
}

public class WavePath : PathBase {
	private float amp = 0.1f;
	private float phase = 0.1f;

	public float CurrentTime { get; set; }
	public float Amplitude { get { return amp; } set { amp = value; } }
	public float Phase { get { return phase; } set { phase = value; } }

	public override void Reset()
	{
		CurrentTime = TimeOffset;
	}
	
	public override Vector2 GetNextStep()
	{
		Vector2 result = new Vector2 (Speed, Amplitude * Mathf.Sin (CurrentTime + Phase));
	
		//Debug.Log ("WavePath Vec=" + result);
		CurrentTime += Speed;
		return result;
	}
}

public class TrianglePath : PathBase {
	private float amp = 0.01f;
	private float y = 0.0f;
	private float sign = 1.0f;  
	private int step = 0;
	
	public float Amplitude { get { return amp; } set { amp = value; } }
	
	public override void Reset()
	{
		step = (int)TimeOffset;
	}
	
	public override Vector2 GetNextStep()
	{
		step += 1;
		if (step % 200 == 0) {
			sign = -sign;
			step = 0;
		}
		y = sign*amp;
		Vector2 result = new Vector2 (speed, y);
		//Debug.Log ("Vec=" + result);
		return result;
	}
}

public class AttackPath : PathBase
{
	private float currentTime;
	public GameObject Hero { get; set;}
	public GameObject Me { get; set;}
    public Action OnActivate;
    bool onActivateCalled;
    public bool AttackIgnoringDistance;

	public override void Reset()
	{
		currentTime = TimeOffset;
	}

	public override Vector2 GetNextStep()
	{
       
        if (Hero == null)
            return Defines.Vector2Zero;
        if (!AttackIgnoringDistance && currentTime < 0)
		{
			//Debug.Log ("Current Time = "+currentTime);
			currentTime += Mathf.Abs(Speed);
            return Defines.Vector2Zero;
		}

        if (OnActivate != null && !onActivateCalled)
        {
            onActivateCalled = true;
            OnActivate();
        }
        
		var obj1Pos = Hero.transform.position.x > Me.transform.position.x ? Hero.transform.position : Me.transform.position;
		var obj2Pos = Hero.transform.position.x > Me.transform.position.x ? Me.transform.position : Hero.transform.position;
		var step = Hero.transform.position.x > Me.transform.position.x ? Speed : -Speed;
		var x = obj1Pos.x - obj2Pos.x;
		var y = obj1Pos.y - obj2Pos.y;
		var r = XYToRTConverter.GetR (x, y) + step;
		var t = XYToRTConverter.GetT (x, y);
		var newX = XYToRTConverter.GetX (r, t) + obj1Pos.x;
		var newY = XYToRTConverter.GetY (r, t) + obj1Pos.y;
		var result = new Vector2 (newX - x, newY - y);
		result.x -= obj1Pos.x;
		result.y -= obj1Pos.y;
		//Debug.Log ("Hero at:"+Hero.transform.position+", Enemy at:"+Me.transform.position +", New Offset = "+result);
		return result;
	}
}

public class CombinedPath : PathBase
{
	public PathBase[] Paths { get; set; }

	public override void Reset()
	{
		if (Paths == null)
			return;
		foreach (var path in Paths)
			path.Reset();
	}
	
	public override Vector2 GetNextStep()
	{
		if (Paths == null)
			return new Vector2();
		Vector2 result = new Vector2 ();
		foreach (var path in Paths) {
			var step = path.GetNextStep();
			result.x += step.x;
			result.y += step.y;
		}
		//Debug.Log ("Combined=" + result);
		return result;
	}
}