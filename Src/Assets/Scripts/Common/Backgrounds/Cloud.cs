﻿using UnityEngine;
using System.Collections;
using Commons;

public class Cloud : MonoBehaviour {
	TimeTracker trLastSet = new TimeTracker();
	public bool fitCamera = false;
	// Use this for initialization
	void Start () {
		SetRandomInitPos ();
	}

	void SetRandomInitPos()
	{
		//var rect = Camera.main.rect;
	}

	void PositionBeforeCamera ()
	{
		var posRight = Camera.main.transform.Find("right").transform.position;
		posRight.z = this.transform.position.z;
		posRight.y = this.transform.position.y;
		this.transform.position = posRight;
		trLastSet.Reset ();
	}

	// Update is called once per frame
	void Update () {
		if (fitCamera && !GetComponent<Renderer>().IsVisibleFrom (Camera.main) && trLastSet.TotalSeconds > 3) 
		{
			PositionBeforeCamera ();
			
		}
	}
}
