﻿using UnityEngine;
using System.Collections;

public class CloudsEmitter : MonoBehaviour {
	ParticleSystem par;
	float xDistToHero;
	public GameObject car;
	// Use this for initialization
	void Start () {
		par = this.GetComponent<ParticleSystem> ();
		Debug.Log ("par = "+par);
		xDistToHero = transform.position.x - car.transform.position.x;
		Debug.Log ("init xDistToHero = "+xDistToHero);
	}

	// Update is called once per frame
	void Update () {
		if (car == null)
		    return;
		var xCurrDistToHero = transform.position.x - car.transform.position.x;
		//Debug.Log ("xCurrDistToHero = "+xCurrDistToHero);
		transform.position = new Vector2(transform.position.x + xDistToHero - xCurrDistToHero, transform.position.y);
	}
}
