using System;
using UnityEngine;
using GoogleMobileAds;
using GoogleMobileAds.Api;
using Commons;

// Example script showing how to invoke the Google Mobile Ads Unity plugin.
public class GoogleMobileAdsDemoScript : MonoBehaviour
{
  public bool ShowGUI = true;
  private static BannerView bannerView;//create it only once
  private static InterstitialAd interstitial;
  public static GoogleMobileAdsDemoScript instance;
  public static bool TagForChildDirectedTreatment;

  public static GoogleMobileAdsDemoScript Instance
  {
    get
    {
      //MonoBehaviour can not be created by code
      //if (instance == null)
      //{
      //    Debug.Log("GoogleMobileAdsDemoScript create Instance");
      //    instance = new GoogleMobileAdsDemoScript();
      //}
      return instance;
    }
  }
  void Awake()
  {
    instance = this;
    if (trTotalGameLasting == null)
      trTotalGameLasting = new TimeTracker();
  }

  public void ShowBanner()
  {
    Debug.Log("ShowBanner");
    if (bannerView == null)
      RequestBanner();
    else
      bannerView.Show();
  }

  void RequestBanner()
  {
#if UNITY_EDITOR
    string adUnitId = "unused";
#elif UNITY_ANDROID
		string adUnitId = "ca-app-pub-6468049049488510/3253679583";
#elif UNITY_IPHONE
		string adUnitId = "ca-app-pub-6468049049488510/2490977583";
#else
        string adUnitId = "unexpected_platform";
#endif

    // Create a 320x50 banner at the top of the screen.
    bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Top);

    // Register for ad events.
    bannerView.AdLoaded += HandleAdLoaded;
    bannerView.AdFailedToLoad += HandleAdFailedToLoad;
    //bannerView.Open += HandleAdOpened;
    //bannerView.OnAdClosing += HandleAdClosing;
    bannerView.AdClosed += HandleAdClosed;
    //bannerView.OnAdLeftApplication += HandleAdLeftApplication;
    // Load a banner ad.
    bannerView.LoadAd(createAdRequest());

  }

  public void RequestInterstitial()
  {
    try
    {
#if UNITY_EDITOR
      string adUnitId = "unused";
#elif UNITY_ANDROID
			    string adUnitId = "ca-app-pub-6468049049488510/7676873587";
#elif UNITY_IPHONE
		string adUnitId = "ca-app-pub-6468049049488510/2572287182";
#else
               string adUnitId = "unexpected_platform";
#endif

      // Create an interstitial.
      if (interstitial == null)
      {
        interstitial = new InterstitialAd(adUnitId);
        // Register for ad events.
        interstitial.AdLoaded += HandleInterstitialLoaded;
        interstitial.AdFailedToLoad += HandleInterstitialFailedToLoad;
        //interstitial.AdOpening += HandleInterstitialOpened;
        //interstitial.OnAdClosing += HandleInterstitialClosing;
        interstitial.AdClosed += HandleInterstitialClosed;
        //interstitial.OnAdLeftApplication += HandleInterstitialLeftApplication;
      }
      // Load an interstitial ad.
      interstitial.LoadAd(createAdRequest());
      Debug.Log("RequestInterstitial ends");
    }
    catch (Exception exc)
    {
      Debug.Log(exc.Message);
    }
  }

  // Returns an ad request with custom ad targeting.
  private AdRequest createAdRequest()
  {
    showLog = "createAdRequest TagForChildDirectedTreatment = " + TagForChildDirectedTreatment;
    return new AdRequest.Builder()
            .AddTestDevice(AdRequest.TestDeviceSimulator)
            .AddTestDevice("0123456789ABCDEF0123456789ABCDEF")
            //.AddKeyword("game")
            //.SetGender(Gender.Male)
            //.SetBirthday(new DateTime(1985, 1, 1))
            //.TagForChildDirectedTreatment(TagForChildDirectedTreatment)
            .AddExtra("color_bg", "9B30FF")
            .Build();

  }
  static DateTime? lastShowed;

  string showLog = "start";
  public static bool FullEverShown;
  static TimeTracker trTotalGameLasting;
  public void ShowInterstitial()
  {

    try
    {
      if (trTotalGameLasting.TotalSeconds < 200)
        return;
      Debug.Log("ShowInterstitial starts interstitial = " + interstitial);
      //if (InterstitialOpened != null)
      //    InterstitialOpened(this, EventArgs.Empty);
      if (lastShowed != null)
      {
        var diff = (DateTime.Now - lastShowed.Value).TotalSeconds;
        if (FullEverShown && diff < 300)//300
        {
          showLog = "ShowInterstitial ret! diff = " + diff;
          Debug.Log(showLog);
          return;
        }
        Debug.Log("ShowInterstitial diff  OK, showing if loaded... ");
        //Log.Add(showLog, 5); 
      }
      //return;
      lastShowed = DateTime.Now;
      if (interstitial.IsLoaded())
      {
        FullEverShown = true;
        if (InterstitialOpening != null)
          InterstitialOpening(this, EventArgs.Empty);
        interstitial.Show();
        //RequestInterstitial();
        showLog += ", shown!";
      }
      else
      {
        var message = "Interstitial is not ready yet.";
        Debug.Log(message);
      }
    }
    catch (Exception exc)
    {
      //Log.Add(exc);
      Debug.Log("ShowInterstitial " + exc.Message);
    }
  }

  public void HideBanner()
  {

    if (bannerView != null)
      bannerView.Hide();
  }

  #region Banner callback handlers

  public void HandleAdLoaded(object sender, EventArgs args)
  {
    print("HandleAdLoaded event received.");
  }

  public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
  {
    print("HandleFailedToReceiveAd event received with message: " + args.Message);
  }

  public void HandleAdOpened(object sender, EventArgs args)
  {
    print("HandleAdOpened event received");
  }

  void HandleAdClosing(object sender, EventArgs args)
  {
    print("HandleAdClosing event received");
  }

  public void HandleAdClosed(object sender, EventArgs args)
  {
    print("HandleAdClosed event received");
  }

  public void HandleAdLeftApplication(object sender, EventArgs args)
  {
    print("HandleAdLeftApplication event received");
  }

  #endregion

  #region Interstitial callback handlers

  public void HandleInterstitialLoaded(object sender, EventArgs args)
  {
    print("HandleInterstitialLoaded event received. interstitial = " + interstitial);
  }

  public void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
  {
    print("HandleInterstitialFailedToLoad event received with message: " + args.Message);
  }
  public EventHandler InterstitialOpened;
  public EventHandler InterstitialOpening;
  public void HandleInterstitialOpened(object sender, EventArgs args)
  {
    print("HandleInterstitialOpened event received");
    if (InterstitialOpened != null)
      InterstitialOpened(this, EventArgs.Empty);

  }

  void HandleInterstitialClosing(object sender, EventArgs args)
  {
    print("HandleInterstitialClosing event received");
  }

  public void HandleInterstitialClosed(object sender, EventArgs args)
  {
    print("HandleInterstitialClosed event received");
    RequestInterstitial();
  }

  public void HandleInterstitialLeftApplication(object sender, EventArgs args)
  {
    print("HandleInterstitialLeftApplication event received");
  }
  void OnGUI()
  {
    //Log.OnGUI ();
    //GUI.Label (new Rect (10, 5, 1000, 20), showLog);
    if (!ShowGUI)
      return;
    // Puts some basic buttons onto the screen.
    GUI.skin.button.fontSize = (int)(0.05f * Screen.height);

    Rect requestBannerRect = new Rect(0.1f * Screen.width, 0.05f * Screen.height,
                                      0.8f * Screen.width, 0.1f * Screen.height);
    if (GUI.Button(requestBannerRect, "Request Banner"))
    {
      RequestBanner();
    }

    Rect showBannerRect = new Rect(0.1f * Screen.width, 0.175f * Screen.height,
                                   0.8f * Screen.width, 0.1f * Screen.height);
    if (GUI.Button(showBannerRect, "Show Banner"))
    {
      bannerView.Show();
    }

    Rect hideBannerRect = new Rect(0.1f * Screen.width, 0.3f * Screen.height,
                                   0.8f * Screen.width, 0.1f * Screen.height);
    if (GUI.Button(hideBannerRect, "Hide Banner"))
    {
      bannerView.Hide();
    }

    Rect destroyBannerRect = new Rect(0.1f * Screen.width, 0.425f * Screen.height,
                                      0.8f * Screen.width, 0.1f * Screen.height);
    if (GUI.Button(destroyBannerRect, "Destroy Banner"))
    {
      bannerView.Destroy();
    }

    Rect requestInterstitialRect = new Rect(0.1f * Screen.width, 0.55f * Screen.height,
                                            0.8f * Screen.width, 0.1f * Screen.height);
    if (GUI.Button(requestInterstitialRect, "Request Interstitial"))
    {
      RequestInterstitial();
    }

    Rect showInterstitialRect = new Rect(0.1f * Screen.width, 0.675f * Screen.height,
                                         0.8f * Screen.width, 0.1f * Screen.height);
    if (GUI.Button(showInterstitialRect, "Show Interstitial"))
    {
      ShowInterstitial();
    }

    Rect destroyInterstitialRect = new Rect(0.1f * Screen.width, 0.8f * Screen.height,
                                            0.8f * Screen.width, 0.1f * Screen.height);
    if (GUI.Button(destroyInterstitialRect, "Destroy Interstitial"))
    {
      interstitial.Destroy();
    }
  }

  #endregion
}
