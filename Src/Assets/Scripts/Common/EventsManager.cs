﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Commons;

namespace Commons
{
	namespace Events
	{
		public class EventsManager : MonoBehaviour
		{
			private Dictionary<int, List<EventHandler<EventArgs>>> listeners = new Dictionary<int, List<EventHandler<EventArgs>>> ();
	
			public static EventsManager Instance { 
				get;
				private set; 
			}
			public void OnAwake()
			{
				Instance = this;
				Debug.Log (" EventsManager awake");
			}

			public void AddListener (int eventType, EventHandler<EventArgs> handler)
			{
				//List of listeners for this event
				List<EventHandler<EventArgs>> listenList = null;
		
				// Check existing event type key. If exists, add to list
				if (listeners.TryGetValue (eventType, out listenList)) {
					//List exists, so add new item
					listenList.Add (handler);
					return;
				}
		
				//Otherwise create new list as dictionary key
				listenList = new List<EventHandler<EventArgs>> ();
				listenList.Add (handler);
				listeners.Add (eventType, listenList);
			}
	
			/// <summary>
			/// Posts the notification.
			/// </summary>
			/// <param name="eventType">Event type.</param>
			/// <param name="sender">Sender.</param>
			public void PostNotification (int eventType, object sender, EventArgs args)
			{
				//List of listeners for this event only
				List<EventHandler<EventArgs>> listenList = null;
		
				//If no event exists, then exit
				if (!listeners.TryGetValue (eventType, out listenList))
					return;
		
				//Entry exists. Now notify appropriate listeners
				listenList.ForEach (i => i (sender, args));
			}
		}
	}
}
