﻿using UnityEngine;
using System.Collections;

public class DebugSettings
{
    public bool AlwaysGenerateProp = false;
    public int MaxCreatedProps = 0;
    public bool GodMode = false;
    public bool ShowNextLevelOnPause = false;

    public DebugSettings()
    {
    }

    public void FullMode()
    {
        AlwaysGenerateProp = true;
    }
}