﻿using UnityEngine;
using System.Collections;

public class GroundHitCheck : MonoBehaviour {
	
	//flag to find out if the object is grounded
	public bool isGrounded = false;
	
	//Empty gameobject created to determine the bounds/center of the object
	Transform GroundCheck1;

	//The layer for which the overlap has to be detected 
	public LayerMask ground_layers;

    void Start()
    {
        GroundCheck1 = transform.Find("ground_check");
    }
	
	//All the Physics related things to be done here
	void FixedUpdate(){
		//check if the empty object created overlaps with the 'ground_layers' within a radius of 1 units
		isGrounded = Physics2D.OverlapCircle(GroundCheck1.position, 0.25f, ground_layers);
		
		//uncomment this and comment the above line, if you are using a rectangle or somewhat similar shaped object
		//isGrounded = Physics2D.OverlapArea(GroundCheck1.position, GroundCheck2.position, ground_layers); 
		
		//Debug.Log("Grounded: "+isGrounded);
		
	}
	
}