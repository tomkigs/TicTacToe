﻿using Assets.Scripts.Game.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Commons
{
	public static class Extensions
	{
		public static void ForEach<T>( this IEnumerable<T> source,  Action<T> action)
		{
			foreach (T element in source)
				action(element);
		}

        public static string GetDescription(this Point point)
        {
            return "x= " + point.X + ", y= " + point.Y;
        }

        public static IEnumerable<TSource> MaxByField<TSource, TKey>(
        this IEnumerable<TSource> source, Func<TSource, TKey> keySelector) where TKey : IComparable
        {
            var max = source.Max(keySelector);
            return source.Where(i => keySelector(i).Equals(max));
        }

    }
    public static class Vector3Ext
    {
        public static Vector3 MoveBy(this Vector3 src, int x, int y, int z)
        {
            return new Vector3(src.x + x, src.y + y, src.z + z);
        }
        public static Vector3 MoveBy(this Vector3 src, float x, float y)
        {
            return new Vector3(src.x + x, src.y + y);
        }
        public static Vector3 MoveBy(this Vector3 src, float x, float y, float z)
        {
            return new Vector3(src.x + x, src.y + y, src.z + z);
        }
        public static Vector3 MoveBy(this Vector3 src, Vector3 dest)
        {
            return new Vector3(src.x + dest.x, src.y + dest.y, src.z + dest.z);
        }

    }


    public static class Vector2Ext
	{
		public static Vector2 MoveBy(this Vector2 src, int x, int y)
		{
			return new Vector2(src.x + x, src.y + y);
		}
		public static Vector2 MoveBy(this Vector2 src, float x, float y)
		{
			return new Vector2(src.x + x, src.y + y);
		}
	}
}
