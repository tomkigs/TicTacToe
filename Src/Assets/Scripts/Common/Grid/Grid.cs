﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using TicTacToeXAML;

namespace Commons.Grid
{
    public class Grid : MonoBehaviour
    {
        public GameObject columnPrefab;
        List<Item> gridItems;
        List<Column> columns = new List<Column>();
        
		public Item[,] itemsBuffer;

        void Awake()
        {
			//Debug.Log ("Grid awake");
			itemsBuffer = new Item[GameDefines.BoardSize,GameDefines.BoardSize];
			for (int col = 0; col < GameDefines.BoardSize; col++)
            {
                //Debug.Log("create column " + col);
                var columnObj = Factory.Instantiate(columnPrefab, gameObject, 0);
            }
        }

        public void OnStart()
        {
            //Debug.Log("Grid start");
			var startPoint = new Vector3(-GameDefines.BoardSize / 2, GameDefines.BoardSize / 2, -1);
            startPoint = startPoint.MoveBy(0.5f, -0.5f, 0f);//middle rotate point
            //var startPoint = new Vector3(0, 0, -1);
            var columns = FindObjectsOfType<Column>();
			for (int col = 0; col < columns.Count(); col++)
            {
                var column = columns[col];
                column.gameObject.transform.parent = transform;
                //var column = columnObj.GetComponent<Column>();
                var colPos = startPoint.MoveBy(col, 0, 0);
				column.setIndex(col, colPos, GameDefines.BoardSize);

                this.columns.Add(column);
            }

            gridItems = GameObject.FindGameObjectsWithTag("grid_item").Where(i => i.GetComponent<Item>() != null).Select(i => i.GetComponent<Item>()).ToList();
			for(int x = 0;x<GameDefines.BoardSize;x++)
				for(int y = 0;y<GameDefines.BoardSize;y++) {
			
					itemsBuffer [y, x] = getItemAtSlow (x, y);
			}
            Debug.Log("Grid Awake gridItems " + gridItems.Count);
        }

        void Start()
        {
            OnStart();
        }

        public int Width
        {
            get { return columns.Count; }
        }

        public int Height
        {
            get
            {
                if (columns.Count == 0)
                    return 0;
                return columns[0].Size;
            }//HACK rows count
        }

        // Update is called once per frame
        /*protected virtual void Update()
        {

        }*/

        public void HideAllHightlights()
        {
            Items.ForEach(i => i.ShowHighlight(false));
        }

        public List<Item> Items
        {
            get { return gridItems; }
        }

		public Item getItemAtSlow(int x, int y)
        {
            return gridItems.Where(i => i.x == x && i.y == y).FirstOrDefault();
        }


        public Item getItemAt(Vector3 worldPoint)
        {
            foreach (var gi in Items)
            {
                if (gi.GetComponent<Collider2D>() == Physics2D.OverlapPoint(worldPoint))
                {
                    return gi;
                }
            }

            return null;
        }

        internal Item FindSnappingItem(Vector3 position)
        {
            Item item = gridItems.FirstOrDefault();

            if (item == null)
                return null;
            var dist = (item.transform.position - position).sqrMagnitude;
            foreach (var nextItem in gridItems)
            {
                var nextDist = (nextItem.transform.position - position).sqrMagnitude;
                if (nextDist < dist)
                {
                    item = nextItem;
                    dist = (nextItem.transform.position - position).sqrMagnitude;
                }
            }
            //var min = gridItems.Select(i => (i.transform.position - position).sqrMagnitude).Min();
            if (dist > 10)
                Debug.LogError("FindSnappingItem min too big! Item = "+ item.name + " dist = "+ dist);
            return item;
        }
    }
}
