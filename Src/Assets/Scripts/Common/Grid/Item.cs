﻿using UnityEngine;
using System.Collections;
using System;

namespace Commons.Grid
{
    public class Item : MonoBehaviour
    {
        public int x;
        public int y;
        public GameObject highlight;

        void Start()
        {
            highlight = transform.Find("highlight").gameObject;
        }
		/*
        public override string ToString()
        {
            return "[" + x + "," + y + "]";
        }
*/
        internal void ShowHighlight(bool show)
        {
            highlight.SetActive(show);
        }

        internal bool isHighlighted()
        {
            return highlight.activeInHierarchy;
        }
		/*
        bool showPos = false;
        void OnGUI()
        {
            if (!showPos)
                return;
            Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
            pos.y = Screen.height - pos.y;
            //Debug.Log("Screen.width = "+ Screen.width+ ", Screen.height = " + Screen.height + " OnGUI x = " + pos.x + ", y = "+pos.y);
            UnityEngine.GUI.Label(new Rect(pos.x - 15, pos.y, 32, 32), x+","+y);
        }
        */
    }
}
