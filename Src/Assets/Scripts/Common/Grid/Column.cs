﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Commons.Grid
{
    public class Column : MonoBehaviour
    {
        public Item itemPrefab;
        List<Item> items = new List<Item>();
        
        public int Size
        {
            get { return items.Count; }
        }

        internal void setIndex(int col, Vector3 startPos, int size)
        {
            int height = size;
            for (int row = 0; row < height; row++)
            {
                var item = Factory.Instantiate(itemPrefab.gameObject, gameObject, 0);
                var itemPos = startPos.MoveBy(0f, -1 * row, -1f);

                item.transform.parent = transform;
                item.transform.localPosition = itemPos;

                item.GetComponent<Item>().x = col;
                item.GetComponent<Item>().y = row;

                items.Add(item.GetComponent<Item>());
            }
        }

    }
}
