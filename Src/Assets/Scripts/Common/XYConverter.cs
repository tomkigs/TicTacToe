using System;
using UnityEngine;

namespace Common
{
	public class XYToRTConverter
	{
		public static float GetR(float x, float y)
		{
			return Mathf.Sqrt (x * x + y * y);
		}
		
		public static float GetT(float x, float y)
		{
			if (x == 0)
				return y > 0 ? Mathf.PI / 2 : -Mathf.PI / 2;
			return Mathf.Atan (y / x);
		}
		
		public static float GetX(float r, float t)
		{
			return r * Mathf.Cos (t);
		}
		
		public static float GetY(float r, float t)
		{
			return r * Mathf.Sin (t);
		}
	}
}