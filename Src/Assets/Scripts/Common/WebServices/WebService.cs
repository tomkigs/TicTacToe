﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
//using Commons.KotoG;
using Assets.Scripts.Common.Logging;

namespace Commons
{
    public class WebService
    {
        //http://kotogames.com/php/ws.php?wsdl
        public const char Separator = ';';

        public void Warmup()
        {
            try
            {
                //Task.Factory.StartNew(() =>
                //{
                //    SimpleServicePortTypeClient svc = new SimpleServicePortTypeClient();
                //    var hello = svc.GetHelloAsync("tom").ContinueWith(p =>
                //    {
                //        if (p.Exception != null)
                //        {
                //            p.Exception.Handle(x =>
                //                {
                //                    Log.AddInfo(x.Message);
                //                    return false;
                //                });
                //        }
                //    });
                //});
                //Log.AddInfo("GetHelloAsync = " + hello);
            }
            catch (Exception exc)
            {
                Log.AddError(exc);
            }

        }

        public List<string> GetLevelNamesSync(string dirName)
        {
            List<string> names = new List<string>();
            try
            {
//                SimpleServicePortTypeClient svc = new SimpleServicePortTypeClient();
//                var levels = svc.GetFilesAsync(dirName).Result.@return;
//#if WINDOWS_8
//                names =  LevelLoader.GetLevelsList(levels);
//#endif
//                Log.AddInfo("names.Count = " + names);
            }
            catch (Exception exc)
            {
                Log.AddError(exc);
            }
            //Log.AddInfo(s);
            return names;
        }
        //static int RemoteCallErrorCounter = 0;
        //public async Task<GetFilesResponse> GetLevelNamesAsync(string dirName)
        //{
        //    bool error = false;
        //    try
        //    {
        //        SimpleServicePortTypeClient svc = new SimpleServicePortTypeClient();
        //        var levels = await svc.GetFilesAsync(dirName);
        //        Log.AddInfo("GetLevelNamesAsync returning levels ");

        //        return levels;
        //    }
        //    catch (Exception exc)
        //    {
        //        error = true;
        //        RemoteCallErrorCounter++;
        //        Log.AddError(exc);
        //        Log.AddInfo("GetLevelNamesAsync failed  RemoteCallErrorCounter = " + RemoteCallErrorCounter);
                
        //    }
        //    if (error && RemoteCallErrorCounter < 2)
        //        await GetLevelNamesAsync(dirName);
        //    else if (error)
        //        LastCallOK = false;
        //    //Log.AddInfo(s);
        //    return null;
        //}
        //bool addTxt
        public string GetLevelContent(string fullFilePath)
        {
            try
            {
                //if (!fullFilePath.EndsWith(".txt"))
                //    fullFilePath = fullFilePath + ".txt";
                    //SimpleServicePortTypeClient svc = new SimpleServicePortTypeClient();
                    //return svc.GetFileContentAsync(fullFilePath).Result.@return;
            }
            catch (Exception exc)
            {
                Log.AddError(exc);
            }
            return "";
        }

        public  bool SetFileContent(string fileName, string content)
        {
            try
            {
                //SimpleServicePortTypeClient svc = new SimpleServicePortTypeClient();
                //var r = svc.SetFileContentAsync(fileName, content).Result;
                //return true;
            }
            catch (Exception exc)
            {
                Log.AddError(exc);
            }

            return false;
           
        }
        public static bool LastCallOK { get; private set; } 
        public static void PrepareToCall()
        {
            LastCallOK = true;
            //RemoteCallErrorCounter = 0;
        }
    }
}
