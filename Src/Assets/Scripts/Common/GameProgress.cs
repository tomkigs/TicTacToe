﻿using UnityEngine;
using System.Collections;

namespace Commons
{
	public class GameProgress
	{
		public int Score;
		public int LastLevel = 1;
        public int CurrentLevel = 1;
        public int MaxEverPlayedLevel = 1;
        public int KilledEnemies;
        public int TotalEnemies;
        int lives;
		public int StartingLevelLives;//how many used had entering the level?
		public const int DefaultLives = 5;
		public const int MaxLives = 5;
 
		public GameProgress()
		{
            LastLevel = PlayerPrefs.GetInt("LastLevel", 1);
            MaxEverPlayedLevel = PlayerPrefs.GetInt("MaxEverPlayedLevel", LastLevel);
            ResetLives();
		}

        public void ResetLevelScore()
        {
            KilledEnemies = 0;
            Score = 0;
        }

        public void SaveLastLevel(int level)
        {
            LastLevel = level;
            PlayerPrefs.SetInt("LastLevel", level);

            if (LastLevel > MaxEverPlayedLevel)
            {
                SaveMaxEverPlayedLevel(LastLevel);
            }
        }

        public void SaveMaxEverPlayedLevel(int level)
        {
            MaxEverPlayedLevel = level;
            PlayerPrefs.SetInt("MaxEverPlayedLevel", MaxEverPlayedLevel);
        }
                
		
		public void ResetLives()
		{
			lives = DefaultLives;
			StartingLevelLives = lives;
		}

		public int Lives
		{
			get{ return lives;}
			set
			{
                if (value <= MaxLives)
					lives = value;
			}
		}

        public bool IsAlive
        {
            get { return Lives >= 0; }
        }
	}
}
