﻿using UnityEngine;
using System.Collections;
using Commons;
using System.Collections.Generic;
using System;

/// <summary>
/// Creating instance of sounds from code with no effort
/// </summary>
public class AudioService : MonoBehaviour
{
	public AudioClip levelEndSound;
	public AudioClip gameOverSound;

    static AudioSource audioSourceMusic;
    static AudioSource audioSourceSound;
    public static AudioSource AudioSourceSoundExtra;
    public AudioClip[] MusicClips;
	public static int currentMusicClipIndex;

    static bool musicOn = true;
    public static bool SoundOn = true;
	public static string SoundsPath = "Sounds/";
	public static AudioService Instance;
    Dictionary<string, AudioClip> sounds = new Dictionary<string, AudioClip>();

    void Start()
	{
		Instance = this;
        audioSourceMusic = GetComponentsInChildren<AudioSource>()[0];
        audioSourceSound = GetComponentsInChildren<AudioSource>()[1];
        Debug.Log("AudioListener.volume = " + AudioListener.volume);
        PlayMusic();
	}

    void Update()
    {
        if (audioSourceMusic != null && MusicOn)
        {
            if (!audioSourceMusic.isPlaying)
                PlayMusic();//play next song, why there is no event song has finished ?
        }
    }

    public static bool MusicOn
    {
        set 
        { 
            musicOn = value;
            if (!musicOn)
                audioSourceMusic.Pause();
            else
                audioSourceMusic.Play();
        }
        get { return musicOn; }
    }
        
    public void PlaySound (string sound, bool extraPlayer = false)
    {
        if (!SoundOn)
            return;
        if (string.IsNullOrEmpty(sound))
            return;
        //Debug.Log ("MakeSound "+sound);
        //TimeTracker tr = new TimeTracker();
        AudioClip clip = GetClip(sound);
        //Debug.Log("Resources.Load " + tr.TotalSeconds);
        MakeSound(clip, extraPlayer);

    }

    private AudioClip GetClip(string sound)
    {
        AudioClip clip = null;
        if (sounds.ContainsKey(sound))
            clip = sounds[sound];
        else
        {
            clip = (AudioClip)Resources.Load(SoundsPath + sound);
            sounds.Add(sound, clip);
        }

        return clip;
    }

    /// <summary>
    /// Play a given sound
    /// </summary>
    /// <param name="originalClip"></param>
    protected void MakeSound(AudioClip originalClip, bool extraPlayer = false)
	{
        //AudioSource.PlayClipAtPoint(originalClip, Vector3.zero, 1);
        if (extraPlayer && AudioSourceSoundExtra)
        {
            AudioSourceSoundExtra.PlayOneShot(originalClip);
        }
        else
            audioSourceSound.PlayOneShot(originalClip);
        //if (!musicOn)
        //   audioSource.Pause();//HACK unity bug?
    }

    public void PlayMusic()
	{
		if (!MusicOn)
			return;

		if(MusicClips != null)
		{
            if(audioSourceMusic == null)
			    audioSourceMusic = GetComponentInChildren<AudioSource>();
			//Debug.LogError("audio = "+audio);
			if(currentMusicClipIndex ==  MusicClips.Length)
				currentMusicClipIndex = 0;
			if(MusicClips.Length > 0)
			{
				audioSourceMusic.clip = MusicClips[currentMusicClipIndex];
                currentMusicClipIndex++;
				audioSourceMusic.Play();
			}
		}
	}
	public void MakeGameOverSound ()
	{
		MakeSound(gameOverSound);
	}
    	
	public void MakeLevelEndSound()
	{
		MakeSound(levelEndSound);
	}
	

}