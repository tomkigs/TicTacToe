﻿using UnityEngine;
using System.Collections;
using System;
using Commons.GUI;
using UnityEngine.SceneManagement;
using Assets.Scripts.Common.Logging;

namespace Commons
{
	public class ApplicationService   {
		public static readonly ApplicationService Instance = new ApplicationService();
		public bool GamePaused{ get; set; }
		public static string LevelCoreName = "Scene";
		public ReleaseInfo ReleaseInfo = new ReleaseInfo();
		public GameProgress GameProgress = new GameProgress();
        public DebugSettings DebugSettings = new DebugSettings();
        public int NumberOfLevels = 9;
        public bool MainMenuUsed;
        internal bool PlayGamesPlatformActivated;
        public int currentTestIndex;
		public bool DeserializeMatchAfterLoad;

        ApplicationService()
        {
            ReleaseInfo.ShopName = ReleaseInfo.Shop.Google;
            if (GameProgress.LastLevel > NumberOfLevels)
                GameProgress.LastLevel = NumberOfLevels;

            //var sdk = GetSDKLevel();
            //Debug.Log("GetSDKLevel = "+ sdk);
        }

        public int GetSDKLevel()
        {
            try
            {
#if UNITY_ANDROID
                var clazz = AndroidJNI.FindClass("android.os.Build$VERSION");
                var fieldID = AndroidJNI.GetStaticFieldID(clazz, "SDK_INT", "I");
                var sdkLevel = AndroidJNI.GetStaticIntField(clazz, fieldID);

                return sdkLevel;
#endif
            }
            catch (Exception exc)
            {
                Debug.Log(exc);

            }
            return -1;

        }

        public void EnsureDataConsistency()
        {
            try
            {
#if UNITY_EDITOR
                if (!MainMenuUsed)
                {
                    var levelNumberFromName = GetLevelNumberFromName();

                    SaveLevelData(levelNumberFromName);
                }
#endif

            }
            catch (Exception exc)
            {
				Log.AddError(exc);
            }
        }

        public int GetLevelNumberFromName()
        {
            
            return Convert.ToInt32(SceneManager.GetActiveScene().name.Replace(LevelCoreName, ""));
        }

		public void LoadLevel(int level = 1, bool next = false, bool reload = false)
		{
            SceneManager.LoadScene(LevelCoreName+level);
            SaveLevelData(level);
			GameProgress.ResetLives ();
			Resume();
            GoogleMobileAdsDemoScript.Instance.HideBanner();
		}

        private void SaveLevelData(int level)
        {
            GameProgress.CurrentLevel = level;
            GameProgress.SaveLastLevel(level);
        }

        public void ContinueGame()
        {
            if (GameProgress.LastLevel > NumberOfLevels)
                GameProgress.LastLevel = NumberOfLevels;
            LoadLevel(GameProgress.LastLevel);
        }
		
		public void ReloadLevel(string caller = "")
		{
            LoadLevel(GameProgress.CurrentLevel, false, true);
			Resume();
		}
		
		public void LoadNextLevel()
		{
            var current = GameProgress.CurrentLevel;
			LoadLevel(current+1, true);
		}

		public void Pause()
		{
			Time.timeScale = 0f;
			GamePaused = true;
		}
		
		public void Resume()
		{
			Time.timeScale = 1;
			GamePaused = false;
		}

        internal void ShowMainMenu()
        {
            SceneManager.LoadScene("MainMenu");
        }

        public static void SaveLanguage(string lang)
        {
            PlayerPrefs.SetString("Language", lang);
        }

        public static string LoadLanguage()
        {
            return PlayerPrefs.GetString("Language");
        }


    }
}
