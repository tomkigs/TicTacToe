﻿using UnityEngine;
using System.Collections;
using Commons;

public class HeavyItem : EntityBase
{
    bool falling;
    new Rigidbody2D rigidbody2D;
    SpriteSwitcher ssw;
    public bool dieOnFall;
    //TimeTracker trFalling = new TimeTracker();
    // Use this for initialization
	new void  Start () {
        rigidbody2D = GetComponent<Rigidbody2D > ();
        ssw = GetComponent<SpriteSwitcher>();
    }
    void FixedUpdate()
    {
        //if(tag == "iceBlock")
        //    Debug.Log("FixedUpdate rigidbody2D.velocity.y = "+ rigidbody2D.velocity.y + " name = "+name);
        var faillOld = falling;
        falling = rigidbody2D.velocity.y < -4f;// && trFalling.TotalSeconds > 0.2f;
       
        if (faillOld && !falling)
        {
            AudioService.Instance.PlaySound("stone1");
            if (dieOnFall)
            {
                if (ssw == null)
                    Die();
                else
                {
                    ssw.ShowNext();
                    if (ssw.CurrentIndex == ssw.Sprites.Length)
                        Die();
                }
            }
        }

        //var vel = rigidbody2D.velocity.y;
        //Debug.Log("HeavyItem vel = " + vel);
    }
    // Update is called once per frame
    void Update () {
        
    }

    protected override Vector2 GetExplodePosition()
    {
        if(tag == "iceBlock")
           return new Vector2(transform.position.MoveBy(0.5f, 0, 0).x, transform.position.y);
        return base.GetExplodePosition();
        
    }
}
