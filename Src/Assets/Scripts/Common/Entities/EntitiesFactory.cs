﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Commons;

public class EntitiesFactory : MonoBehaviour {

	public List<GameObject> EntitiesPrefabs = new List<GameObject>();

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	public static GameObject[] CreateRandomArray(EntitiesFactory[] factories, Vector2 positionRange)
	{
		GameObject[] arr = new GameObject[factories.Length];
		for (int i = 0; i < factories.Length; i++) 
		{
			arr [i] = factories[i].CreateRandom (positionRange);
		}	
		return arr;
	}

    public GameObject CreateRandom(Vector2 positionRange)
    {
        var obj = CreateRandom();
        if (obj == null)
            return null;
        var x = RandHelper.GetRandomInt((int)positionRange.x);
        var y = RandHelper.GetRandomInt((int)positionRange.y);
        var rdx = (float)RandHelper.GetRandomDouble();
        var signx = rdx > 0.5f ? 1 : -1;
        var rdy = (float)RandHelper.GetRandomDouble();
        var signy = rdy > 0.5f ? 1 : -1;

		var destX = signx * x * rdx;
		var destY = signy * y * rdy;
		if (destX >= 0 && destX < 1)
			destX += 1;
		else if(destX > -1)
			destX -= 1;
		if (destY >= 0 && destY < 1)
			destY += 1;
		else if(destY > -1)
			destY -= 1;
		obj.transform.position = new Vector2(destX, destY);
        return obj;
    }

    public GameObject CreateRandom()
	{
        var index = RandHelper.GetRandomInt(EntitiesPrefabs.Count);
        if(EntitiesPrefabs.Count > index)
         return Factory.Instantiate(EntitiesPrefabs[index]);
        return null;

    }
}
