﻿using UnityEngine;
using System.Collections;
using System;

public class SpriteSwitcher : MonoBehaviour {
    SpriteRenderer sr;
    public int CurrentIndex = 0;
    public Sprite[] Sprites;
    public EventHandler AllSpritesShown;

	// Use this for initialization
	void Start () {
        sr = GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ShowNext()
    {
        CurrentIndex++;
        if (Sprites.Length > CurrentIndex)
            sr.sprite = Sprites[CurrentIndex];
        else if (AllSpritesShown != null)
            AllSpritesShown(this, EventArgs.Empty);

    }
}
