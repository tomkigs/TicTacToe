﻿using UnityEngine;
using System.Collections;
using Commons;

public class LeftRight {
    EntityBase entity;
	bool timeBased = true;
	TimeTracker tracker = new TimeTracker();
	public float CycleDuration = 2.5f;//seca

	public LeftRight(EntityBase entity)
	{
		this.entity = entity;
	}
	
	// Update is called once per frame
	public void Update () 
	{
		if (timeBased) 
		{
			if(tracker.TotalSeconds > CycleDuration && entity.CanFlipWalking())
			{
				entity.Flip();
				tracker.Reset();
			}
		}
	}
}
