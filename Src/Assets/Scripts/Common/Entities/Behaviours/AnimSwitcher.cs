﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AnimSwitcher  {
	protected Dictionary<int, string> states = new Dictionary<int, string>();
	public int CurrentState = -1;
	List<int> idle = new List<int> ();
	List<int> stopsVelocity = new List<int> ();

    EntityBase entity;
	Animator animator;

	public AnimSwitcher(EntityBase entity)//, string animation, float duration = 2, bool stopEntity = true)
	{
		this.entity = entity;
		//if (entity.Speed.x != 0)
		//	orgSpeed = entity.Speed;
		animator = entity.GetComponent<Animator> ();
	}
	public bool allowsStringStates ;
	public void SetAnimatedState(int state)
	{
		//if (CurrentState == BaseEntity.States.DieState)
		//	return;
		if (ContainsState (state)) 
		{
			CurrentState = state;
			var st = states [CurrentState];
			//Debug.Log ("st = "+ st);
			this.animator.Play (st);
		} 
		else if(!allowsStringStates)
			throw new Exception ("unknown state : "+state + " eniity = "+this.entity);
	}
	public virtual bool IsIdle(){return 
            CurrentState == EntityBase.States.Idle1 || 
            CurrentState == EntityBase.States.Idle2 || 
            CurrentState == EntityBase.States.Idle3; }

	public void AddAnimatedState(int st, string name)
	{
		states.Add (st, name);
	}
	public bool ContainsState(int state){
				return states.ContainsKey (state);
		}
	public void AddIdle (int stateID, bool stopVelocity = true)
	{
		idle.Add (stateID);
		if(stopVelocity)
			stopsVelocity.Add (stateID);
	}

	//public void StopEntity ()
	//{
	//	entity.Speed = new Vector2 (0, 0);
	//}

	//public void StartEntity ()
	//{
	//	if (orgSpeed.x != 0) 
	//	{	//hack
	//		entity.Speed = orgSpeed;
	//	}
	//}
	public int SinglePlayState = -1;
	public void OnAnimationFinished()
	{
		var index = RandHelper.GetRandomInt(idle.Count);
		if (SinglePlayState > 0 && idle [index] == SinglePlayState && CurrentState == SinglePlayState) 
		{
			index = index == 0? 1 : 0;//HACK
		}

		var nextAnim = idle [index];
		//if(stopsVelocity.Contains(nextAnim))
		//{
		//	//Debug.Log("OnAnimationFinished going idle entity.Speed = "+entity.Speed);
		//	StopEntity ();
		//}
		//else
		//{
		//	StartEntity ();
		//}
		SetAnimatedState(nextAnim);
	}
	public void Update () 
	{
		//if (CurrentState == BaseEntity.States.WalkState && entity.Speed.x == 0)
		//	entity.Speed = orgSpeed;
	}
	
}
