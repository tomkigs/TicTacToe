﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Commons;

public class EntityBase : MonoBehaviour {
	List<string> killingTags = new List<string>();
	public GameObject explodeObject;
    protected SpriteRenderer spriteRenderer;
    public string DeathSound;
    public Color ExplodePartsColor = Color.white;
    protected AnimSwitcher animSwitcher;
    bool facingLeft = false;
    public Vector2 Direction = new Vector2(0, 0);
    public GameObject ghostPrefab;
    protected Animator animator;
    public float DestroyExplosionAfter = 1.0f;

    public class States
    {
        public const int WalkState = 0;
        public const int FightState = 1;
        public const int DistanceFightState = 2;
        
        public const int JumpState = 4;
        public const int DieState = 9;
        public const int TurnState = 5;
        public const int Idle1 = 10;
        public const int Idle2 = 11;
        public const int Idle3 = 12;
    }

    // Use this for initialization
    protected void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animSwitcher = new AnimSwitcher(this);
        animator = GetComponent<Animator>();
    }

    public void SetAnimatedState(int state)
    {
        animSwitcher.SetAnimatedState(state);
    }

    protected DebugSettings DebugSettings
    {
        get { return ApplicationService.Instance.DebugSettings; }
    }

    protected GameProgress GameProgress
    {
        get { return ApplicationService.Instance.GameProgress; }
    }

    public virtual bool CanFlipWalking()
    {
        return IsIdle();
    }
    protected int CurrentState
    {
        get { return animSwitcher.CurrentState; }
    }
    public virtual bool IsIdle()
    {
        if (animSwitcher == null)
            return false;
        return animSwitcher.IsIdle();
    }

    protected Vector2 GetMoveDirectionFromFacing()
    {
        return new Vector2(facingLeft ? -1 : 1, 0);
    }

    public virtual void Flip()
    {
        //Debug.Log ("Flip " + this + " state = "+CurrentState);
        facingLeft = !facingLeft;
        Direction = GetMoveDirectionFromFacing();

        //do actual flip
        Flip(transform);
    }

    void Flip(Transform transform)
    {
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    public void AddAnimatedState(int stateID, string name, bool isIdle = false, bool stopsVelocity = false)
    {
        animSwitcher.AddAnimatedState(stateID, name);
        if (isIdle)
            animSwitcher.AddIdle(stateID, stopsVelocity);
    }
    public bool ContainsState(int state)
    {
        return animSwitcher.ContainsState(state);
    }

    public bool IsShownByCamera()
    {
        return spriteRenderer != null && spriteRenderer.IsVisibleFrom(Camera.main);
    }
	// Update is called once per frame
	void Update () {
	
	}

    private void Explode()
    {
        if (explodeObject)
        {
            var emitter = InstantiateExplosion();
            Destroy(emitter, DestroyExplosionAfter);

            var go = emitter as GameObject;
            if (go != null)
            {
                var ps = go.GetComponent<ParticleSystem>();
                if(ps != null)
                    ps.startColor = ExplodePartsColor;
            }
        }
    }

    public void Die()
    {
        Debug.Log("Die "+name);
        Explode();


        if (ghostPrefab != null)
        {
            var ghost = Instantiate(ghostPrefab, transform.position, Quaternion.identity) as GameObject;
            Destroy(ghost, 3);
            ghost.transform.localScale = this.transform.localScale;
            //if (Direction.x < 0)
            //    Flip(ghost.transform);
        }
        Destroy(gameObject);
        AudioService.Instance.PlaySound(DeathSound);
    }

    protected Object InstantiateExplosion(GameObject explosion = null, string sound = "")
    {
        var expPos = GetExplodePosition();
        return InstantiateExplosion(expPos, explosion, sound);
    }

    protected virtual Vector2 GetExplodePosition()
    {
        return new Vector2(transform.position.x, transform.position.y);
    }

    protected Object InstantiateExplosion(Vector2 pos, GameObject explosion = null, string sound = "")
    {
        if (explosion == null)
            explosion = explodeObject;
        if (explosion)
        {
            if (!string.IsNullOrEmpty(sound))
                AudioService.Instance.PlaySound(sound);
            return Factory.Instantiate(explosion, pos, 5);
        }
        return null;
    }

	protected void AddHarmingTag(string tag){
		killingTags.Add (tag);
	}
	protected bool IsHarmedBy(string tag){
		return killingTags.Contains (tag);
	}
}
