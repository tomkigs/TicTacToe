﻿using System;
using System.Collections.Generic;
//using System.Diagnostics;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Common.Logging
{
    public class Log
    {
        public static void AddInfo(string log)
        {
			//Log2222222222222
			Debug.Log(log);
        }

        public static void AddError(string log)
        {
			Debug.LogError(log);
        }
        //
        public static void AddError(Exception exc)
        {
			Debug.LogError(exc.Message);
			Debug.LogError (exc.StackTrace);
        }
    }
}
