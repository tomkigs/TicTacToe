﻿using UnityEngine;
using System.Collections;

public class CameraHelper : MonoBehaviour {
    public static CameraHelper Instance;
    Vector3 zeroWorld;
    // Use this for initialization
    void Start () {
        Instance = this;
        var zero = new Vector3(0, 0, 0);// Camera.main.transform.position.z * -1);
        zeroWorld = Camera.main.ScreenToWorldPoint(zero);
        zeroWorld.x *= -1;
        zeroWorld.y *= -1;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    //Vector3 offset;
    void SetOffset(Vector3 offset)
    {
        //this.offset = offset;
    }

    public Vector3 GetWorldDelta(Vector2 screenDelta)
    {
        Vector2 screenDelta3d = new Vector3(screenDelta.x, screenDelta.y, 0);
        var worldPoint = Camera.main.ScreenToWorldPoint(screenDelta3d);
        worldPoint += zeroWorld - Camera.main.transform.position;

        //Debug.Log ("worldPoint = "+worldPoint);
        return worldPoint;
    }
}
