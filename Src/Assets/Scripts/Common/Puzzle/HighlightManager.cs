﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Commons;
using System.Linq;
using System;
using Commons.Effects;

namespace Commons.Puzzle
{
    public class HighlightManager : MonoBehaviour
    {
        public class DragDropInfo
        {
            public GameObject puzzle;
            public GameObject highlight;
        }

        Dictionary<GameObject, List<GameObject>> puzzleToHighlight = new Dictionary<GameObject, List<GameObject>>();
        Commons.DragDropManager dragDropMgr;
        public string puzzleParentTransformName = "";
        public string hightlightParentTransformName = "";
        public string highlightChild = "";
        GameObject puzzleParentTransform;
        GameObject hightlightParentTransform;
        public float MinHighlightDistance = 1f;
        public EventHandler<EventArgs<DragDropInfo>> OnObjectSnapped;

        public void AddPuzzleToHighlightByTag(string puzzleNameBegin, string highlightTag)
        {
            GameObject puzzleSearchStart = GetPuzzleSearchStart();
            List<Transform> puzzles = GetPuzzles(puzzleSearchStart, puzzleNameBegin);
            //var highs = GameObject.FindGameObjectsWithTag(highlightTag).Select(i=>i.transform).ToList();
            var highs = transform.GetChildrenByNameStart("coal");/// (highlightTag).Select(i => i.transform).ToList();
            if (!highs.Any())
            {
                Debug.LogError("AddPuzzleToHighlightByTag highlight not found highlightTag = " + highlightTag);
                return;
            }
            AddHightlights(puzzles, highs);
        }

        public void AddPuzzleToHighlight(string puzzleNameBegin, string highlightNameBegin)
        {
            if (puzzleParentTransformName.Any())
                SetParentTransform();
            if (hightlightParentTransformName.Any())
                SetParentHightlightTransform();
            GameObject puzzleSearchStart = GetPuzzleSearchStart();

            GameObject highSearchStart = gameObject;
            if (hightlightParentTransform != null)
            {
                highSearchStart = hightlightParentTransform;
            }
            AddPuzzleToHighlight(puzzleSearchStart, highSearchStart, puzzleNameBegin, highlightNameBegin);
        }

        private GameObject GetPuzzleSearchStart()
        {
            GameObject puzzleSearchStart = gameObject;
            if (puzzleParentTransform != null)
            {
                puzzleSearchStart = puzzleParentTransform;
            }

            return puzzleSearchStart;
        }

        void AddPuzzleToHighlight(GameObject puzzleSearchStart, GameObject hightSearchStart, string puzzleNameBegin, string highlightNameBegin)
        {
            List<Transform> puzzles = GetPuzzles(puzzleSearchStart, puzzleNameBegin).Where(i=> !i.name.EndsWith("Ph")).ToList();
            if (!puzzles.Any())
                Debug.LogError("AddPuzzleToHighlight !puzzles.Any() puzzleNameBegin = "+ puzzleNameBegin);
            var highs = hightSearchStart.transform.GetChildrenByNameStart(highlightNameBegin);
            if (!highs.Any())
            {
                Debug.LogError("AddPuzzleToHighlight highlight not found highlightNameBegin = " + highlightNameBegin);
                return;
            }
            AddHightlights(puzzles, highs);

        }

        private static List<Transform> GetPuzzles(GameObject puzzleSearchStart, string puzzleNameBegin)
        {
            var puzzles = puzzleSearchStart.transform.GetChildrenByNameStart(puzzleNameBegin);
            if (!puzzles.Any())
            {
                Debug.LogError("AddPuzzleToHighlight puzzle not found puzzleNameBegin = " + puzzleNameBegin);
                return puzzles;
            }

            return puzzles;
        }

        private void AddHightlights(List<Transform> puzzles, List<Transform> highs)
        {
            
            foreach (var puzzle in puzzles)
            {
                foreach (var high in highs)
                {
                    if (!puzzleToHighlight.ContainsKey(puzzle.gameObject))
                        puzzleToHighlight[puzzle.gameObject] = new List<GameObject>();
                    puzzleToHighlight[puzzle.gameObject].Add(high.gameObject);
                    if (highlightChild.Any())//hide fill
                        high.Find(highlightChild).gameObject.SetActive(false);
                    if (high.GetComponent<AlphaChanger>() != null)
                        high.GetComponent<AlphaChanger>().enabled = false;
                }
            }
        }

        public void SetDragDropManager(Commons.DragDropManager mgr)
        {
            dragDropMgr = mgr;
            dragDropMgr.OnObjectDragged += OnObjectDragged;
            dragDropMgr.OnObjectDraggEnded += OnObjectDraggEnded;
        }
        //GameObject dragged;
        void OnObjectDragged(object sender, EventArgs<GameObject> obj)
        {
            if (puzzleToHighlight.ContainsKey(obj.Value))
            {
                var highs = puzzleToHighlight[obj.Value].Where(i => !busySlots.Contains(i)).ToList(); 
                highs.ForEach(i => ShowHighlight(i,false));
                foreach (var high in highs)
                {
                    //float distance = Vector3.Distance(high.transform.position, obj.Value.transform.position);
                    float distance = Vector2.Distance(high.transform.position, obj.Value.transform.position);
                    var active = distance < MinHighlightDistance;
                    ShowHighlight(high, active);
                    //Debug.Log("OnObjectDragged! dist = " + distance + " active = " + active);
                    if (active)
                    {
                        //dragged = obj.Value;
                        break;
                    }
                }
            }
        }
        
        void OnObjectDraggEnded(object sender, EventArgs<GameObject> obj)
        {
            //if (dragged == null)//do I drag?
            //    return;
            //dragged = null;
            if (puzzleToHighlight.ContainsKey(obj.Value))
            {
                var highs = puzzleToHighlight[obj.Value].Where(i=> !busySlots.Contains(i)).ToList();
                foreach (var high in highs)
                {
                    if (high.transform.Find(highlightChild).gameObject.activeInHierarchy)
                    {
                        obj.Value.transform.position = high.transform.position;
                        high.transform.Find(highlightChild).gameObject.SetActive(false);
                        busySlots.Add(high);
                        snappedPuzzles.Add(obj.Value);
                        if (OnObjectSnapped != null)
                            OnObjectSnapped(this, new EventArgs<DragDropInfo>(new DragDropInfo() { puzzle = obj.Value, highlight = high }));

                        if (high.GetComponent<AlphaChanger>() != null)
                            high.GetComponent<AlphaChanger>().enabled = true;
                        break;
                    }
                }
                //Debug.Log("OnObjectDragged! dist = " + distance + " active = " + active);
            }
        }

        List<GameObject> busySlots = new List<GameObject>();
        List<GameObject> snappedPuzzles = new List<GameObject>();
        public bool IsSnapped(GameObject puzzle)
        {
            return snappedPuzzles.Contains(puzzle);
        }

        private void ShowHighlight(GameObject high, bool active)
        {
            high.transform.Find(highlightChild).gameObject.SetActive(active);
        }

        // Use this for initialization
        void Start()
        {
            busySlots.Clear();
            snappedPuzzles.Clear();
            SetParentTransform();
        }

        private void SetParentTransform()
        {
            if (puzzleParentTransformName.Any())
            {
                puzzleParentTransform = GameObject.Find(puzzleParentTransformName);
            }
        }

        private void SetParentHightlightTransform()
        {
            if (hightlightParentTransformName.Any())
            {
                hightlightParentTransform = GameObject.Find(hightlightParentTransformName);
            }
        }
            // Update is called once per frame
        void Update()
        {

        }
    }
}
