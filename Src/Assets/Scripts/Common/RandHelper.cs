﻿using UnityEngine;
using System.Collections;

public class RandHelper : MonoBehaviour {
	static System.Random rnd = new System.Random ();

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static int GetRandomInt(int max){
		return rnd.Next (max);
	}

	public static double GetRandomDouble(){
		return rnd.NextDouble ();
	}
}
