﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common
{
    class Defines
    {
        public static readonly Vector3 Vector3Zero = new Vector3(0, 0, 0);
        public static readonly Vector3 Vector2Zero = new Vector2(0, 0);
    }
}
