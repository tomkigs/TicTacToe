﻿using UnityEngine;
using System.Collections;
public class AnimateUV : MonoBehaviour {
	public float scrollSpeed = 0.5f;
	Vector2 myVector;
	public bool ThreeD = true;
	// Use this for initialization
	// Update is called once per frame
	void Update () {
		float offsetY=scrollSpeed*Time.time;
		myVector = new Vector2(0, offsetY);
		if(ThreeD)
			GetComponent<Renderer>().material.SetTextureOffset("_MainTex", myVector);
		else
			GetComponent<SpriteRenderer>().material.SetTextureOffset("_MainTex", myVector);
	}
}