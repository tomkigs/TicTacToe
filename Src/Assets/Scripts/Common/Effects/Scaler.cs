using UnityEngine;
using System.Collections;

namespace Commons
{
	namespace Effects
	{
		public class Scaler : MonoBehaviour {
			public float step = 0.005f;
			//public bool upDown = true;
			public float maxFlipFlopChange = 0.1f;
			float currentChange = 0;
            public enum ScaleType { FlipFlop, Up, Down}
            public ScaleType scaleType = ScaleType.FlipFlop;
            public float maxScaleUp = 1.0f;
            public float minScaleDown = 0f;
            public float StartDelay = 0;
            TimeTracker trTotal = new TimeTracker();
            // Use this for initialization
            void Start () {
				
			}
			
			// Update is called once per frame
			void Update () {
                if (StartDelay > 0 && trTotal.TotalSeconds < StartDelay)
                    return;
                else
                    trTotal.Pause();
                //var rot = this.transform.rotation;	
                //Debug.Log("Scaler update");
                var scale = this.transform.localScale;
                if (scaleType == ScaleType.FlipFlop)
                {
                    scale.x += step;
                    scale.y += step;
                    currentChange += step;
                    
                    if (currentChange >= maxFlipFlopChange)
                    {
                        step *= -1;

                    }
                    if (currentChange <= 0)
                        step *= -1;
                }
                else 
                {
                    var stepSign = scaleType == ScaleType.Up ? 1 : -1;
                    var canApply = (scaleType == ScaleType.Up && scale.x < maxScaleUp) || ( scaleType == ScaleType.Down && scale.x > minScaleDown);
                    if (canApply)
                        scale.x += stepSign*step;
                    canApply = (scaleType == ScaleType.Up && scale.y < maxScaleUp) || (scaleType == ScaleType.Down && scale.y > minScaleDown);
                    if (canApply)
                        scale.y += stepSign * step;
                }

                this.transform.localScale = scale;
			}
		}
	}
}