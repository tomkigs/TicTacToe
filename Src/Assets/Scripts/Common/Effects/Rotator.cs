﻿using UnityEngine;
using System.Collections;

namespace Commons
{
	namespace Effects
	{
		public class Rotator : MonoBehaviour {
			public float rotStep = 10f;
            
			// Use this for initialization
			void Start () {
			
			}
			
			// Update is called once per frame
			void Update () {
				this.transform.Rotate (0f, 0f, rotStep); 
			}
		}
	}
}