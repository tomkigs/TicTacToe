﻿using UnityEngine;
using System.Collections;
using Commons;
using System;
using UnityEngine.UI;

public class Flicker: MonoBehaviour {
	TimeTracker flickerTracker = new TimeTracker();
	TimeTracker enabledTracker = new TimeTracker();
	SpriteRenderer sr;
    public float AutoDisableTimeLength;
	public float OffSeconds = 0.1f;
	public float OnSeconds = 0.5f;
	public EventHandler EnabledChanged;
    Text txt;
	public bool isEnabled = false;

	public bool Enabled 
	{
        get { return isEnabled; }
		set{
            isEnabled = value;
            if (value)
			    enabledTracker.Reset();
			if(EnabledChanged != null)
				EnabledChanged(this, EventArgs.Empty);
		}
	}
	
	void Start()
	{
		sr = GetComponent<SpriteRenderer> ();
        if(sr == null)
            txt = GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () 
	{
		if (!Enabled) 
		{
            EnsureRenderEnabled();
			return;
		}

		if (Enabled && AutoDisableTimeLength > 0) 
		{
			if(enabledTracker.TotalSeconds > AutoDisableTimeLength)
			{
				Enabled = false;
                EnsureRenderEnabled();
			}
		}

		bool doSwitch = false;
        bool isEnabled = sr != null ? sr.enabled : txt.enabled;
        if (isEnabled && flickerTracker.TotalSeconds >= OnSeconds) 
		{
			doSwitch = true;
		}
		else if (!isEnabled && flickerTracker.TotalSeconds >= OffSeconds) 
		{
			doSwitch = true;
		}
		if (doSwitch) 
		{
			//Debug.Log("doSwitch!! tracker.TotalSeconds = " +tracker.TotalSeconds);
			flickerTracker.Reset();
            if(sr != null)
                sr.enabled = !sr.enabled;	
            else
                txt.enabled = !txt.enabled;
        }
	}

    private void EnsureRenderEnabled()
    {
        if (sr != null)
        {
            if (!sr.enabled)//effect is turned off, so turn on the renderer
                sr.enabled = true;
        }
        else
            if (!txt.enabled)//effect is turned off, so turn on the renderer
                txt.enabled = true;
    }
}
