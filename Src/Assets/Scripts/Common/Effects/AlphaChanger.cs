using System;
using UnityEngine;
using UnityEngine.UI;

namespace Commons
{
	namespace Effects
	{
		public class AlphaChanger : MonoBehaviour
		{
			//float orgAlpha;
			public bool Active = true;
            public float Step = -0.01f;
            SpriteRenderer sr;
            Image img;
            Text txt;
			void Start ()
			{
                sr = GetComponent<SpriteRenderer>();
                if(sr == null)
                    img = GetComponent<Image>();
                if (sr == null && img == null)
                    txt = GetComponent<Text>();
                //orgAlpha = sr.color.a;
            }

			void Update()
			{
                if (Active)
                {
                    if(sr != null)
                        ChangeAlpha(sr, Step);
                    else if (img != null)
                        ChangeAlpha(img, Step);
                    else if (txt != null)
                        ChangeAlpha(txt, Step);
                }
			}

            public void SetAlpha(float amount)
            {
                if (sr == null)
                    sr = GetComponent<SpriteRenderer>();//HACK
                sr.SetAlpha(amount);
            }

            public static void SetAlpha(Transform obj, float amount)
            {
                var sr = obj.GetComponent<SpriteRenderer>();
                sr.SetAlpha(amount);
            }

			public static void SetAlpha(MonoBehaviour obj, float amount)
			{
				var sr = obj.GetComponent<SpriteRenderer> ();
				sr.SetAlpha (amount);
			}

            public static void ChangeAlpha(SpriteRenderer sr, float step = 0.01f)
			{
				var col = sr.color;
                if (step < 0 && col.a > 0 || step > 0 && col.a < 1) 
				{
                    col.a = col.a + step;
				}
                sr.color = col;
			}

            public static void ChangeAlpha(Image sr, float step = 0.01f)
            {
                var col = sr.color;
                if (step < 0 && col.a > 0 || step > 0 && col.a < 1)
                {
                    col.a = col.a + step;
                }
                sr.color = col;
            }

            public static void ChangeAlpha(Text sr, float step = 0.01f)
            {
                var col = sr.color;
                if (step < 0 && col.a > 0 || step > 0 && col.a < 1)
                {
                    col.a = col.a + step;
                }
                sr.color = col;
            }
        }
	}
}

