﻿using UnityEngine;
using System.Collections;

namespace Commons
{
	public class SmoothFollow2D : MonoBehaviour 
	{
		private Vector3 velocity = Vector3.zero;
		public Transform target;
		Vector3? deltaZero;

		void Start()
		{

		}
		// Update is called once per frame
		void Update () 
		{
			if (target)
			{
				Vector3 point = Camera.main.WorldToViewportPoint(target.position);
				Vector3 delta = target.position - Camera.main.ViewportToWorldPoint(new Vector3(0.2f, 0.5f, point.z)); 
				Vector3 destination = transform.position + delta;
				if(deltaZero == null)
				{
					deltaZero = delta;
				}
				destination -= deltaZero.Value;
				transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, 0);
			}
			
		}
	}
}