using System;
using System.Collections;
using UnityEngine;

namespace Common
{
	public interface PathProps
	{
		void Reset();
		Vector2 GetNextStep();
	}

	public class PathSegment : PathProps
	{
		public Vector2 segment;
		public float step;

		public float count = 0f;
		public bool Finished { get { return count >= 100f;  } }
		private float dx = 0f;
		private float dy = 0f;

		public virtual void Reset()
		{
			count = 0;
			dx = (segment.x / 100f) * step;
			dy = (segment.y / 100f) * step;
		}
		
		public virtual Vector2 GetNextStep()
		{
			count += step;
			return new Vector2 (dx, dy);
		}
	}

	public class PathSegmentContainer : PathProps
	{
		public PathSegment[] segments;
		public bool loop = true;
		private int count = 0;
        public int firstMoveSleepSegmentsCount = 0;

		public virtual void Reset()
		{
            count = count > 0 ? firstMoveSleepSegmentsCount : 0;
			foreach (var seg in segments)
				seg.Reset ();
		}
		
		public virtual Vector2 GetNextStep()
		{
			if (segments == null)
                return Defines.Vector2Zero;

			if (count >= segments.Length) 
			{
				if (loop)
					Reset ();
				else
                    return Defines.Vector2Zero;
			}
			var next = segments [count].GetNextStep ();
			if (segments [count].Finished)
				count += 1;
			return next;
		}
	}
}

