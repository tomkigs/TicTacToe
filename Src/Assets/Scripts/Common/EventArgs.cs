﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Commons
{
	public class EventArgs<T> : EventArgs
	{
		public EventArgs(T value)
		{
			m_value = value;
		}
		
		private T m_value;
		
		public T Value
		{
			get { return m_value; }
		}
	}

    public class EventArgs<T, E> : EventArgs
    {
        public EventArgs(T value, E value1)
        {
            m_value = value;
            m_value1 = value1;
        }

        private T m_value;
        private E m_value1;

        public T Value
        {
            get { return m_value; }
        }

        public E Value1
        {
            get { return m_value1; }
        }
    }
}
