﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Commons
{
    public class ReleaseInfo
    {
        public enum Shop { Google, Samsung, SlideMe, LG, Other}
        public enum GameLicenceType { Demo, FullVersion }
        public string GameName;
        public GameLicenceType LicenceType = GameLicenceType.Demo;
        public Shop ShopName = Shop.Google;

        public bool IsRateable()
        {
            return ShopName == Shop.Google || ShopName == Shop.SlideMe;
        }

        public bool CanBuyFullVersion()
        {
            return ShopName == Shop.Google || ShopName == Shop.SlideMe;
        }
    }
}
