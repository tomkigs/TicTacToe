﻿using UnityEngine;
using System.Collections;
using Lean;
using Commons.Grid;
using System;
using Commons;
using TicTacToeXAML;

namespace VikingChess
{
    public class Entity : EntityBase
    {
        Animator secondAnimator;
        public GridElemType Type;
        Rigidbody2D rb2D;
        public float moveTime = 0.1f;			//Time it will take object to move, in seconds.
        private float inverseMoveTime;			//Used to make movement more efficient.
        protected GameManager gameManager;
        Item snappedTo;
        public string SnappedToDesc;
        public static bool moventIsLasting = false;
        public event EventHandler<EventArgs<Entity>> movementEnded;
        public bool destroyed;

        new void Start()
        {
            base.Start();
            gameManager = GameManager.Instance;
            //Get a component reference to this object's Rigidbody2D
            rb2D = GetComponent<Rigidbody2D>();
            //By storing the reciprocal of the move time we can use it by multiplying instead of dividing, this is more efficient.
            inverseMoveTime = 1f / moveTime;

            if (Type == GridElemType.Cross)
            {
                secondAnimator = transform.Find("x_2").GetComponent<Animator>();
                secondAnimator.enabled = false;
            }
        }

        internal void OnOneAnimDone()
        {
            secondAnimator.enabled = true;
        }

        public Item SnappedTo
        {
            get { return snappedTo; }
            set {
                snappedTo = value;
                SnappedToDesc = snappedTo.x+", "+ snappedTo.y;
            }
        }

        public int X
        {
            get { return SnappedTo != null ? SnappedTo.x : -1; }
        }

        public int Y
        {
            get { return SnappedTo != null ? SnappedTo.y : -1; }
        }

        //Co-routine for moving units from one space to next, takes a parameter end to specify where to move to.
        public IEnumerator SmoothMovement(Vector3 end)
        {
            moventIsLasting = true;
            //Calculate the remaining distance to move based on the square magnitude of the difference between current position and end parameter. 
            //Square magnitude is used instead of magnitude because it's computationally cheaper.
            float sqrRemainingDistance = (transform.position - end).sqrMagnitude;

            //While that distance is greater than a very small amount (Epsilon, almost zero):
            while (sqrRemainingDistance > 0.01)
            {
                //Find a new position proportionally closer to the end, based on the moveTime
                Vector3 newPostion = Vector3.MoveTowards(rb2D.position, end, inverseMoveTime * Time.deltaTime);

                //Call MovePosition on attached Rigidbody2D and move it to the calculated position.
                rb2D.MovePosition(newPostion);

                //Recalculate the remaining distance after moving.
                sqrRemainingDistance = (transform.position - end).sqrMagnitude;

                //Return and loop until sqrRemainingDistance is close enough to zero to end the function
                yield return null;
            }
            moventIsLasting = false;
            if (movementEnded != null)
                movementEnded(this,new EventArgs<Entity>(this));
        }
        
        void OnDestroy()
        {
            destroyed = true;
        }
        virtual public bool IsMyEnemy(Entity other)
        {
            return false;
        }
    }
}
