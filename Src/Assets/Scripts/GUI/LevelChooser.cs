﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using Common.GUI;
using Commons;
using System;
using SnowmanBalls.GUI;

namespace SnowmanBalls
{
    public class LevelChooser : PauseBaseForm
    {
        public MainMenu menu;
        // Use this for initialization
        void Start()
        {
            allowGoBack = true;
            //Gui.RescaleTransform(transform);
            
            SetLocks();
        }

        private void SetLocks()
        {
            //ApplicationService.Instance.GameProgress.MaxEverPlayedLevel = 0;
            var levels = transform.Find("levels");
            for (int i = 2; i < 10; i++)
            {
                var level = levels.transform.Find("l" + i);
                var lockItem = level.Find("lock");
                if (lockItem != null)
                    lockItem.gameObject.SetActive(i > ApplicationService.Instance.GameProgress.MaxEverPlayedLevel);
            }
        }

        TimeTracker tr = new TimeTracker();
        public void LevelChosen()
        {
            if (tr.TotalSeconds < 0.5)
                return;
            var selectedBtn = EventSystem.current.currentSelectedGameObject as GameObject;
            Debug.Log("LevelChooser selectedBtn = " + selectedBtn.name);
            var parentTr = selectedBtn.GetComponentInParent<Transform>();
            if (parentTr.Find("lock") != null && parentTr.Find("lock").gameObject.activeInHierarchy)
            {
                Debug.Log("LevelChooser selectedBtn locked!");
                return;
            }
            base.Hide();

            var levelStr = parentTr.name.Substring("l".Length, parentTr.name.Length - "l".Length);
            //menu.LoadLevel(Convert.ToInt32(levelStr));
        }

        public void GotoMainMenu()
        {
            base.Hide();
        }
    }
}
