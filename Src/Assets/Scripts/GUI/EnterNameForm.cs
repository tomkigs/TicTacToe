﻿using Common.GUI;
using System;
using UnityEngine.UI;

public class EnterNameForm : PauseBaseForm
{
    public static EnterNameForm Instance;
    public string UserName
    {
        get
        {
            if (nameTxt == null)
                return "";
            return nameTxt.text;
        }
        set {
            if (nameTxt == null)
                return ;
            nameTxt.text = value;
        }
    }
    Text nameTxt;
    public event EventHandler Save;

    void Awake()
    {
        Instance = this;
        nameTxt = transform.Find("nameTxt").Find("Text").GetComponent<Text>();
        //var newGame = transform.FindChild ("NewGameBtn");//.GetComponent<Button> ();
    }

    public void OnSave()
    {
        if (Save != null)
            Save(this, EventArgs.Empty);
    }

    public void OnCancel()
    {
        Hide();
    }
}
