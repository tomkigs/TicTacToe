﻿using UnityEngine;
using System.Collections;
using VikingChess;
using GooglePlayGames;
using GooglePlayGames.BasicApi.Multiplayer;
using Commons;
using Assets.Scripts.Game;
using UnityEngine.UI;
using SnowmanBalls.GUI;
using Zybex.GUI;
//using GooglePlayGames.Native.Cwrapper;
using Assets.Scripts;

public class InGamePanel : MonoBehaviour
{
  GameManager mgr;
  Text undoHintNumber;
  Text suggestHintNumber;
  Reporter rep;
  public static InGamePanel Instance;
  // Use this for initialization
  void Start()
  {
    Instance = this;
    mgr = GameManager.Instance;
      #if UNITY_EDITOR
    if (GameObject.Find("Reporter") != null)
    {
      rep = GameObject.Find("Reporter").GetComponent<Reporter>();
      rep.show = false;
    }
#endif
    //rep.doShow ();

    suggestHintNumber = transform.Find("suggest_move").Find("corner_display").GetComponent<Text>();
    undoHintNumber = transform.Find("undo_move").Find("corner_display").GetComponent<Text>();
    Refresh();

    var networkGame = GameSettings.Instance.Gametype == TicTacToeXAML.GameType.TwoPlayersPlayServices;
    transform.Find("suggest_move").GetComponent<Button>().interactable = !networkGame;
    transform.Find("undo_move").GetComponent<Button>().interactable = !networkGame;
  }

  public void Refresh()
  {
    UpdateUndoHintInfo();
    UpdateSuggestHintInfo();
  }


  public void ShowReporter()
  {
    if (!rep.show)
      rep.doShow();
    else
      rep.doHide();
    //rep.show = !rep.show;
  }
  public void OnNewGame()
  {
    Forms.Instance.Pause();
  }
  const int MinOpponents = 1;
  const int MaxOpponents = 1;
  const int Variant = 0;  // default
  public void OnInviteFriend()
  {
    GamePlayService.InviteFriend();
  }

  public void OnUndoMove()
  {
    if (mgr.IsGameOver())
    {
      mgr.PlayBeep();
      return;
    }
    mgr.UndoMove();
    UpdateUndoHintInfo();
  }

  public void OnBackToMainMenu()
  {
    ApplicationService.Instance.ShowMainMenu();
  }

  public void OnZoomIn()
  {
    if (Camera.main.orthographicSize > 1)
      Camera.main.orthographicSize -= 0.5f;
    else
      mgr.PlayBeep();
    Debug.Log("OnZoomIn Camera.main.orthographicSize = " + Camera.main.orthographicSize);
  }

  public void OnZoomOut()
  {
    if (Camera.main.orthographicSize < 15)
      Camera.main.orthographicSize += 0.5f;
    else
      mgr.PlayBeep();
    Debug.Log("OnZoomOut Camera.main.orthographicSize = " + Camera.main.orthographicSize);

  }

  public void OnSuggestMove()
  {
    mgr.SuggestMove();
    UpdateSuggestHintInfo();
  }

  public void UpdateSuggestHintInfo()
  {
    var hintMove = mgr.GetSuggestMoveHintInfo();
    suggestHintNumber.text = hintMove.Current.ToString();
  }

  public void UpdateUndoHintInfo()
  {
    if (undoHintNumber == null)
      return;
    MaxSpecialHint hintUndo = mgr.GetUndoHintInfo();
    undoHintNumber.text = hintUndo.Current.ToString();
  }
}
