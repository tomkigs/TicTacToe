﻿using UnityEngine;
using UnityEngine.UI;
using Common.GUI;
using Commons;
using Assets.Scripts.GUI;
using VikingChess;
using TicTacToeXAML;
using SnowmanBalls.GUI;
using Assets.Scripts.Common.Logging;
using Assets.Scripts;
using CommonsPC;
using System.Linq;
using Assets.Scripts.Game;

namespace Zybex.GUI
{
  public class Forms : Gui
  {
    public static new Forms Instance;
    PauseBaseForm pauseForm;
    GameOverForm gameOverForm;
    PauseBaseForm levelFinishedForm;
    //PauseBaseForm gameFinishedForm;
    BaseForm newGameForm;
    BaseForm settingsForm;
    LeaderboardsForm leaderboardsForm;
    NetworkGameProposal networkGameProposal;
    public EnterNameForm enterNameForm;

    public static TimeTracker TrSinceClosed = new TimeTracker();

    void Awake()
    {
      base.OnAwake();
      Instance = this;

      try
      {
        pauseForm = InitForm<PauseForm>("pauseForm");
        gameOverForm = InitForm<GameOverForm>("gameOverForm");
        levelFinishedForm = InitForm<LevelFinishedForm>("levelFinishedForm");
        //gameFinishedForm = InitForm<GameFinishedFrom>("gameFinishedForm");
        settingsForm = InitForm<SettingsForm>("settingsForm");
        newGameForm = InitForm<NewGameForm>("newGameForm");
        leaderboardsForm = InitForm<LeaderboardsForm>("leaderboardsForm");
        networkGameProposal = InitForm<NetworkGameProposal>("remoteGameProposal");
        enterNameForm = InitForm<EnterNameForm>("enterNameForm");
        enterNameForm.Save += EnterNameForm_Save;
        if (leaderboardsForm != null)
        {
          leaderboardsForm.UsedInMainMenu = false;
          (gameOverForm).SetLeaderboardsForm(leaderboardsForm);
        }

        var forms = GameObject.Find("Forms");
        if (forms != null)
          forms.GetComponent<CanvasScaler>().scaleFactor = GetScaleFactor();// * .8f;

      }
      catch (System.Exception exc)
      {
        Log.AddError(exc);
      }
    }

    private void EnterNameForm_Save(object sender, System.EventArgs e)
    {
      SaveUserScore(enterNameForm.UserName);
    }

    void Start()
    {
      GoogleMobileAdsDemoScript.Instance.HideBanner();
    }

    void HandleDebugInput()
    {
#if UNITY_EDITOR
      if (Forms.Instance.enterNameForm != null && Forms.Instance.enterNameForm.Visible)
        return;
      if (Input.GetKeyDown(KeyCode.F))
        levelFinished = true;
      if (Input.GetKeyDown(KeyCode.O))
        GameOver();
      if (Input.GetKeyDown(KeyCode.Escape))
        Pause();
      //if (Input.GetKeyDown(KeyCode.G))
      //    GameFinished();
      if (Input.GetKeyDown(KeyCode.L))
      {
        gameOverForm.SetLeaderboardEntry(GameManager.Instance.CreateWinInfo(TicTacToeGrid.Instance.crossGame));
        ShowLeaderboards();
        ShowKeyboard();
      }
      if (Input.GetKeyDown(KeyCode.N))
        NewGame(false);
#endif
    }

    TouchScreenKeyboard keyboard;
    bool useTouchScreenKeyboard = false;
    void Update()
    {
      HandleDebugInput();

      if (levelFinishedForm != null && levelFinished && trLevelFinished.TotalSeconds > 0.5f)
      {
        if (ApplicationService.Instance.GameProgress.CurrentLevel < ApplicationService.Instance.NumberOfLevels)
        {
          if (!levelFinishedForm.Visible)
            LevelFinished();
        }
        //else if(!gameFinishedForm.Visible)
        //     GameFinished();
        levelFinished = false;
      }
      if (useTouchScreenKeyboard)
      {
        string userName = "";
        if (keyboard != null && keyboard.done)
        {
          keyboard = null;
          userName = keyboard.text;
          SaveUserScore(userName);
        }
      }
    }

    void SaveUserScore(string userName)
    {
      Debug.Log("SaveUserScore userName = " + userName);
      if (userName.Any())
      {
        leaderboardsForm.SaveUserScore(userName);

        HideCurrent();
        ShowForm(leaderboardsForm);
      }
      else
        GameManager.Instance.PlayBeep();
    }

    public override bool AnyFormShown()
    {
      return base.AnyFormShown() || (NetworkGameProposal.Instance && NetworkGameProposal.Instance.Visible);
    }

    public void Pause()
    {
      if (AnyFormShown())
        shownForm.Hide();
      else if (pauseForm != null)
        ShowForm(pauseForm);
    }
    internal void GameOver(WinInfo info, PlayerGame humanGame, PlayerGame wininingPlayer)
    {
      gameOverForm.Init(info, humanGame, wininingPlayer);
      GameOver();
    }

    public void GameOver()
    {
      if (shownForm != gameOverForm || !gameOverForm.Visible)
      {
        //AudioService.Instance.PlaySound("game_over");
        ShowForm(gameOverForm);
      }
    }

    public void LevelFinished()
    {
      ShowForm(levelFinishedForm);
    }

    //public void GameFinished()
    //{
    //    ShowForm(gameFinishedForm);
    //}

    public void HideCurrent()
    {
      if (AnyFormShown())
      {
        shownForm.Hide();
        shownForm = null;
      }
    }

    public void NewGame(bool networkRevenge)
    {

      HideCurrent();
      if (networkRevenge)
      {
        GameManager.Instance.OnNewGame();
#if UNITY_ANDROID
				GamePlayService.Rematch ();
#endif
      }
      else
      {
        if (GameSettings.Instance.Gametype == GameType.TwoPlayersPlayServices)
        {
          GameSettings.Instance.InitNetworkGame = true;
          ApplicationService.Instance.LoadLevel(1);
        }
        else
          GameManager.Instance.OnNewGame();
      }

      //ShowForm(newGameForm);
    }

    public void Settings()
    {
      ShowForm(settingsForm);
    }

    public void MainMenu()
    {
      ApplicationService.Instance.ShowMainMenu();

    }

    void ShowKeyboard()
    {
      if (useTouchScreenKeyboard)
        keyboard = TouchScreenKeyboard.Open("user", TouchScreenKeyboardType.Default, false, false, false);
      else
      {
        leaderboardsForm.Hide();
        ShowForm(enterNameForm);
      }
    }

    //called when btn 'Add to Leaderboards is clicked'
    public void ShowLeaderboards()
    {
      HideCurrent();
      if (LiderboardManager.Instance.GetSet(true) == null)
      {
        MessageBox.Instance.Show("Failed to read data", true);
        return;
      }
      leaderboardsForm.SetActiveTab(GameSettings.Instance.DifficultyLevel == GameSettings.Difficulty.Easy);
      if (leaderboardsForm.Mode == LeaderboardsForm.LiderboardEntityShowMode.OnBoard)
      {
        Debug.Log("user shall be added to the list of best players!, TouchScreenKeyboard !!!");
        ShowKeyboard();
      }
      else
        ShowForm(leaderboardsForm);
    }
  }
}

