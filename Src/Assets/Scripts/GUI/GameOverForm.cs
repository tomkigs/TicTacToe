﻿using UnityEngine;
using System.Collections;
using Common.GUI;
using Common;
using TicTacToeXAML;
using Assets.Scripts;
using CommonsPC;
using UnityEngine.UI;
using Assets.Scripts.Game;

namespace Zybex.GUI
{
  public class GameOverForm : PauseBaseForm
  {
    Transform middle;
#if !UNITY_WEBGL
    LeaderboardsForm leaderboardsForm;
#endif
    Vector3 orgMMPos = Vector3.zero;

    void Awake()
    {
      Debug.Log("GameOverForm awake start");
      //Revenge
      base.OnAwake();
      //Instance = this;
      Debug.Log("GameOverForm awake end");
    }
    // Use this for initialization
    void Start()
    {
      SetTitle("GameOver");
    }

    public void SetLeaderboardsForm(LeaderboardsForm leaderboardsForm)
    {
#if !UNITY_WEBGL
      this.leaderboardsForm = leaderboardsForm;
#endif
    }

    bool ProposeRevange()
    {
      return GameSettings.Instance.Gametype == GameType.TwoPlayersPlayServices && wininingPlayer.typeOfElem != TicTacToeGrid.Instance.MatchEntity.ElemType;
    }

    public void OnNewGame()
    {
      Forms.Instance.NewGame(ProposeRevange());
    }

    //bool ShowAddScoreToLeaderBoards;
    PlayerGame wininingPlayer;
    internal void Init(WinInfo info, PlayerGame humanGame, PlayerGame wininingPlayer)
    {
      Debug.Log("GameOverForm Init start");
      this.wininingPlayer = wininingPlayer;
      middle = transform.Find("middle");

      var newGame = transform.Find("NewGameBtn");//.GetComponent<Button> ();
      newGame.gameObject.SetActive(!ProposeRevange());
      //newGame.transform.FindChild("Text").GetComponent<Text>().text = ProposeRevange() ? "Revenge" : "New Game";

      var mainMenu = transform.Find("MainMenuBtn");//.GetComponent<Button> ();
      if (orgMMPos == Vector3.zero)
        orgMMPos = mainMenu.transform.position;
      if (ProposeRevange())
        mainMenu.transform.position = middle.position;
      else
        mainMenu.transform.position = orgMMPos;

      Debug.Log("GameOverForm Init SetLeaderboardEntry start");
      GameType gameType = GameSettings.Instance.Gametype;
      bool humanWon = humanGame != null && wininingPlayer != null && gameType == GameType.OnePlayer
        && humanGame.typeOfElem == wininingPlayer.typeOfElem;
      bool easy = GameSettings.Instance.difficultyLevel == GameSettings.Difficulty.Easy;
#if !UNITY_WEBGL
      if (humanWon)
      {
        SetLeaderboardEntry(info);
      }
      else
      {

        leaderboardsForm.SetLiderboardEntry(null);
      }

      Debug.Log("GameOverForm Init SetLeaderboardEntry ends");
      var lbBtn = transform.Find("LeaderboardsBtn");
      if (gameType == GameType.OnePlayer)
      {
        var rc = lbBtn.GetComponent<RectTransform>();
        var txt = lbBtn.transform.Find("Text").GetComponent<Text>();
        if (leaderboardsForm.Mode == LeaderboardsForm.LiderboardEntityShowMode.OnBoard)
        {
          txt.text = "Add score to leaderboards";
          rc.sizeDelta = new Vector2(500, 48);
        }
        else
        {
          txt.text = "Leaderboards";
          rc.sizeDelta = new Vector2(300, 48);
        }

        leaderboardsForm.SetActiveTab(easy);
      }
      else
        lbBtn.gameObject.SetActive(false);
#endif
    var status = transform.Find("status");
      var winnerValue = status.Find("winnerValue").GetComponent<Text>();
      winnerValue.text = info.Winner;

      var movesValue = status.Find("movesValue").GetComponent<Text>();
      movesValue.text = info.Moves.ToString();

      var timeValue = status.Find("timeValue").GetComponent<Text>();
      timeValue.text = info.TimeString;

      base.Show();
      Debug.Log("GameOverForm Init ends");
    }

    public void SetLeaderboardEntry(WinInfo info)
    {
      bool easy = GameSettings.Instance.difficultyLevel == GameSettings.Difficulty.Easy;
      var entityToAdd = LiderboardManager.Instance.CreateEntity(info.Time,
                          info.TimeString, easy ? GameDefines.ModeEasy : GameDefines.ModeNormal);
      entityToAdd.Name = "";
#if UNITY_ANDROID
      if (Social.localUser.userName.Length > 0)
        entityToAdd.Name = Social.localUser.userName;


            entityToAdd.Platform = (int)LiderboardManager.Platform.Android;
#endif
      entityToAdd.NumberOfMoves = info.Moves;
#if !UNITY_WEBGL
      leaderboardsForm.SetLiderboardEntry(entityToAdd);
#endif
    }

    public void OnClose()
    {
      base.Hide();
    }

  }
}
