﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Linq;
using UnityEngine.UI;
using System.Collections.Generic;
//using Zybex.Events;
using System;
using Commons;
using Common;
using Assets.Scripts;
using VikingChess;
using Assets.Scripts.Game;
using TicTacToeXAML;

namespace SnowmanBalls
{
	namespace GUI
	{
		public class HUD : MonoBehaviour {
            Text turnStatusLbl;
           
            Text nextMoveByValue;
            Text numberOfMovesValue;
            Text difficultyValue;
            Text timeValue;
            //HealthBar progress;

            void Init()
            {
                //Lean.LeanLocalization.Instance.SetLanguage("Other");
				if (GameObject.Find ("turnStatusLbl") != null) {
					turnStatusLbl = GameObject.Find ("turnStatusLbl").GetComponent<Text> ();
				}
                nextMoveByValue = GameObject.Find("nextMoveByValue").GetComponent<Text>();
                numberOfMovesValue = GameObject.Find("movesNumberValue").GetComponent<Text>();
                difficultyValue = GameObject.Find("difficultyValue").GetComponent<Text>();
                timeValue = GameObject.Find("timeValue").GetComponent<Text>();

				bool hideUpperStatus = GameSettings.Instance.Gametype != GameType.OnePlayer;
				difficultyValue.gameObject.SetActive (!hideUpperStatus);
				GameObject.Find ("difficultyLabel").SetActive (!hideUpperStatus);
				timeValue.gameObject.SetActive (!hideUpperStatus);
				GameObject.Find ("timeLabel").SetActive (!hideUpperStatus);

                LanguageManager.SetLanguage(ApplicationService.LoadLanguage());

				GetComponent<CanvasScaler>().scaleFactor = Gui.GetScaleFactor() * 1.25f;

				var nextMoveByLabel = GameObject.Find("nextMoveByLabel").GetComponent<Text>();
				var networkGame = GameSettings.Instance.Gametype == TicTacToeXAML.GameType.TwoPlayersPlayServices;
				nextMoveByLabel.text = networkGame ? "You play as:" : "Next move by:";
                //LanguageManager.SetText(levelName, ApplicationService.Instance.GetLevelNumberFromName().ToString());
				turnStatusLbl.text = "";
            }

            void Update()
            {

        var networkGame = false;
        GridElemType elemType = GridElemType.None;
#if UNITY_ANDROID
        networkGame = GameSettings.Instance.Gametype == TicTacToeXAML.GameType.TwoPlayersPlayServices;
        elemType = TicTacToeGrid.Instance.MatchEntity.ElemType;
#endif
        nextMoveByValue.text = networkGame ? (elemType).ToString().ToUpper() : TicTacToeGrid.Instance.CrossTurn ? "X" : "O";
                
				if (GameSettings.Instance.Gametype == GameType.TwoPlayersPlayServices) 
				{
					if (TicTacToeGrid.Instance.MatchEntity.DeserializeCount == 0) 
					{
						turnStatusLbl.text = "Please wait...";
					}
					else
						turnStatusLbl.text = GamePlayService.CanPlay() ? "Your turn" : "Opponent turn";
				}
				
                numberOfMovesValue.text = TicTacToeGrid.Instance.GetNumberOfElems().ToString();
                difficultyValue.text = GameSettings.Instance.DifficultyLevel == GameSettings.Difficulty.Easy ? "Easy" : "Normal";
				if(!ApplicationService.Instance.GamePaused && !GameManager.Instance.IsGameOver())
                	timeValue.text = GameManager.Instance.GameLasting.GetTotalTime();
                ////LanguageManager.SetText(scoreLbl,  "Score", ": " + ApplicationService.Instance.GameProgress.Score);
                //if (GameManager.Instance.Statistics != null)
                //{
                //    //progress.SetCurrentHealth(GameManager.Instance.Statistics.PercentJobDone);
                //    //LanguageManager.SetText(doneLbl, "Done", ": " + GameManager.Instance.Statistics.PercentJobDone + " %");
                //    var left = GameManager.Instance.Statistics.TimeLeft.ToString("#.##");
                //    if (left == "")
                //        left = "0.00";
                //    LanguageManager.SetText(timeLeftLbl, "Time Left", ": " + left + " s");
                //}

                //levelName.color = GameSettings.Instance.DifficultyLevel == GameSettings.Difficulty.Easy ? Color.blue : Color.red;
            }

			// Use this for initialization
			void Start () {
                Init();

			}

			

			public void ButtonPressed()
			{
				var selectedBtn = EventSystem.current.currentSelectedGameObject as GameObject;
				Debug.Log ("selectedBtn = "+selectedBtn.name);
			}


            public void OnReload()
            {
                ApplicationService.Instance.ReloadLevel();

            }

        }
	}
}
