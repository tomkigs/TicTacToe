﻿using UnityEngine;
using System.Collections;
using Commons;
using Commons.GUI;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Common;
using Assets.Scripts;
using GooglePlayGames;
using CommonsPC;
using TicTacToeXAML;
using Assets.Scripts.GUI;
using UnityEngine.SceneManagement;

namespace SnowmanBalls
{
  namespace GUI
  {
    public class MainMenu : MainMenuBase
    {
      NewGameForm levelChooser;
      LeaderboardsForm leaderboards;
      public string VersionDigit = "1.71";
      public static MainMenu Instance;
      Reporter rep;
      NetworkGameProposal networkGameProposal;

      void Awake()
      {
        Instance = this;
        //GameObject.Find("Hud").SetActive(false);
      }
      void Start()
      {
        if (GameObject.Find("Reporter") != null)
        {
          rep = GameObject.Find("Reporter").GetComponent<Reporter>();
          rep.show = false;
        }
        if(NetworkGameProposal.Instance)
          NetworkGameProposal.Instance.Hide();//create instance, and hide it
#if UNITY_ANDROID
        GamePlayService.Init();
#endif
        InitLang();
        base.OnStart();

        var sf = Gui.GetScaleFactor();
        if (sf > 1)
        {
          GetComponent<CanvasScaler>().scaleFactor = sf;
          Debug.Log("scaleFactor = " + sf);
        }
        else
          GetComponent<CanvasScaler>().scaleFactor = sf;
        //if (Screen.width > Gui.DesignedWidth)
        //{
        //   transform.GetChildByName("title1").localScale = new Vector3(Gui.GetScaleFactor(), Gui.GetScaleFactor(), 1);
        //	transform.GetChildByName("title2").localScale = new Vector3(Gui.GetScaleFactor(), Gui.GetScaleFactor(), 1);
        //}
        var startTr = transform.Find("bkg");
        levelChooser = startTr.Find("newGameForm").GetComponent<NewGameForm>();
        levelChooser.Hide();

        leaderboards = startTr.Find("leaderboardsForm").GetComponent<LeaderboardsForm>();
        leaderboards.Hide();

        networkGameProposal = startTr.Find("remoteGameProposal").GetComponent<NetworkGameProposal>();

        Translate();


#if UNITY_ANDROID
        LiderboardManager.Instance.ReloadAsync();
        GamePlayService.GetAllInvitations();
#else
        transform.GetChildByName("title1").GetComponent<Text>().text = "Tic Tac Toe";
#endif
      }

      private static void InitLang()
      {
        var lang = ApplicationService.LoadLanguage();
        if (string.IsNullOrEmpty(lang))
        {
          lang = "English";
          ApplicationService.SaveLanguage(lang);
        }
        LanguageManager.SetLanguage(lang);
      }

      public void ShowAchievementsUI()
      {
        #if UNITY_ANDROID
        GamePlayService.ShowAchievementsUI();
        #endif
      }

      public void OnShowLogs()
      {
        if (!rep.show)
          rep.doShow();
        else
          rep.doHide();
      }

      private void Translate()
      {
        //LanguageManager.SetText("ChooseLevelBtn", "ChooseLevel");
        //LanguageManager.SetText("PlayBtn", "Play");
        //LanguageManager.SetText("RateMeBtn", "RateMe");

        //var tr = Lean.LeanLocalization.GetTranslation("MoreGames");
        //if (Lean.LeanLocalization.Instance.CurrentLanguage == "Other")
        //    tr.Text = "Więcej\nGier";
        //else
        //    tr.Text = "More\nFree\nGames";
        //LanguageManager.SetText("MoreGamesBtn", "MoreGames");

        //LanguageManager.SetChildText(this.levelChooser.gameObject, "title", "ChooseLevel");
        //LanguageManager.SetChildText(this.gameObject, "version", "Version", ": " + VersionDigit);
        //LanguageManager.SetChildText(this.gameObject, "music", "MusicBy", ": DST");
        //LanguageManager.SetText(loading.gameObject, "Loading");
      }

      // Update is called once per frame
      void Update()
      {
        base.OnUpdate();
      }

      public void ShowNewGameForm()
      {
        Debug.Log("OnNewGame");
        levelChooser.Show();
      }

      public void OnLeaderboards()
      {
        if (LiderboardManager.Instance.GetSet(true) == null)
        {
          MessageBox.Instance.AutoScale = true;
          MessageBox.Instance.Show("Failed to read data", true); ;
          return;
        }
        leaderboards.Show();
      }

      public void ClearInvitations()
      {
#if UNITY_ANDROID
        GamePlayService.DeleteAllInvitations();
#endif
      }

      public void ShowInvitations()
      {
#if UNITY_ANDROID
      GamePlayService.ShowInvitations();
#endif

      }

    

      //public void OnLanguageChanged()
      //{
      //    var selectedBtn = EventSystem.current.currentSelectedGameObject as GameObject;
      //    Debug.Log("selected lang Btn = " + selectedBtn.name);
      //    string lang = "";
      //    if (selectedBtn.name.EndsWith("PlnBtn"))
      //    {
      //        lang = "Other";
      //    }
      //    else
      //        lang = "English";
      //    ApplicationService.SaveLanguage(lang);
      //    LanguageManager.SetLanguage(lang);
      //    Translate();
      //}
    }
  }
}
