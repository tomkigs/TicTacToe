﻿using UnityEngine;
using System.Collections;
using Common.GUI;
using Zybex.GUI;
using System;
using Commons;

public class SettingsForm : BaseForm
{
    public bool UsedInMainMenu;
    // Use this for initialization
    void Start()
    {
        
        var sett = transform.Find("settingsPanel");
        var btnToHide = "PlayButton";

        if (UsedInMainMenu)
        {
            Gui.RescaleTransform(transform);
            btnToHide = "BackButton";
        }

        var btn = sett.Find(btnToHide);
        if(btn != null)
            btn.gameObject.SetActive(false);
    }

    public override void Show()
    {
        var sett = transform.Find("settingsPanel").GetComponent< SettingsPanel>();
        sett.Load();
        base.Show();
    }

    public event EventHandler OnHidden;

    public override void Hide()
    {
        var sett = transform.Find("settingsPanel").GetComponent<SettingsPanel>();
        sett.Save();

        //if(BackgroundManager.Instance != null)
        //    BackgroundManager.Instance.ActivateChildren();

        base.Hide();
        if (OnHidden != null)
            OnHidden(this, EventArgs.Empty);
    }
}