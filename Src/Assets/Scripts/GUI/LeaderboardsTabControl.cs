﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Commons;
using SnowmanBalls.GUI;
using TicTacToeXAML;
using System.Linq;
using UnityEngine.EventSystems;
using CommonsPC;
using CommonsPC.Leaderboards;

public class LeaderboardsTabControl : MonoBehaviour {
	Toggle easyBtn;
	Toggle normalBtn;
	ToggleGroup group;
	GameObject panelEasy;
	GameObject panelNormal;
	Color orgTabTextColor;
	Color orgEntityTextColor = Color.white;

	void Awake()
	{
		normalBtn = transform.Find("ToggleNormal").GetComponent<Toggle>();
		easyBtn = transform.Find("ToggleEasy").GetComponent<Toggle>();

		panelNormal = transform.Find ("PanelNormal").gameObject;
		panelEasy = transform.Find ("PanelEasy").gameObject;

		orgTabTextColor = GetTabText (true).color;
		group = GetComponent<ToggleGroup>();
	}

    // Use this for initialization
    void Start () {
		//OnTabActivated ();//activate panel based on isOn of toggle
		Debug.Log ("LeaderboardsTabControl Start");
		Invoke("OnTabActivated", .15f);//otherwise exc were thrown
    }

	Text GetTabText(bool easy)
	{
		var toggle = easy ? easyBtn : normalBtn;
		return toggle.transform.Find ("Label").GetComponent<Text> ();
	}
	//bool currentEasy;
	public void SetStartingTab(bool easy)
	{
		//currentEasy = easy;
		Debug.Log ("LeaderboardsTabControl SetStartingTab easy = "+easy);
		normalBtn.isOn = !easy;
		easyBtn.isOn = easy;
	}
	//int activationCount;

	public void OnTabActivated()
	{
		//if (activationCount > 0  && activationCount <= 2)
		//	Invoke("OnTabActivated", .15f);//hack
		//activationCount++;

		Debug.Log("OnTabActivated normalBtn on="+normalBtn.isOn);
		panelEasy.SetActive (!normalBtn.isOn);
		panelNormal.SetActive (normalBtn.isOn);
		SetPanelData(!normalBtn.isOn);

		var txt = GetTabText (!normalBtn.isOn);
		txt.color = Color.white;
		var txtOther = GetTabText (normalBtn.isOn);
		txtOther.color = orgTabTextColor;
	}
	LiderboardEntity highlightedEntity;
	LeaderboardsForm.LiderboardEntityShowMode mode;
	public void SetHighlightedEntity(LiderboardEntity entity, LeaderboardsForm.LiderboardEntityShowMode mode)
	{
		Debug.Log("SetHighlightedEntity ="+entity);
		highlightedEntity = entity;
		this.mode = mode;
	}

    void SetPanelData(bool isPanelEasy)
    {
		//Debug.Log ("SetPanelData starts easy = "+isPanelEasy);
		var panelShown = isPanelEasy ? panelEasy : panelNormal;
		var entries = panelShown.transform.GetChildrenByNameStart("leaderboardEntry");
        var provider = LiderboardManager.Instance;
		var set = provider.GetSet(!isPanelEasy);
		if (set == null) {
			
			return;
		}
		
        for(int i=0; i< set.Data.Count; i++)
        {
			var entryName = "leaderboardEntry" + (i + 1);
			var en = entries.Where(e => e.name == entryName).FirstOrDefault();
			if (en != null) {
				//Debug.Log ("SetPanelData bind for  "+en.name);
				var le = en.GetComponent<LeaderboardEntry> ();
				if (le == null) {
					Debug.LogError ("SetPanelData le == null!!!");
					break;
				}
				le.Bind ((i + 1).ToString (), set.Data [i]);
				if (highlightedEntity != null && mode == LeaderboardsForm.LiderboardEntityShowMode.OnBoard && highlightedEntity.Equals (set.Data [i]))
					le.Highlight ();
			} else
				Debug.LogError ("leaderboardEntry "+entryName + " not found in GUI!");
        }
		var yc = panelShown.transform.Find ("leaderboardEntryYourScore");
		if(yc != null)
		{
			var your = yc.GetComponent<LeaderboardEntry> ();
			if (mode == LeaderboardsForm.LiderboardEntityShowMode.BeneathBoard && highlightedEntity != null
			    && ((highlightedEntity.SetName == "Normal" && !isPanelEasy) || (highlightedEntity.SetName == "Easy" && isPanelEasy))) {
				highlightedEntity.Name = "Your Score";
				your.Bind ("", highlightedEntity);
			} else
				your.Clear ();
		}
			
		//Debug.Log ("SetPanelData ends easy = "+isPanelEasy);
    }

}
