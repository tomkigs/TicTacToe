﻿using Common.GUI;
using Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SnowmanBalls.GUI;
using Commons.Effects;

namespace Assets.Scripts.GUI
{
  class NewGameForm : BaseForm
  {
    Toggle onePlayer;
    Toggle twoPlayersSameDevice;
    Toggle twoPlayersPlayService;

    Toggle difficultyEasy;
    Toggle difficultyNormal;
    public bool UsedInMainMenu = false;
    Transform difficultyParent;

    void Start()
    {
      allowGoBack = true;

      //did not looked nice in full hd
      //if(UsedInMainMenu)
      //         	SetScale();

      onePlayer = transform.GetChildByName("OnePlayer").GetComponent<Toggle>();
      twoPlayersSameDevice = transform.GetChildByName("TwoPlayersSameDevice").GetComponent<Toggle>();
      twoPlayersPlayService = transform.GetChildByName("TwoPlayersPlayService").GetComponent<Toggle>();

      difficultyEasy = transform.GetChildByName("Easy").GetComponent<Toggle>();
      difficultyNormal = transform.GetChildByName("Normal").GetComponent<Toggle>();

      if (GameSettings.Instance.Gametype == TicTacToeXAML.GameType.OnePlayer)
        onePlayer.isOn = true;
      else if (GameSettings.Instance.Gametype == TicTacToeXAML.GameType.TwoPlayersSameDevice)
        twoPlayersSameDevice.isOn = true;
      else
        twoPlayersPlayService.isOn = true;

      var ShowInvitationsBtn = transform.GetChildByName("ShowInvitationsBtn");//.GetComponent<Scaler>();
      ShowInvitationsBtn.gameObject.SetActive(false);
#if UNITY_ANDROID
      ShowInvitationsBtn.gameObject.SetActive(GamePlayService.invites != null && GamePlayService.invites.Any());
#endif
      difficultyParent = transform.GetChildByName("difficulty");

    }

    public void OnShowInv()
    {
      twoPlayersPlayService.isOn = true;
      GamePlayService.ShowInvitations();
      Hide();
    }

    void Update()
    {
      difficultyParent.gameObject.SetActive(onePlayer.isOn);
    }

    public void StartGame()
    {
      Debug.Log("onePlayer = " + onePlayer.isOn + " difficultyEasy = " + difficultyEasy);

      if (onePlayer.isOn)
        GameSettings.Instance.Gametype = TicTacToeXAML.GameType.OnePlayer;
      else if (twoPlayersSameDevice.isOn)
        GameSettings.Instance.Gametype = TicTacToeXAML.GameType.TwoPlayersSameDevice;
      else
      {
        if (!ApplicationService.Instance.PlayGamesPlatformActivated)
        {
          Debug.LogError("StartGame - !PlayGamesPlatformActivated");
          MessageBox.Instance.Show("Trying to connect to the play services...", true);
          GamePlayService.Init();
          return;
        }
        else
        {
          GameSettings.Instance.Gametype = TicTacToeXAML.GameType.TwoPlayersPlayServices;
          GameSettings.Instance.InitNetworkGame = true;
        }
      }

      GameSettings.Instance.DifficultyLevel = difficultyEasy.isOn ? GameSettings.Difficulty.Easy : GameSettings.Difficulty.Normal;

      //MainMenu.Instance.ShowLoading ();
      transform.GetChildByName("PlayBtn").transform.Find("Text").GetComponent<Text>().text = "Loading..."; ;
      ApplicationService.Instance.LoadLevel(1);

    }

    public void OnClose()
    {
      this.Hide();
    }
  }
}
