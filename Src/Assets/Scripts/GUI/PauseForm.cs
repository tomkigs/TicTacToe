﻿using UnityEngine;
using System.Collections;
using Common.GUI;
using Commons;
using Common;
using Assets.Scripts;
using Zybex.GUI;
using VikingChess;

namespace SnowmanBalls.GUI
{
    public class PauseForm : PauseBaseForm
    {
        void Awake()
        {
            allowGoBack = true;
            FullEnginePause = true;
            base.OnAwake();
        }
        // Use this for initialization
        void Start()
        {
            SetTitle("GamePaused");
        }
		public override void Reload()
		{
			//GameManager.Instance.OnNewGame ();
			Forms.Instance.NewGame(false);
		}
        public override void Hide()
        {
            //if(Tutorial.Instance.restoreTut)
            //    Tutorial.Instance.gameObject.SetActive(true);
            
            base.Hide();
            //GameObject.Find("snow").SetActive(GameSettings.Instance.UseDynamicBackgrounds);
        }


        public void PlayNextLevel()
        {
            base.Hide();
            ApplicationService.Instance.LoadNextLevel();
        }

        int playNextLevelInvisibleCounter;
        public void OnPlayNextLevelInvisible()
        {
            playNextLevelInvisibleCounter++;
            if (playNextLevelInvisibleCounter == 3)
            {
                Forms.Instance.LevelFinished();
                Hide();
            }
                //PlayNextLevel();
        }

        public void Settings()
        {
            Debug.Log("Settings!!");
            //App.ReloadLevel();
            Forms.Instance.Settings();
        }

    }

}