﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using UnityEngine.UI;
using Assets.Scripts.Common.Logging;

public class SettingsPanel : MonoBehaviour {
    Toggle useDynamicBackgrounds;
    Toggle difficultyNormal;
    Toggle difficultyEasy;
    // Use this for initialization
    void Start ()
    {
        SetMembers();
        Load();
    }

    private void SetMembers()
    {
        if (useDynamicBackgrounds != null)
            return;
        var other = transform.Find("Other");
        Debug.Log("other = " + other);
        useDynamicBackgrounds = other.Find("useParticles").GetComponent<Toggle>();

        var diff = transform.Find("Difficulty");
        Debug.Log("Difficulty = " + diff);

        difficultyNormal = diff.Find("normal").GetComponent<Toggle>();
        difficultyEasy = diff.Find("easy").GetComponent<Toggle>();
    }

    // Update is called once per frame
    void Update () {}

    public void Load()
    {
        try
        {
            if(useDynamicBackgrounds == null)
                SetMembers();

            useDynamicBackgrounds.isOn = GameSettings.Instance.UseDynamicBackgrounds;

            //toggle group buggy ?
            if (GameSettings.Instance.DifficultyLevel == GameSettings.Difficulty.Normal)
            {
                difficultyNormal.isOn = true;
                difficultyEasy.isOn = false;
            }
            else
            {
                difficultyNormal.isOn = false;
                difficultyEasy.isOn = true;
            }
            Debug.Log(" difficultyNormal.isOn = " + difficultyNormal.isOn);
        }
        catch (System.Exception exc)
        {
			Log.AddError(exc);
        }
    }

    public void Save()
    {
        GameSettings.Instance.UseDynamicBackgrounds = useDynamicBackgrounds.isOn;
        GameSettings.Instance.DifficultyLevel = difficultyNormal.isOn ? GameSettings.Difficulty.Normal : GameSettings.Difficulty.Easy;
    }
}
