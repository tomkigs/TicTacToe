﻿using UnityEngine;
using System.Collections;
using Common.GUI;
using CommonsPC.Leaderboards;
using System;
using Assets.Scripts.Common.Logging;
using CommonsPC;
using System.Linq;

public class LeaderboardsForm : BaseForm
{
  public bool UsedInMainMenu;
  public enum LiderboardEntityShowMode
  {
    Hide, OnBoard, BeneathBoard
  }
  private LiderboardEntity entity;
  LiderboardManager manager;

  // Use this for initialization
  void Start()
  {
    allowGoBack = true;
    manager = LiderboardManager.Instance;

    //did not looked nice in full hd
    //if (UsedInMainMenu)
    //	SetScale();
  }

  public LiderboardEntityShowMode Mode
  {
    get;
    private set;
  }

  public void SetLiderboardEntry(LiderboardEntity entity)
  {
    manager = LiderboardManager.Instance;
    this.entity = entity;
    CalculateMode();
  }

  bool startingTabEasy;
  public void SetActiveTab(bool easy)
  {
    startingTabEasy = easy;
  }

  public void OnClose()
  {
    this.Hide();
  }

  public override void Show()
  {

    base.Show();

    //entity = manager.GetSet (true).Data[0];//TODO
    //mode = LiderboardEntityShowMode.OnBoard;
    var tabs = transform.Find("tabControl").GetComponent<LeaderboardsTabControl>();
    tabs.SetHighlightedEntity(entity, Mode);
    tabs.SetStartingTab(startingTabEasy);
    tabs.OnTabActivated();
  }

  public void SaveUserScore(string name)
  {
    if (entity != null)
    {
      entity.Name = name;
      manager.AddToList(entity);
    }
  }

  void CalculateMode()
  {
    Mode = LiderboardEntityShowMode.Hide;

    try
    {
      if (entity != null)
      {
        if (manager.ShouldAddToList(entity))
          Mode = LiderboardEntityShowMode.OnBoard;//
        else
        {
          Mode = LiderboardEntityShowMode.BeneathBoard;
        }
      }
    }
    catch (Exception exc)
    {
      Log.AddError(exc);
    }

    Debug.Log("CalculateMode end mode = " + Mode + " entity = " + entity);
  }

}
