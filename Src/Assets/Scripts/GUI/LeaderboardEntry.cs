﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using CommonsPC.Leaderboards;
using System;
using CommonsPC;
using Assets.Scripts.Common.Logging;

public class LeaderboardEntry : MonoBehaviour {
    public Sprite appleImage;
    public Sprite winImage;
    public Sprite androidImage;
    Image img;
    Text name_;
    Text score;
    Color imgColor;

	void Awake()
	{
		EnsureMembers ();
		//imgColor = img.color;

		Clear ();
	}

	public void Clear()
	{
        EnsureMembers();
        name_.text = "";
		score.text = "";
		Color hidden = new Color(255, 255, 255, 0);
        

        img.color = hidden;
	}
    // Use this for initialization
    void Start () {
    }

	void EnsureMembers()
	{
		if (name_ != null)
			return;
		img = transform.Find("PlatformImage").GetComponent<Image>();
        imgColor = img.color;
        name_ = transform.Find("Name").GetComponent<Text>();
		score = transform.Find("Score").GetComponent<Text>();
	}

	public void Highlight()
	{
		EnsureMembers ();
        name_.color = Color.red;
		score.color = Color.red;
	}

    void SetActive(bool active)
    {
        Debug.Log("SetActive "+ active);
        try
        {
            img.gameObject.SetActive(active);
            name_.gameObject.SetActive(active);
            score.gameObject.SetActive(active);
        }
        catch (Exception exc)
        {
            Log.AddError(exc);
        }
    }

	LiderboardEntity liderboardEntity;
    internal void Bind(string index, LiderboardEntity liderboardEntity)
    {
        try
        {
			//Debug.Log("LeaderboardEntry bind "+liderboardEntity.Name);
			this.liderboardEntity = liderboardEntity;
			EnsureMembers();
            
            //img.color = imgColor;
            name_.text = index;
			if(index.Length > 0)
				name_.text += ". ";
            name_.text += liderboardEntity.Name;
            score.text = liderboardEntity.ScoreDisplay;
            if ((int)LiderboardManager.Platform.Android == liderboardEntity.Platform)
                img.sprite = androidImage;
            else if ((int)LiderboardManager.Platform.Windows8 == liderboardEntity.Platform)
                img.sprite = winImage;
            else
                img.sprite = appleImage;
        }
        catch (Exception exc)
        {
            Log.AddError(exc);
        }
    }
}
