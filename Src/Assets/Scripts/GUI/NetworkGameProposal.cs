﻿using UnityEngine;
using System.Collections;
using Common.GUI;
using UnityEngine.SceneManagement;
using Assets.Scripts;
using Commons;
using VikingChess;
using GooglePlayGames.BasicApi.Multiplayer;
using System;
using UnityEngine.UI;
using Assets.Scripts.Common.Logging;

public class NetworkGameProposal : BaseForm {
	public static NetworkGameProposal Instance;
	public bool Accepted;
	Invitation inv;
	Text prompt;

	void Awake()
	{
		Instance = this;
	}

	Transform GetPrompt()
	{
		return transform.Find ("prompt");
	}

	public void Show(Invitation inv)
	{
		Accepted = false;
		try {
			if (inv != null)
				Debug.Log ("NetworkGameProposal show DisplayName = " + inv.Inviter.DisplayName);
			else {
				Debug.Log ("NetworkGameProposal show inv = null!!!");
				return;
			}
			this.inv = inv;
			if (prompt == null) {
				
				if (GetPrompt () != null)
					prompt = GetPrompt ().GetComponent<Text> ();
			}
			if (inv != null && prompt != null)
				prompt.text = "Player " + inv.Inviter.DisplayName + " would like to play a game with you.";
		} catch (Exception ex) {
			Log.AddError (ex);
		}
		
		base.Show ();
	}

	public void OnAccept()
	{
		Accepted = true;
		GameSettings.Instance.Gametype = TicTacToeXAML.GameType.TwoPlayersPlayServices;
		base.Hide ();
		ApplicationService.Instance.LoadLevel (1);
	}

	public void OnReject()
	{
		try{
		GamePlayService.DeclineInv (inv);
		}catch(Exception exc) {
			Debug.Log (exc);
		}
		base.Hide ();

	}
}
