﻿using UnityEngine;
using Commons;
using Common.GUI;
using UnityEngine.UI;
using System;
using SnowmanBalls.GUI;
using System.Collections.Generic;

namespace Zybex.GUI
{
    public class LevelFinishedForm : PauseBaseForm
    {
        public ParticleSystem stars;
        static Dictionary<int, string> achievments = new Dictionary<int, string>()
        {
            { 1, "CgkIwMz5togWEAIQAw"},
            { 2, "CgkIwMz5togWEAIQBA"},
            { 3, "CgkIwMz5togWEAIQBQ"},
            { 4, "CgkIwMz5togWEAIQBg"}
        };

        void Awake()
        {
            base.OnAwake();
            
        }

        void Start()
        {
            showFullScreenAds = true;
      //if (!GameManager.Instance.ShowBestTime())
      //    transform.GetChildByName("bestTime").gameObject.SetActive(false);
#if UNITY_ANDROID
            if(achievments.ContainsKey(App.GameProgress.LastLevel))
                GamePlayService.ReportProgress(achievments[App.GameProgress.LastLevel]);
#endif
      //save last level in case of user has problems with game continuing after ad click
      //if (App.GameProgress.MaxEverPlayedLevel < App.GameProgress.LastLevel + 1)
      //    App.GameProgress.SaveMaxEverPlayedLevel(App.GameProgress.LastLevel + 1);

      //App.GameProgress.SaveLastLevel(App.GameProgress.LastLevel + 1);

      //stars = transform.FindChild("ParticleStars").GetComponent<ParticleSystem>();

      //GetText("TimeSpendValue").text = GameManager.Instance.Statistics.TimeSpend.ToString("#.##");
      //GetText("KilledEnemiesValue").text = ApplicationService.Instance.GameProgress.KilledEnemies.ToString();


      ////it sucks
      //var pph = transform.FindChild("particlesPlaceHolder");
      //Action<int> positionParticles = (int index) =>
      //{
      //    var posParticles = pph.transform.FindChild("centerPS" + index).position;
      //    pph.transform.FindChild("PS" + index).position = posParticles;
      //};
      //positionParticles(1);
      //positionParticles(2);

      ////enemyStarsEmmited = true;
      //HightlightResult("KilledEnemiesValue");

      //Invoke("HightlightScore", 1f);
      //SetTitle("LevelFinished");

      //ResetLevelScore();


    }

    private static void ResetLevelScore()
        {
            ApplicationService.Instance.GameProgress.ResetLevelScore(); 
        }

        void HightlightScore()
        {
            HightlightResult("TimeSpendValue");
        }
        void HightlightResult(string txtName)
        {
            EmitParticles(GetText(txtName));
            AudioService.Instance.PlaySound("bowl_show");
        }


        void EmitParticles(Text txtName)
        {
            var pos = txtName.transform.Find("center").position;
            stars.transform.position = pos;
            stars.Emit(10);
        }

        Text GetText(string txtName)
        {
            return transform.Find(txtName).GetComponent<Text>();
        }
        TimeTracker trLasting = new TimeTracker();
        public void PlayNextLevel()
        {
            if (trLasting.TotalSeconds > 0.5)//prevent accidental clicks.
            {
                base.Hide();
                if (ApplicationService.Instance.GameProgress.CurrentLevel < ApplicationService.Instance.NumberOfLevels)
                    App.LoadNextLevel();
                //else
                //   Forms.Instance.GameFinished();
            }
            
        }
    }
}
