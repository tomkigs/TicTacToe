﻿using UnityEngine;
using System.Collections;
using TicTacToeXAML;

public class WinInfo {

    public string Winner;
    public string TimeString;
    public float Time;
    public int Moves;
    //public PlayerGame wininingPlayer { get; set; }
    //public PlayerGame humanGame;
    //public LineCheckResult lineCheckResult;

    public WinInfo(bool crossWin, string time, int moves)
	{
		Winner = crossWin ? "X": "O";
        TimeString = time;
		Moves = moves;
	}
}
