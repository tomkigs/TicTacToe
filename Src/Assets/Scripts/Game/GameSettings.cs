﻿using TicTacToeXAML;
using UnityEngine;

namespace Assets.Scripts
{
    public class GameSettings
    {
        public static readonly GameSettings Instance = new GameSettings();
        public enum Difficulty { Easy, Normal};
        public Difficulty difficultyLevel = Difficulty.Easy;
        public GameType Gametype;
        bool useDynamicBackgrounds = true;
        bool everShownToUser = false;
		public bool InitNetworkGame;

        GameSettings()
        {
            Gametype = GameType.OnePlayer;
            Load();
        }

        void Save()
        {
            PlayerPrefs.SetInt("DifficultyLevel", (int)DifficultyLevel);
            PlayerPrefs.SetInt("UseDynamicBackgrounds", UseDynamicBackgrounds ? 1 : 0);
            PlayerPrefs.SetInt("EverShownToUser", EverShownToUser ? 1 : 0);
        }

        void Load()
        {
            difficultyLevel = (Difficulty)PlayerPrefs.GetInt("DifficultyLevel", (int)Difficulty.Easy);
            useDynamicBackgrounds = PlayerPrefs.GetInt("UseDynamicBackgrounds", 1) == 1 ? true : false;
            everShownToUser = PlayerPrefs.GetInt("EverShownToUser", 1) == 1 ? true : false;
        }

        public Difficulty DifficultyLevel
        {
            get { return difficultyLevel; }
            set
            {
                difficultyLevel = value;
                Save();
            }
        }

        public bool UseDynamicBackgrounds
        {
            get { return useDynamicBackgrounds; }
            set
            {
                useDynamicBackgrounds = value;
                Save();
            }
        }

        public bool EverShownToUser
        {
            get { return everShownToUser; }
            set
            {
                everShownToUser = value;
                Save();
            }
        }
    }
}
