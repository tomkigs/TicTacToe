﻿using Commons.Grid;
using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Game.Core;
using TicTacToeXAML;
using Assets.Scripts.Common.Logging;
using VikingChess;
using UnityEngine;
using Commons;
using Assets.Scripts.Game.Core.Tests;

namespace Assets.Scripts.Game
{
  public class TicTacToeGrid : Commons.Grid.Grid
  {
    public Point LastAddedToHighlight;
    Stack<Entity> lastAddedCrosses = new Stack<Entity>();
    Stack<Entity> lastAddedCircles = new Stack<Entity>();
    internal PlayerGame crossGame;
    PlayerGame circleGame;
    PlayerGame compGame;
    internal PlayerGame humanGame;
    internal PcPlayer pcPlayer;
    public static int GameLength = 5;
    public GameManager mgr;
    bool startGameCrossTurn;
    public static TicTacToeGrid Instance;
    public bool CrossTurn;
    GameObject lastMove;
    GameObject crossLine;
    public Entity[,] entitiesBuffer;
    Quaternion stratRot;
    public Dictionary<GridElemType, Point> lastAddedNetworkGame = new Dictionary<GridElemType, Point>();
    MatchEntity matchServiceEntity;
    bool started;

    void Start()
    {
      started = true;
      mgr = GameManager.Instance;
      Instance = this;
      base.OnStart();
      if (entitiesBuffer == null)
        entitiesBuffer = new Entity[GameDefines.BoardSize, GameDefines.BoardSize];
      stratRot = crossLine.transform.localRotation;
#if UNITY_ANDROID
      if (matchServiceEntity == null)
        matchServiceEntity = new MatchEntity(this);
#endif
      DesrializeIfNeeded();
      Debug.Log("TicTacToeGrid start end");

      //NetworkGameProposal.Instance.Show ();
      //SetBackgroundColor (false);
    }

    void DesrializeIfNeeded()
    {
      if (!started)
        return;
      if (ApplicationService.Instance.DeserializeMatchAfterLoad)
      {
#if UNITY_ANDROID
				Debug.Log ("DeserializeMatchAfterLoad !!!");
				ApplicationService.Instance.DeserializeMatchAfterLoad = false;
				matchServiceEntity.Deserialize (GamePlayService.match.Data, "DeserializeMatchAfterLoad");
#endif
      }
    }

    public void OnNewGame()
    {
      lastAddedCrosses.Clear();
      lastAddedCircles.Clear();
      lastMove = transform.Find("lastMove").gameObject;
      lastMove.SetActive(false);
      entitiesBuffer = new Entity[GameDefines.BoardSize, GameDefines.BoardSize];

      lastAddedNetworkGame[GridElemType.Circle] = new Point(-1, -1);
      lastAddedNetworkGame[GridElemType.Cross] = new Point(-1, -1);


      crossLine = transform.Find("crossLine").gameObject;
      crossLine.SetActive(false);

      crossGame = new PlayerGame(GridElemType.Cross, this);
      circleGame = new PlayerGame(GridElemType.Circle, this);

      if (GameSettings.Instance.Gametype == GameType.OnePlayer)
      {
        compGame = crossGame;
        humanGame = circleGame;
        humanGame.typeOfElem = GridElemType.Circle;

        pcPlayer = new PcPlayer(this, compGame, humanGame);
      }
      else
      {
        compGame = null;
        humanGame = null;
        pcPlayer = null;
      }

      CrossTurn = false;
      startGameCrossTurn = CrossTurn;
      #if UNITY_ANDROID
      matchServiceEntity = new MatchEntity(this);
#endif
      TestBase.InitMembers(this, crossGame, circleGame, compGame, pcPlayer);
      Debug.Log("diffic = " + GameSettings.Instance.DifficultyLevel);

      DesrializeIfNeeded();
      //CreateDrawings();

      //NetworkGameProposal.Instance.Show (null);
    }


    internal void ShowCrossLine(Item startPoint, int rotationZ, LineType lineType)
    {
      Debug.Log("ShowCrossLine startPoint = " + startPoint);
      crossLine.SetActive(true);
      var pos = startPoint.transform.position;
      pos.z = -12;
      if (rotationZ == 45 || rotationZ == -45)
      {
        crossLine.GetComponent<Animator>().Play("CrossLineLong");
      }
      else
      {
        if (lineType == LineType.w12)
          pos = pos.MoveBy(0, 0.5f, 0);
        else
          pos = pos.MoveBy(-0.5f, 0f, 0);
        crossLine.GetComponent<Animator>().Play("CrossLineShort");
      }
      crossLine.transform.position = pos;
      crossLine.transform.localRotation = stratRot;
      crossLine.transform.Rotate(new Vector3(0, 0, rotationZ));
    }

    PlayerGame GetPlayerGameForNetworkGame()
    {
      return this.matchServiceEntity.ElemType == GridElemType.Cross ? crossGame : circleGame;
    }

    internal PlayerGame GetPlayerGameForNetworkGameEndCheck()
    {
      return this.matchServiceEntity.ElemType == GridElemType.Cross ? circleGame : crossGame;
    }

    internal PlayerGame GetPlayerGameForMove()
    {
      if (GameSettings.Instance.Gametype == GameType.OnePlayer)
        return humanGame;
      else if (GameSettings.Instance.Gametype == GameType.TwoPlayersSameDevice)
        return CrossTurn ? crossGame : circleGame;
      return GetPlayerGameForNetworkGame();

    }

    public int GetNumberOfElems()
    {
      return this.mgr.GetNumberOfElems();
    }

    internal bool MakePlayerMoveToPoint(PlayerGame pl, Point pt)
    {
      if (pt.X >= 0)
      {
        if (entitiesBuffer[pt.Y, pt.X] != null)
          return false;

        AddElem(pl, pt.X, pt.Y);

        OnMoveDone(pl);

        return true;
      }
      return false;

    }
    Color colorOn = Color.white;
    Color colorOff = new Color(.7f, .7f, .7f);
    public void SetBackgroundColor(bool on)
    {
      Camera.main.backgroundColor = on ? colorOn : colorOff;
      //this.GetComponent<SpriteRenderer>().color = col;
    }

    void HighlightLastMove()
    {

      try
      {
        lastMove.transform.position = getItemAtSlow(LastAddedToHighlight.X, LastAddedToHighlight.Y).transform.position;
      }
      catch (Exception ex)
      {
        Log.AddError(ex);
      }
    }

    internal Entity AddElem(PlayerGame player, int x, int y)
    {
      Entity elem = null;
      try
      {
        GridElemType type = player.typeOfElem;
        elem = mgr.CreateEntity(x, y, type == GridElemType.Cross);
        entitiesBuffer[y, x] = elem;
        LastAddedToHighlight = new Point(x, y);
        HighlightLastMove();
        lastMove.SetActive(true);
        if (GameSettings.Instance.Gametype == GameType.OnePlayer)
        {
          if (player.typeOfElem == GridElemType.Circle)
            lastAddedCircles.Push(elem);
          else
            lastAddedCrosses.Push(elem);
        }
        else
        {
          lastAddedNetworkGame[type] = new Point(x, y);
#if UNITY_EDITOR
          //TestSerialization();
#endif
        }
      }
      catch (Exception exc)
      {
        Log.AddError(exc);
      }
      return elem;
    }

    internal bool AnyMoveDone()
    {
      return lastAddedCrosses.Any() || lastAddedCircles.Any();
    }

    internal Point GetStartingPoint()
    {
      return new Point(Width / 2, Height / 2);
    }

    public Stack<Entity> GetMovesByElemType(GridElemType typeOfElem)
    {
      return typeOfElem == GridElemType.Circle ? lastAddedCircles : lastAddedCrosses; ;
    }

    internal int GetLastAddedX(GridElemType typeOfElem)
    {
      if (GameSettings.Instance.Gametype == GameType.OnePlayer)
      {
        var ls = GetMovesByElemType(typeOfElem);
        if (ls.Any())
          return ls.ElementAt(0).SnappedTo.x;
      }
      else
        return lastAddedNetworkGame[typeOfElem].X;
      return -1;
    }

    internal int GetLastAddedY(GridElemType typeOfElem)
    {
      if (GameSettings.Instance.Gametype == GameType.OnePlayer)
      {
        var ls = GetMovesByElemType(typeOfElem);
        if (ls.Any())
          return ls.ElementAt(0).SnappedTo.y;
      }
      else
        return lastAddedNetworkGame[typeOfElem].Y;
      return -1;
    }

    internal bool IsCellEmpty(Point pt)
    {
      //return getItemAt(checkPoint.X, checkPoint.Y) != null;
      return entitiesBuffer[pt.Y, pt.X] == null;
    }

    internal Entity GetElemAt(int y, int x)
    {
      if (y >= Height || y < 0)
        return null;
      if (x >= Width || x < 0)
        return null;
      return entitiesBuffer[y, x];
      //return mgr.GetEntityAt(x, y);
    }

    public bool UndoLastMove()
    {
      if (GameSettings.Instance.Gametype == GameType.OnePlayer)
      {
        if (lastAddedCrosses.Any() && lastAddedCircles.Any())
        {
          var toDel = lastAddedCrosses.Pop();
          entitiesBuffer[toDel.SnappedTo.y, toDel.SnappedTo.x] = null;
          mgr.Entities.Remove(toDel);

          Destroy(toDel.gameObject);

          toDel = lastAddedCircles.Pop();

          entitiesBuffer[toDel.SnappedTo.y, toDel.SnappedTo.x] = null;
          mgr.Entities.Remove(toDel);
          Destroy(toDel.gameObject);
          if (lastAddedCrosses.Any() && lastAddedCircles.Any())
            compGame.CalcCheckResults(false);
          CrossTurn = startGameCrossTurn;
          lastMove.SetActive(false);
          return true;
        }
      }

      return false;
    }

    public bool ComputerMove()
    {
      return GameSettings.Instance.Gametype != GameType.TwoPlayersPlayServices && GameSettings.Instance.Gametype != GameType.TwoPlayersSameDevice
          && ((humanGame.typeOfElem == GridElemType.Cross && !CrossTurn) || (humanGame.typeOfElem == GridElemType.Circle && CrossTurn));
    }

    public void RecalcPoints()
    {
      RecalcPoints(crossGame);
      RecalcPoints(circleGame);
    }

    internal void RecalcPoints(PlayerGame playerGame)
    {

      if (playerGame == null)
        playerGame = humanGame;
      playerGame.ClearResults();
      if (playerGame.LastAddedX < 0)
        return;
      //Log.AddInfo ("RecalcPoints playerGame.lastAddedX =  "+playerGame.lastAddedX);
      var pt1 = new Point(playerGame.LastAddedX, playerGame.LastAddedY);
      var points = new List<Point>() { pt1 };//pt1 };
      points.AddRange(playerGame.GetWatchedPoints().Select(i => i.point));//TODO HACK
      foreach (var pt in points)
      {
        var elemsToCalc = GetElemsToCalculatePoints(playerGame, pt);
        //Log.AddInfo ("RecalcPoints elemsToCalc count ="+elemsToCalc.Count);
        elemsToCalc.RemoveAll(i => i.X == pt1.X && i.Y == pt1.Y);
        foreach (var he in elemsToCalc)
        {
          var cp = new Point(he.X, he.Y);
          //Log.AddInfo ("RecalcPoints cp ="+cp+ ", pt = "+pt);
          playerGame.CalcCheckResults(cp, pt, false, false);
        }
      }

      //Log.AddInfo ("RecalcPoints ends");
    }

    internal List<GridElemEx> GetElemsToCalculatePoints(PlayerGame playerGame, Point checkPoint)
    {
      List<GridElemEx> elems = new List<GridElemEx>();

      elems.Add(new GridElemEx(entitiesBuffer[checkPoint.Y, checkPoint.X], checkPoint.X, checkPoint.Y));
      int size = 5;
      for (int x = checkPoint.X - size; x < checkPoint.X + size; x++)
      {
        if (x < 0)
          continue;
        for (int y = checkPoint.Y - size; y < checkPoint.Y + size; y++)
        {
          if (y < 0 || x < 0 || y >= Height || x >= Width)
            continue;
          if (entitiesBuffer[y, x] == null)
          {
            GridElem gel = null;
            if (entitiesBuffer[y, x] != null)
              gel = new GridElem(entitiesBuffer[y, x]);
            elems.Add(new GridElemEx() { elem = gel, X = x, Y = y });
          }
        }
      }

      return elems;
    }



    internal bool EndGameIfShould(PlayerGame player)
    {
      //Debug.Log("EndGameIfShould st player = " + player.typeOfElem);
      var pw = player.CreatePointWeight(new Point(player.LastAddedX, player.LastAddedY));//check for a win
      //Debug.Log("EndGameIfShould pw found...");
      var bcw = pw.GetBestConstWeight();
      if (bcw.contWeight >= 5)
      {
        //ContainsFiveInTheRow(player))
        mgr.OnGameEnd(player, humanGame, pw.GetBestConstWeight());//TODO
        return true;
      }

      return false;
    }

    internal void OnMoveDone(PlayerGame player)
    {
      Debug.Log("OnMoveDone player = " + player.typeOfElem + " lastpoint = " + player.LastAddedX + "," + player.LastAddedY);
      if (EndGameIfShould(player))
        return;
      CrossTurn = !CrossTurn;
    }

    internal LineCheckResult GetDiagonalLineNew(PlayerGame playerGame, int startGridX, int startGridY, LineType lineType)
    {
      LineCheckResult res = new LineCheckResult();
      res.elems = new List<GridElemEx>();
      res.lineType = lineType;
      int size = 5;
      int y = startGridY - size;
      int x = (lineType == LineType.w10_30 ? startGridX - size : startGridX + size);
      int maxChecks = size * 2 + 1;
      int startX = x;

      for (; x < startX + maxChecks; x += lineType == LineType.w10_30 ? 1 : -1, y += 1)// lineType == LineType.w10_30 ? 1 : -1)
      {
        try
        {
          if (x >= Width)
          {
            if (lineType == LineType.w10_30)
              break;
            else
              continue;
          }
          if (x < 0)
          {
            if (lineType == LineType.w10_30)
              continue;
            else
              break;
          }

          if (y >= Height)
          {
            continue;
          }

          if (y < 0)
            continue;
          var el = entitiesBuffer[y, x];
          GridElem gel = null;
          if (el != null)
            gel = new GridElem(el);
          var elem = new GridElemEx(gel, x, y);
          res.elems.Add(elem);
        }
        catch (Exception exc)
        {
          Log.AddError(exc);
        }

      }
      //Log.AddInfo(string.Format("GetDiagonalLineNew ends, typeOfElem = {0},  elems count = {1}", playerGame.typeOfElem, res.elems.Count));
      return res;
    }

    internal LineCheckResult GetStraightLineNew(PlayerGame playerGame, int startGridX, int startGridY, LineType lineType)
    {
      LineCheckResult res = new LineCheckResult();
      /*
            
			if (startGridX < 0 || startGridY < 0)
				return res;
				*/
      res.elems = new List<GridElemEx>();
      res.lineType = lineType;

      int nInRowMinOne = 5;//4, 3
      int maxChecks = (nInRowMinOne) * 2 + 1;
      bool horiz = lineType == LineType.w9;
      int start = horiz ? startGridX - nInRowMinOne : startGridY - nInRowMinOne;
      for (int i = start; i < start + maxChecks; i++)
      {
        if (horiz && i >= Width)
          break;
        if (!horiz && i >= Height)
          break;
        if (i < 0)
          continue;
        GridElemEx elem = new GridElemEx();
        elem.X = horiz ? i : startGridX;
        elem.Y = horiz ? startGridY : i;
        //if(horiz)
        var el = entitiesBuffer[elem.Y, elem.X];
        GridElem gel = null;
        if (el != null)
          gel = new GridElem(el);

        elem.elem = gel;
        //else
        //    elem.elem = gridElems[i, startGridX];

        res.elems.Add(elem);
      }
      //Log.AddInfo(string.Format("GetStraightLineNew ends, typeOfElem = {0},  elems count = {1}" , playerGame.typeOfElem, res.elems.Count));
      return res;
    }


    public bool waitingForRecalcPoints = false;
    bool recalcPointsDone = false;
    void Update()
    {
      if (Input.GetKeyDown(KeyCode.M))
        TestSerialization();
      if (Input.GetKeyDown(KeyCode.C))
        ShowCrossLine(this.itemsBuffer[12, 12], 45, LineType.w10_30);
      if (waitingForRecalcPoints && recalcPointsDone)
      {
        waitingForRecalcPoints = false;
        recalcPointsDone = false;
        if (pcPlayer.MakeComputerMove().X >= 0)
          OnMoveDone(player);//OnMoveDone
      }
    }
    PlayerGame player = null;

    //bool useThreads;
    public void MakeDeviceMove()
    {
      if (waitingForRecalcPoints)
        return;
      //tr.Reset ();
      player = null;

      if (ComputerMove())
      {
        player = compGame;

        /*if (useThreads) {
					Thread oThread = new Thread (new ThreadStart (() => {
						waitingForRecalcPoints = true;
						recalcPointsDone = false;
						RecalcPoints ();
						recalcPointsDone = true;
					}));
					oThread.Start ();
				}
                else
                */
        {
          RecalcPoints();
          pcPlayer.MakeComputerMove();
        }
      }
      //Log.AddInfo("MakeDeviceMove lasted sec= " + tr.TotalSeconds);
    }

    public byte[] Serialize(string context = "")
    {
      byte[] myData = this.matchServiceEntity.ToBytes(context);
      return myData;
    }

    public MatchEntity MatchEntity
    {
      get { return matchServiceEntity; }
    }

    public MatchEntity Deserialize(byte[] data, string context = "")
    {
      matchServiceEntity.Deserialize(data, context);
      HighlightLastMove();
      return matchServiceEntity;
    }

    public void TestSerialization()
    {
      var bytes = Serialize("test");
      lastAddedNetworkGame[GridElemType.Circle] = new Point(-1, -1);
      lastAddedNetworkGame[GridElemType.Cross] = new Point(-1, -1);
      Deserialize(bytes, "test");
    }
  }
}
