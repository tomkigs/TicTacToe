﻿using Commons.Grid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VikingChess;

namespace Assets.Scripts.Game.Strategy
{
    class MoveAdvisor
    {
        GameManager gameManager;
        Grid grid;

        public MoveAdvisor(GameManager gameManager, Grid grid)
        {
            this.gameManager = gameManager;
            this.grid = grid;
        }

        

        private Entity SingleSideEnemySearch(ref int x, ref int y, List<Entity> enemies, List<Entity> movers)
        {
            Entity en = null;
            if (enemies.Any())
            {

                var adj = gameManager.GetAdjustingOnX(enemies.First(), movers).First();
                if (adj.SnappedTo.y == enemies.First().SnappedTo.y)
                {
                    y = enemies.First().SnappedTo.y;
                    if (adj.SnappedTo.x > enemies.First().SnappedTo.x)
                    {
                        x = enemies.First().SnappedTo.x - 1;
                    }
                    else if (adj.SnappedTo.x < enemies.First().SnappedTo.x)
                    {
                        x = enemies.First().SnappedTo.x + 1;
                    }
                }
                int foundX = x;
                int foundY = y;
                foreach (var mover in movers)
                {
                    //gameManager.SelectEntity(mover);
                    var highs = grid.Items.Where(i => i.isHighlighted()).ToList();
                    if (highs.Any(i => i.x == foundX && i.y == foundY))
                    {
                        en = mover;
                        break;
                    }
                }
            }

            return en;
        }

        List<Entity> GetAllEnemiesWithSingleCapturePossibility<T>()
        {
            var t_enemies = gameManager.Entities.Where(i => i.GetType() == typeof(T)).ToList();
            var t_ones = gameManager.Entities.Where(i => i.GetType() != typeof(T)).ToList();
            return t_enemies.Where(i=> HasOnlyOneEnemyNeibour(t_ones, i)).ToList();
        }

        private bool HasOnlyOneEnemyNeibour(List<Entity> enemies, Entity i)
        {
            return gameManager.GetAdjustingOnX(i, enemies).Count == 1;
            //throw new NotImplementedException();
        }
    }
}
