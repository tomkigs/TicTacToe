﻿using Assets.Scripts.Game.Core;
using System;
using System.Collections.Generic;
using VikingChess;
using Assets.Scripts.Common.Logging;

namespace TicTacToeXAML
{
    class GameDefines
    {
        public static Dictionary<Pattern, string> Patterns = new Dictionary<Pattern, string>();
        public const char PatternMatch = 'z';
        public const char PatternEmpty = 'e';
        public const string PatternOne = "z";
        public const string PatternTwo = "zz";
        public const string PatternThree = "zzz";
        public const string PatternFour = "zzzz";
        public const string PatternFive = "zzzzz";
		public const int MinPointWeightLength = 5; 
        public static Point InvalidPoint = new Point(-1, -1);
        public const string ModeEasy = "Easy";
        public const string ModeNormal = "Normal";
        public const int BoardSize = 25;


        static GameDefines()
        {
            var patterns = Enum.GetValues(typeof(Pattern));
            foreach (Pattern pat in patterns)
            {
                
                Patterns.Add(pat, pat.ToString().Replace('Z', 'z').Replace('E', 'e'));
            }

        }
    }

    public enum GridElemType { None = ' ', Cross = 'x', Circle = 'o' }
    enum LineType { w9, w10_30, w12, w13_30 };
    public enum GameType { OnePlayer, TwoPlayersSameDevice, TwoPlayersPlayServices }
    enum LineCheckType { Win, NextPcMove, Block }
	public enum Pattern
    {
        ZZZZZ,
        ZZZZE, EZZZZ, ZZEZZ, ZZZEZ, ZEZZZ,//4 "zxzzezeee"
        ZZEZE, ZEZZE, EZZEZ, ZZZEE, EZZZE, EEZZZ, EZEZZ, ZEZEZ,
        /*ZEEZZ, ZZEEZ,*/ ZZEEE, EZZEE, EEZZE, EEEZZ, EZEZE, ZEZEEE, EEZEZ,ZEEEZ,  //2
        ZEEEE, EZEEE, EEZEE, EEEZE, EEEEZ,//1
        Unknown
    }

    class WinInfo
    {
        public GridElem start = null;
		public GridElem end = null;
		public LineType type = LineType.w9;
    }

    class GridElem
    {
        public GridElemType Type;
        public Entity Sprite;

        public GridElem(Entity sprite)
        {
            Sprite = sprite;
            if(sprite == null)
			{
				Log.AddError("GridElem sprite null!!!");

			}
            Type = Sprite.Type;
        }

        public override string ToString()
        {
            return Type.ToString();
        }
    }

    class GridElemEx
    {
        public GridElem elem;
        public int X;
        public int Y;

        public GridElemEx()
        { }
        public GridElemEx(GridElem elem, int x, int y)
        {
            this.elem = elem;
            this.X = x;
            this.Y = y;
        }

		public GridElemEx(Entity sprite,int x, int y)
        {
			if(sprite != null)
            	elem = new GridElem(sprite);
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return (elem != null ? elem.ToString() : "null") + " [y=" + Y + ", x=" + X + "]";
        }

        public Point CreatePoint()
        {
            return new Point(X, Y);
        }
    }


    class Player
    {
        public Player()
        {
            Name = "";
        }
        public string Name { get; set; }

    }


}
