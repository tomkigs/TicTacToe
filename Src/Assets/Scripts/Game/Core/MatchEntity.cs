﻿using UnityEngine;
using System.Collections;
using System.IO;
using TicTacToeXAML;
using VikingChess;
using Assets.Scripts.Common.Logging;
using Assets.Scripts.Game;
using Assets.Scripts.Game.Core;


namespace TicTacToeXAML
{
public class MatchEntity  {

	private const int Header = 102; // sanity check for serialization

	public const char MarkNone = ' ';
	public GridElemType ElemType;
	public GridElemType WinType;

	// the participant ID that plays as 'X' (the other one plays as 'O')
	private string mParticipantIdX = "";
	VikingChess.Entity[,] entitiesBuffer;
	GameManager mgr;
	TicTacToeGrid grid;
	public int DeserializeCount;

	public MatchEntity(TicTacToeGrid grid) 
	{
		mgr = GameManager.Instance;
		this.grid = grid;
		this.entitiesBuffer = grid.entitiesBuffer;
		
	}

	public void Deserialize(byte[] b, string context = "")
	{
		DeserializeCount++;
		if (b != null) 
		{
			ReadFromBytes(b);
			//Print (context);
		}
	}

	public GridElemType GetMyMark(string participantId)
	{
		if (mParticipantIdX.Equals("")) 
		{
			// if X is unclaimed, claim it!
			mParticipantIdX = participantId;
		}
		return mParticipantIdX.Equals(participantId) ? GridElemType.Cross : GridElemType.Circle;
	}

	public void TestReadWrite()
	{
		//grid.AddElemForCurrentPlayer (3, 10);
		var bytes = ToBytes ();
		ReadFromBytes (bytes);
		Print ();
	}

	private void ReadFromBytes(byte[] b) {
		Log.AddInfo ("MatchEntity ReadFromBytes start  b Length = "+b.Length);
		BinaryReader rdr = new BinaryReader(new MemoryStream(b));
		int header = rdr.ReadInt32();
		if (header != Header) {
			// we don't know how to parse this version; user has to upgrade game
			throw new MatchData.UnsupportedMatchFormatException("Board data header " + header +	" not recognized.");
		}

		int len = (int)rdr.ReadByte();
		mParticipantIdX = new string(rdr.ReadChars(len));
		int y = 0;
		int x = 0;
		try {
			for (y = 0; y < GameDefines.BoardSize; y++) {
				for (x = 0; x < GameDefines.BoardSize; x++) {
					entitiesBuffer [y, x] = null;
				}
			}
		} catch (System.Exception ex) 
		{
			Log.AddInfo ("clearing exc "+ "x, y ="+x + ", "+y+ ", "+ex.Message);
		}
		mgr.ClearEntities ();
		Log.AddInfo ("Populating buffer");
			try {
				for (y = 0; y < GameDefines.BoardSize; y++) {
					for (x = 0; x < GameDefines.BoardSize; x++) {
						var xV = rdr.ReadInt32 ();
						var yV = rdr.ReadInt32 ();
			
						//grid.AddElemForCurrentPlayer (3, 10);
						var sign = rdr.ReadChar ();
						if (sign != '\0' && sign != ' ') {
							GridElemType type = (GridElemType)sign;
							var en = mgr.CreateEntity (x, y, type == GridElemType.Cross);
							entitiesBuffer [y, x] = en;
						} else
							entitiesBuffer [y, x] = null;
					}
				}
			} catch (System.Exception ex) {
				Log.AddInfo ("populating exc "+ "x, y ="+x + ", "+y+ ", "+ex.Message);
			}
		var point = new Point(-1, -1);
		point.X = rdr.ReadInt32();
		point.Y = rdr.ReadInt32();
		grid.lastAddedNetworkGame [GridElemType.Cross] = point;

		point.X = rdr.ReadInt32();
		point.Y = rdr.ReadInt32();
		grid.lastAddedNetworkGame [GridElemType.Circle] = point;
		WinType = (GridElemType)rdr.ReadChar ();
		grid.LastAddedToHighlight.X = rdr.ReadInt32();
		grid.LastAddedToHighlight.Y = rdr.ReadInt32();
		
		Log.AddInfo ("MatchEntity ReadFromBytes ends");
		//ComputeWinner();
	}

	public byte[] ToBytes(string context="") 
	{
		Log.AddInfo ("MatchEntity ToBytes...s");
		MemoryStream memStream = new MemoryStream();
		BinaryWriter w = new BinaryWriter(memStream);
		w.Write(Header);
		w.Write((byte)mParticipantIdX.Length);
		w.Write(mParticipantIdX.ToCharArray());

		for (var y = 0; y < GameDefines.BoardSize; y++) 
		{
			for (var x = 0; x < GameDefines.BoardSize; x++) 
			{
				
				w.Write(x);
				w.Write(y);
				var el = GetElemAt(x, y);
				w.Write (el);
			}
		}

		w.Write (grid.lastAddedNetworkGame [GridElemType.Cross].X);
		w.Write (grid.lastAddedNetworkGame [GridElemType.Cross].Y);
		w.Write (grid.lastAddedNetworkGame [GridElemType.Circle].X);
		w.Write (grid.lastAddedNetworkGame [GridElemType.Circle].Y);
		w.Write ((char)WinType);
		w.Write (grid.LastAddedToHighlight.X);
		w.Write (grid.LastAddedToHighlight.Y);

		w.Close();
		byte[] buf = memStream.GetBuffer();
		memStream.Close();
		return buf;
	}

	public void Print(string context = "")
	{
		Log.AddInfo ("MatchEntity Print starts "+ context);
		for (var y = 0; y < GameDefines.BoardSize; y++) 
		{
			string row = "";
			for (var x = 0; x < GameDefines.BoardSize; x++) 
			{
				row += "[" + GetElemAt(x, y) +"]";
			}
			Log.AddInfo (row);
		}
		Log.AddInfo ("MatchEntity Print ends");
	}

	char GetElemAt (int x, int y)
	{
		var ent = entitiesBuffer [y, x];
		char type = ' ';
		if (ent != null)
			type = (char)ent.Type;
		return type;
	}
}
}
