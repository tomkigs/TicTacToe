﻿//using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using Assets.Scripts.Game.Core;
using UnityEngine;

namespace TicTacToeXAML
{
	class LineCheckResult
	{
		public LineType lineType;
		public int nonContWeightFromPattern;
		public int contWeight;
		public List<GridElemEx> elems;
		string gridElemsToPatternFull;
		public string gridElemsToPatternShort;
		public Pattern pattern;
		public int patternIndex;
		public Point checkPoint;
		public bool patternSorroundedByEmpty;
		public static string[] patterns = { GameDefines.PatternFive, GameDefines.PatternFour, GameDefines.PatternThree, GameDefines.PatternTwo, GameDefines.PatternOne };
		public Point? watchedPoint;
		public bool hasOpenThree;
		public bool hasOpenTwo;
		public float effectiveWeight;
		public int indexBefore;
		public int indexAfter;
		public bool IsMatchAfterPattern;
        public const float OpenThreeEffectiveWeight = 3.9999f;
        public const float OpenTwoEffectiveWeight = 2.5f;
        public const float SlicedThreeEffectiveWeight = 2.99f;

        public override string ToString()
		{
			return "x = "+checkPoint.X + ", " + checkPoint.Y + ", "+lineType + ", "+pattern;
		}

		internal void CalculateWeight(GridElemType gridElemType, bool continousWeight)
		{
			nonContWeightFromPattern = 0;
			pattern = Pattern.Unknown;
			hasOpenThree = false;
			hasOpenTwo = false;
			if (elems.Any() && elems.Count >= GameDefines.MinPointWeightLength)
			{
				var ee = GetElemsToPatternFull (gridElemType);
				SeGridElemsToPatternFull(ee);

				CalculateWeight1();
			}
		}

		void SeGridElemsToPatternFull(string pattern)
		{
			try
			{
				gridElemsToPatternFull = pattern;
				if (gridElemsToPatternFull.Length > GameDefines.MinPointWeightLength*2)
				{
					gridElemsToPatternShort = gridElemsToPatternFull.Substring(1, gridElemsToPatternFull.Length - 2);
					elems.RemoveAt(0);
				}
				else
					gridElemsToPatternShort = gridElemsToPatternFull;

			}
			catch (Exception exc)
			{
				Debug.Log (exc.Message);
				throw;
			}
			//elems.RemoveAt(elems.Count-1);
		}

		public void CalculateWeight1()
		{
			string gridElemsToPattern = gridElemsToPatternShort;
			hasOpenThree = false;
			nonContWeightFromPattern = 0;
			patternSorroundedByEmpty = false;
			hasOpenTwo = false;
			foreach (var kv in GameDefines.Patterns)
			{
				var ind = gridElemsToPattern.IndexOf(kv.Value);
				if (ind >= 0)
				{
					var toCheck = gridElemsToPattern.Substring(ind, 5);
					foreach (var pat in patterns)
					{
						if (toCheck.Contains(pat))
						{
							contWeight = pat.Length;
							break;
						}
					}
					//REMOVE SPACES!!!
					toCheck = toCheck.Replace(GameDefines.PatternEmpty.ToString(), "");
					int i = 0;
					while (toCheck.Length > i && toCheck[i++] == GameDefines.PatternMatch)
						nonContWeightFromPattern++;

					pattern = kv.Key;
					patternIndex = ind;

					effectiveWeight = nonContWeightFromPattern;
					if (pattern == Pattern.ZEZEZ)
						effectiveWeight = SlicedThreeEffectiveWeight;

					bool beforePatternIsE = (ind > 0 && gridElemsToPattern[ind - 1] == GameDefines.PatternEmpty) ||
						gridElemsToPattern[ind] == GameDefines.PatternEmpty ||
						(ind == 0 && gridElemsToPatternFull[0] == GameDefines.PatternEmpty);
					int indexAfterPattern = ind + kv.Value.Length + 1;//toCheck.Length + 1;//
					if (beforePatternIsE)
					{

						if (pattern.ToString().EndsWith("E") ||
							IsCharMatchingAtIndex(GameDefines.PatternEmpty, indexAfterPattern))
							//gridElemsToPattern.Length > indexAfterPattern && gridElemsToPattern[indexAfterPattern] == GameDefines.PatternEmpty)
							patternSorroundedByEmpty = true;
					}
					IsMatchAfterPattern = IsCharMatchingAtIndex(GameDefines.PatternMatch, indexAfterPattern);

					hasOpenThree = HasOpenThree();
					hasOpenTwo = HasOpenTwo();

					if (hasOpenThree && effectiveWeight == 3)
						effectiveWeight = pattern == Pattern.ZEZEZ ? SlicedThreeEffectiveWeight : OpenThreeEffectiveWeight;
					if (hasOpenTwo && effectiveWeight == 2)
						effectiveWeight = LineCheckResult.OpenTwoEffectiveWeight;
					if (HasOpenFour() && effectiveWeight == 4f)
						effectiveWeight = 4.5f;
					break;
				}
			}
		}

		bool IsCharMatchingAtIndex(char match, int index)
		{
			return gridElemsToPatternFull.Length > index && gridElemsToPatternFull[index] == match;
		}

		public bool HasOpenFour()
		{
			return contWeight == 4 && patternSorroundedByEmpty || nonContWeightFromPattern == 4 && patternSorroundedByEmpty;
		}

		public bool HasOpenThree()
		{
			return contWeight == 3 && patternSorroundedByEmpty || nonContWeightFromPattern == 3 && patternSorroundedByEmpty  && pattern != Pattern.ZEZEZ;
		}

		public bool HasOpenTwo()
		{
			return contWeight == 2 && patternSorroundedByEmpty || nonContWeightFromPattern == 2 && patternSorroundedByEmpty;
		}

		public string GetElemsToPatternFull(GridElemType gridElemType)
		{
			StringBuilder gridElemsToPattern = new StringBuilder();
			for (int i = 0; i < elems.Count; i++)
			{
				if (elems[i].elem == null)
				{
					gridElemsToPattern.Append(GameDefines.PatternEmpty);
				}
				else
				{
					if (elems[i].elem.Type == gridElemType)
						gridElemsToPattern.Append(GameDefines.PatternMatch);
					else
						gridElemsToPattern.Append(gridElemType == GridElemType.Circle ? (char)GridElemType.Cross : (char)GridElemType.Circle);
				}
			}

			return gridElemsToPattern.ToString();
		}

		public Point GetGridPointFromPatternIndex()
		{
			return new Point(elems[patternIndex].X, elems[patternIndex].Y);
		}

		internal bool OneMoveToWin()
		{
			var winning = new Pattern[] { Pattern.ZZZZE, Pattern.EZZZZ, Pattern.ZZEZZ, Pattern.ZZZEZ, Pattern.ZEZZZ };
			if(winning.Contains(pattern))
				return true;

			return false;
		}

		internal int GetObviousBlockIndex()
		{
			if (OneMoveToWin())
			{
				return patternIndex + pattern.ToString().IndexOf('E');
			}
			return -1;
		}

		internal bool ContainsPoint(Point point)
		{
			return elems.Any(i => i.X == point.X && i.Y == point.Y);
		}

		public int GetIndexAfterPattern(bool lookInsidePattern, string str, int indexAfter, int afterStartIndex)
		{
			for (int i = afterStartIndex; i < str.Length; i++)
			{
				if (str[i] == GameDefines.PatternEmpty)
				{
					int j = i;
					if (!lookInsidePattern)
					{

						while (str[--i] == GameDefines.PatternEmpty)
							j = i;

					}
					indexAfter = j;
					break;
				}
				if (lookInsidePattern && str[i] != GameDefines.PatternMatch)
					break;
			}
			if (indexAfter < 0 && pattern.ToString().EndsWith("Z"))
			{
				indexAfter = str.Length;
			}
			return indexAfter;
		}

		public int GetIndexBeforePattern(bool lookInsidePattern, string str, int indexBefore)
		{
			if (str[patternIndex] == 'Z')
			{
				for (int i = patternIndex; i >= 0; i--)
				{
					if (str[i] == GameDefines.PatternEmpty)
					{
						int j = i;

						if (!lookInsidePattern)
						{
							while (str[++i] == GameDefines.PatternEmpty)
								j = i;
						}
						indexBefore = j;
						break;
					}
					if (lookInsidePattern && str[i] != GameDefines.PatternMatch)
						break;
				}
			}
			else
			{
				indexBefore = patternIndex;
				while (str[indexBefore] == GameDefines.PatternEmpty)
					indexBefore++;
				indexBefore--;
			}
			return indexBefore;
		}
	}
}
