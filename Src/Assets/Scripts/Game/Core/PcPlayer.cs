﻿using Assets.Scripts;
using Assets.Scripts.Common.Logging;
using Assets.Scripts.Game;
using Assets.Scripts.Game.Core;
using Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using TicTacToeXAML.Core;
using TicTacToeXAML.Tests;
//using static Assets.Scripts.GameSettings;

namespace TicTacToeXAML
{
    internal class PcPlayer
    {
        PlayerGame compGame;
        TicTacToeGrid grid;
        PlayerGame humanGame;

        public PcPlayer(TicTacToeGrid grid, PlayerGame compGame, PlayerGame humanGame)
        {
            this.compGame = compGame;
            this.grid = grid;
            this.humanGame = humanGame;

            compGame.opponent = humanGame;
            humanGame.opponent = compGame;
        }

        /// <summary>
        /// about 8 ms ave.10.09.
        /// </summary>
        /// <returns></returns>
        public Point MakeComputerMove()
        {
			
            var pt = GetNextMovePoint();
            if (pt.X >= 0)
				grid.MakePlayerMoveToPoint(compGame, pt);
            
            return pt;
        }

        private Point GetNextMovePoint()
        {
            var pt = GameDefines.InvalidPoint;
            var anyDone = grid.AnyMoveDone();
            if (!anyDone)
                return grid.GetStartingPoint();
            PointWeight bestOpponent = null;
            var moves = grid.GetMovesByElemType(compGame.typeOfElem);
            if (moves.Count == 0 && RandHelper.GetRandomDouble() > 0.5f)
            {
                var hc = grid.GetMovesByElemType(humanGame.typeOfElem);
                if (hc.Count == 1)
                {
                    var first = hc.First();
                    if (first.SnappedTo.x > 0 && first.SnappedTo.x < grid.Width && first.SnappedTo.y > 0 && first.SnappedTo.y < grid.Height)
                    {
                        pt = new Point(first.SnappedTo.x, first.SnappedTo.y - 1);
						//return entitiesBuffer[y, x];
						if (grid.entitiesBuffer[pt.Y, pt.X] != null)
                            pt = GameDefines.InvalidPoint;
                    }
                }
            }
            if (moves.Count == 1)
            {
                if (grid.GetMovesByElemType(humanGame.typeOfElem).Count == 2)////human started
                {
                    var pw = humanGame.GetPointWeightContainingBestEffectiveWeight(false);
                    var res = pw.GetBestNonContWeight();
                    if (res.effectiveWeight == LineCheckResult.OpenTwoEffectiveWeight && res.contWeight == 1 && (res.lineType == LineType.w9 || res.lineType == LineType.w12)
                        )
                    {
                        Log.AddInfo("");
                        pt = humanGame.GetClosestFreePoint(res, false);
                    }
                    else if (!Simple.Test0On && RandHelper.GetRandomDouble() > 0.5f && HumanStartedDiagonally())
                    {
                        pt = GetStartDiagonalResponse();
                    }
					if (pt.Y >=0 && grid.entitiesBuffer[pt.Y, pt.X] != null)
                        pt = GameDefines.InvalidPoint;
                }
            }
            else if (moves.Count >= 2)
            {
                var bestOffensCompWeight = compGame.CalcBestWeightedPoint();
                var bestOffensCompEffWeight = compGame.GetPointWeightContainingBestEffectiveWeight(true);
                {
                    pt = GetPointFromWeightPointContainingBestPoint(bestOffensCompWeight, bestOffensCompEffWeight, compGame, humanGame, true, out bestOpponent);
                    if (pt.X < 0 && bestOpponent != null)
                    {
                        pt = bestOpponent.point;
                    }
                }
            }
            if (pt.X < 0)
            {
                if (!moves.Any() && GameSettings.Instance.DifficultyLevel == GameSettings.Difficulty.Easy)
                {
                    var pw = humanGame.GetPointWeightContainingBestEffectiveWeight(false);
                    var res = pw.GetBestNonContWeight();
                    pt = humanGame.GetClosestFreePoint(res, false);
                }
                else
                    pt = GetBestHumanMove(out bestOpponent);
            }
            if (pt.X < 0)
                pt = bestOpponent.point;
            return pt;
        }

        private bool HumanStartedDiagonally()
        {
            var humanMoves = grid.GetMovesByElemType(humanGame.typeOfElem);
            int xDiff = humanMoves.ElementAt(1).X - humanMoves.ElementAt(0).X;
            int yDiff = humanMoves.ElementAt(1).Y - humanMoves.ElementAt(0).Y;
            return Math.Abs(xDiff) == 1 && Math.Abs(yDiff) == 1;
        }

        Point GetStartDiagonalResponse()
        {
            var resp = new Point(-1, -1);
            var humanMoves = grid.GetMovesByElemType(humanGame.typeOfElem);
            var humFirst = humanMoves.ElementAt(0);
            var humSec = humanMoves.ElementAt(1);
            if (humFirst.X < humSec.X)
            {
                resp.X = humFirst.X;
                resp.Y = humSec.Y;
            }
            else
            {
                resp.X = humSec.X;
                resp.Y = humFirst.Y;
            }
            return resp;
        }

        public Point GetBestHumanMove(out PointWeight bestOpponent)
        {
            bestOpponent = null;
            var bestOffensHumanTotalWeight = humanGame.CalcBestWeightedPoint();
            var bestOffensHumanEffWeight = humanGame.GetPointWeightContainingBestEffectiveWeight(false);
            if (bestOffensHumanTotalWeight == null && bestOffensHumanEffWeight == null)
                return new Point(grid.Width / 2, grid.Height / 2);
            return GetPointFromWeightPointContainingBestPoint(bestOffensHumanTotalWeight, bestOffensHumanEffWeight, humanGame, compGame, false, out bestOpponent);
        }

        private Point GetPointFromWeightPointContainingBestPoint(PointWeight bestOffensCallerByTotalWeight, PointWeight bestOffensCallerByEffWeight, PlayerGame caller,
                                                                 PlayerGame opponent, bool blockBestOpponentPoint, out PointWeight bestOpponent)
        {
            var pt = GameDefines.InvalidPoint;
            bestOpponent = null;
            if (bestOffensCallerByTotalWeight == null && bestOffensCallerByEffWeight == null)
                return pt;

            if (bestOffensCallerByEffWeight != null && bestOffensCallerByEffWeight.OneMoveToWin())
                return caller.GetOneMoveToWinPoint(bestOffensCallerByEffWeight);

            bool callGetBetterPoint = true;
            PointWeight bestOffensOpponentEffWeight = opponent.GetPointWeightContainingBestEffectiveWeight(false);
            var bestOffensOpponentTotalWeight = opponent.CalcBestWeightedPoint(blockBestOpponentPoint);
            var bestTotalWeightOnEffWeightLine = GetBestTotalWeightOnEffWeightLine(opponent, bestOffensOpponentEffWeight);

            if (bestOffensOpponentEffWeight != null && bestOffensOpponentEffWeight.bestEffectiveWeight == LineCheckResult.OpenThreeEffectiveWeight &&
                bestOffensOpponentEffWeight.PeekedWeight.bestEffectiveWeight >= 4.5)
            {
                if (bestTotalWeightOnEffWeightLine != null && bestOffensOpponentEffWeight != null &&
                    bestTotalWeightOnEffWeightLine.GetBetterTotalWeight() > bestOffensOpponentEffWeight.GetBetterTotalWeight()
                    )
                {
                    if (!(bestOffensOpponentEffWeight.GetBetterContWeight() > bestTotalWeightOnEffWeightLine.GetBetterContWeight()))
                    {
                        if (bestOffensOpponentTotalWeight.point != bestTotalWeightOnEffWeightLine.point)
                        {
                            bestOffensOpponentEffWeight = bestTotalWeightOnEffWeightLine;
                            callGetBetterPoint = false;
                        }
                    }
                }
            }

            bool returnBestOpponent = false;
            if (bestOffensCallerByEffWeight.bestContWeight == 4)
                returnBestOpponent = true;
            if (bestOffensOpponentEffWeight != null && bestOffensOpponentEffWeight.PeekedWeight != null && bestOffensOpponentEffWeight.PeekedWeight.bestEffectiveWeight == 5)
            {
                returnBestOpponent = true;
            }
            if (!returnBestOpponent)
            {
                returnBestOpponent = ShallReturnBestOpponent(bestOffensCallerByTotalWeight, bestOffensCallerByEffWeight, caller,
                    ref pt, bestOffensOpponentEffWeight, bestOffensOpponentTotalWeight);
            }

            if (returnBestOpponent)
            {
                if (callGetBetterPoint)
                    bestOpponent = opponent.GetBetterPoint(bestOffensOpponentEffWeight, bestOffensOpponentTotalWeight, true);
                else
                    bestOpponent = bestOffensOpponentEffWeight;
                opponent.ClearWatchedPoints();
            }
            else
            {
                opponent.AddWatchedResult(bestOffensOpponentEffWeight);
                opponent.AddWatchedResult(bestOffensOpponentTotalWeight);
            }
            return pt;
        }

        private static bool ShallReturnBestOpponent(PointWeight bestOffensCallerByTotalWeight, PointWeight bestOffensCallerByEffWeight, PlayerGame caller,
            ref Point pt, PointWeight bestOffensOpponentEffWeight, PointWeight bestOffensOpponentTotalWeight)
        {
            bool offensiveStyle = true;
            var returnBestOpponent = false;
            bool offensiveHasBetterFour = HasOffensiveBetterFour(bestOffensCallerByEffWeight, bestOffensOpponentEffWeight);
            bool offensiveHasBetterThree = HasOffensiveBetterThree(bestOffensCallerByTotalWeight, bestOffensOpponentEffWeight, bestOffensOpponentTotalWeight);
            if (offensiveHasBetterThree
                || bestOffensOpponentEffWeight == null
                ||
                (bestOffensOpponentEffWeight.PeekedWeight.bestEffectiveWeight < bestOffensCallerByEffWeight.PeekedWeight.bestEffectiveWeight
                && (bestOffensOpponentTotalWeight.GetBetterTotalWeight() < 8 || bestOffensCallerByEffWeight.PeekedWeight.bestEffectiveWeight > 4))
                || offensiveHasBetterFour
                || (offensiveStyle && bestOffensOpponentEffWeight.PeekedWeight.bestEffectiveWeight < 4
                && bestOffensOpponentTotalWeight.GetBetterOpenThreeCount() == 0)
                && bestOffensOpponentEffWeight.PeekedWeight.openThreeCount == 0
                && bestOffensOpponentTotalWeight.GetBetterTotalWeight() < 8)
            {
                pt = caller.GetBetterPoint(bestOffensCallerByEffWeight, bestOffensCallerByTotalWeight, true).point;
            }
            else if (bestOffensCallerByEffWeight.PeekedWeight.totalWeight == 9.5 && bestOffensCallerByEffWeight.PeekedWeight.bestContWeight == 4
                && bestOffensOpponentEffWeight.PeekedWeight.bestEffectiveWeight < 4.5)
                pt = caller.GetBetterPoint(bestOffensCallerByEffWeight, bestOffensCallerByTotalWeight, false).point;
            else
                returnBestOpponent = true;

            return returnBestOpponent;
        }

        private static bool HasOffensiveBetterThree(PointWeight bestOffensCallerByTotalWeight, PointWeight bestOffensOpponentEffWeight, PointWeight bestOffensOpponentTotalWeight)
        {
            var callerOpenThrees = bestOffensCallerByTotalWeight.GetBetterOpenThreeCount();
            if (callerOpenThrees >= 2)
            {
                if (bestOffensOpponentEffWeight.GetBestNonContWeightValue() < 3 && bestOffensOpponentTotalWeight.GetBestNonContWeightValue() < 3)
                    return true;
            }

            return false;
        }

        private static bool HasOffensiveBetterFour(PointWeight bestOffensCallerByEffWeight, PointWeight bestOffensOpponentEffWeight)
        {
            var offensiveHasBetterFour = false;
            try
            {
                if (bestOffensOpponentEffWeight != null &&
                    bestOffensOpponentEffWeight.PeekedWeight.bestEffectiveWeight == bestOffensCallerByEffWeight.PeekedWeight.bestEffectiveWeight)
                {
                    if (bestOffensCallerByEffWeight.PeekedWeight.bestEffectiveWeight == 4.5)
                    {
                        var res = bestOffensCallerByEffWeight.GetBestConstWeight();
                        if (res.contWeight >= 3 || (res.contWeight == 2))
                            offensiveHasBetterFour = true;
                    }
                }
            }
            catch (Exception exc)
            {
                Log.AddError(exc);
            }
            return offensiveHasBetterFour;
        }

        List<PointWeight> allInLine = new List<PointWeight>();
        private PointWeight GetBestTotalWeightOnEffWeightLine(PlayerGame opponent, PointWeight bestOffensOpponentEffWeight)
        {
            if (bestOffensOpponentEffWeight == null)
                return null;
            allInLine.Clear();
            var res = bestOffensOpponentEffWeight.GetBestNonContWeight();
            foreach (var pt in res.elems)
            {
                if (pt.elem != null)
                    continue;
                var ptw = opponent.GetPointWeight(new Point(pt.X, pt.Y));
                if (ptw != null)
                    allInLine.Add(ptw);
            }

            var best = allInLine.Any() ? allInLine.MaxByField(i => i.GetBetterTotalWeight()).First() : null;
            return best;
        }

    }
}
