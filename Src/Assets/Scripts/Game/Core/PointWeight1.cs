﻿//using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using Commons;
using Assets.Scripts.Game.Core;

namespace TicTacToeXAML.Core
{
	class PointWeight
	{
		Dictionary<LineType, LineCheckResult> results = new Dictionary<LineType, LineCheckResult>();
		public float totalWeight;
		public float totalWeightWithOpenFill;
		public Point point;
		public bool IsEmpty;
		public Point? watchedPoint;
		//public float peekTotalWeight;
		public int openThreeCount;
		public float bestEffectiveWeight;
		public float bestNonContWeight;
		public int sorroundWeight;
		public PointWeight PeekedWeight;
		public int openTwoCount;
		public int bestContWeight;

		public override string ToString()
		{
			return point.GetDescription();
		}

		public PointWeight(Point point)
		{
			this.point = point;
		}

		public void Add(LineCheckResult res)
		{
			results[res.lineType] = res;
			watchedPoint = res.watchedPoint;
			//IsEmpty = res.elems[
		}

		public float SumWeights()
		{
			return results.Values.Sum(i => i.effectiveWeight);
		}
		internal LineCheckResult GetBestNonContWeight()
		{
			return results.Values.MaxByField(i => i.nonContWeightFromPattern).ElementAt(0);
		}

		public float GetBestNonContWeightValue()
		{
			return GetBestNonContWeight().nonContWeightFromPattern;
		}

		internal LineCheckResult GetBestConstWeight()
		{
			return results.Values.MaxByField(i => i.contWeight).FirstOrDefault();
		}

		void SetTotalWeight()
		{
			var sum = SumWeights();
			totalWeight = sum;
			openThreeCount = results.Values.Where(i => i.hasOpenThree).Count();
			openTwoCount = results.Values.Where(i => i.hasOpenTwo).Count();
			if (openThreeCount > 1)
				totalWeight += openThreeCount * 2;
			totalWeightWithOpenFill = totalWeight;// totalWeight + openThreeCount * 1f + openTwoCount * 0.2f;
		}

		public LineCheckResult GetResult(LineType line)
		{ 
			return results[line];
		}

		internal void Recalculate()
        {
            SetTotalWeight();
            bestEffectiveWeight = results.Values.Any() ? results.Values.Max(i => i.effectiveWeight) : 0;
            bestContWeight = results.Values.Any() ? results.Values.Max(i => i.contWeight) : 0;
            bestNonContWeight = results.Values.Any() ? results.Values.Max(i => i.nonContWeightFromPattern) : 0;
        }

		internal LineCheckResult GetOneMoveToWin()
		{
			return results.Values.First(i => i.OneMoveToWin());
		}

		internal bool OneMoveToWin()
		{
			return results.Values.Any(i => i.OneMoveToWin());
		}
		public IEnumerable<LineCheckResult> GetResultsWithEffectiveWeight(float weight)
		{
			return results.Values.Where(i => i.effectiveWeight == weight);
		}

		public int GetBetterOpenThreeCount()
		{
			if (this.PeekedWeight != null)
				return this.PeekedWeight.openThreeCount > openThreeCount ? this.PeekedWeight.openThreeCount : openThreeCount;
			return openThreeCount;
		}
		public float GetBetterTotalWeight()
		{
			if (this.PeekedWeight != null)
				return this.PeekedWeight.totalWeight > totalWeight ? this.PeekedWeight.totalWeight : totalWeight;
			return totalWeight;
		}

		public float GetBetterContWeight()
		{
			if (this.PeekedWeight != null)
				return this.PeekedWeight.bestContWeight > bestContWeight ? this.PeekedWeight.bestContWeight : bestContWeight;
			return bestContWeight;
		}
		internal int GetBetterOpenTwoCount()
		{
			if (this.PeekedWeight != null)
				return this.PeekedWeight.openTwoCount > openTwoCount ? this.PeekedWeight.openTwoCount : openTwoCount;
			return openTwoCount;
		}


		public bool TotalWeightMuchBetter(PointWeight effWeightToCompare)
		{ 
			return openTwoCount >= 4 && totalWeight > effWeightToCompare.totalWeight * 2
				//|| (openThreeCount > 0 && totalWeight > effWeightToCompare.totalWeight * 2)// && effWeightToCompare.PeekedWeight.openThreeCount < 2)
				|| (PeekedWeight != null && effWeightToCompare.PeekedWeight != null &&
					PeekedWeight.openThreeCount >= 2 && effWeightToCompare.PeekedWeight.bestEffectiveWeight <= 4 
					&& effWeightToCompare.PeekedWeight.openThreeCount == 0)
				;
		}

		internal double GetNotPeekedEffWeight()
		{
			return this.PeekedWeight != null ? this.bestEffectiveWeight : 0;
		}

		Pattern[] threeWithOneHole = new Pattern[] { Pattern.ZZEZE, Pattern.ZEZZE, Pattern.EZZEZ, Pattern.ZZZEE, Pattern.EZZZE, 
			Pattern.EEZZZ, Pattern.EZEZZ };
		internal int NumberOfOpenThreesWithOneHole()
		{
			return PeekedWeight == null ? 0 : this.PeekedWeight.results.Where(i => threeWithOneHole.Contains(i.Value.pattern)).Count();
		}


	}
}
