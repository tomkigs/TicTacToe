﻿using Commons;
//using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using TicTacToeXAML.Core;
using Assets.Scripts.Game;
using Assets.Scripts.Game.Core;
using Assets.Scripts.Common.Logging;
using UnityEngine;

namespace TicTacToeXAML
{
	internal class PlayerGame
	{
		public PlayerGame opponent;
		TicTacToeGrid grid;
		public Player player;
		public int LastAddedX 
		{ 
			get 
			{ 
				return grid.GetLastAddedX(typeOfElem); 
			} 
		}
		public int LastAddedY
		{
			get { return grid.GetLastAddedY(typeOfElem); }
		}
		public GridElemType typeOfElem;
		Dictionary<Point, PointWeight> resultsByPoint = new Dictionary<Point, PointWeight>();

		List<PointWeight> resultsToWatch = new List<PointWeight>();

		public void DeleteUsedWatchedPoints(Point nextCompMovePoint)
		{
			var pt = GetWatchedPointFromUsedPoint(nextCompMovePoint);
			var toDel = resultsToWatch.Where(i => i.point == pt).FirstOrDefault();
			if (toDel != null)
				resultsToWatch.Remove(toDel);
		}

		public void AddWatchedResult(PointWeight bestOpp)
		{
			if (bestOpp == null)
				return;
			if (!resultsToWatch.Where(i => i.point == bestOpp.point).Any())
				resultsToWatch.Add(bestOpp);
		}

		public IEnumerable<PointWeight> GetWatchedPoints()
		{
			return resultsToWatch;
		}

		public Point GetWatchedPointFromUsedPoint(Point pt)
		{
			if (resultsByPoint.ContainsKey(pt) && resultsByPoint[pt].watchedPoint.HasValue)
			{
				return resultsByPoint[pt].watchedPoint.Value;
			}

			return GameDefines.InvalidPoint;
		}

		private PointWeight AddResultToPointWeight(LineCheckResult res)
		{
			PointWeight pw = null;
			if (resultsByPoint.ContainsKey(res.checkPoint))
				pw = resultsByPoint[res.checkPoint];
			else
			{
				pw = new PointWeight(res.checkPoint);
				pw.IsEmpty = grid.IsCellEmpty(res.checkPoint);
				pw.PeekedWeight = GetResultsIfPointChecked(pw);
				resultsByPoint[res.checkPoint] = pw;
			}

			pw.Add(res);
			return pw;
		}

		public void ClearResults()
		{
			resultsByPoint.Clear();
		}

		void Recalculate()
		{
			foreach (var kv in resultsByPoint)
			{
				kv.Value.Recalculate();// 
			}
		}

		public PointWeight GetPointWeight(Point pt)
		{
			if (!resultsByPoint.ContainsKey(pt))
				resultsByPoint.Add(pt, CreatePointWeight(pt));
			return resultsByPoint.ContainsKey(pt) ? resultsByPoint[pt] : null;
		}

		public PlayerGame(GridElemType elemtype, TicTacToeGrid grid)
		{
			player = new Player();
			typeOfElem = elemtype;
			this.grid = grid;
		}

		public PointWeight CalcBestWeightedPoint(bool blockBestOpponentPoint = false)
		{
			var pws = GetEmptyPointWeights().ToList();
			if (pws.Any())
			{
				return GetBestOfPointFromPeekedData(pws, null, blockBestOpponentPoint);
			}
			return null;
		}

		internal PointWeight GetPointWeightContainingBestEffectiveWeight(bool lookAtOpp)
		{
			PointWeight pw = null;
			if (!resultsByPoint.Values.Any())
				return null;
			var openFours = resultsByPoint.Values.Where(i => i.PeekedWeight.bestEffectiveWeight == 4.5 && i.PeekedWeight.bestContWeight == 4).ToList();
			var bestEffectiveWeights = resultsByPoint.Values.MaxByField(i => i.PeekedWeight.bestEffectiveWeight).ToList();
			bool useResult;
			pw = GetBestterOfEffWeights(bestEffectiveWeights, lookAtOpp, out useResult);
			if (pw.GetNotPeekedEffWeight() == 4.5 || pw.PeekedWeight.bestContWeight == 5 || useResult)
				return pw;
			if (openFours.Any())
			{
				var pw1 = GetBestterOfEffWeights(openFours, lookAtOpp, out useResult);
				return pw1;
			}
			return pw;
		}

		private PointWeight GetBestterOfEffWeights(List<PointWeight> bestEffectiveWeights, bool lookAtOpp, out bool useResult)
		{
			useResult = false;
			//Log.AddInfo("GetBestOffensivePoint count =  " + bestEffectiveWeights.Count());
			if (lookAtOpp)
			{
				var best4 = GetBestFourAndHalfEffWeight(bestEffectiveWeights);
				if (best4 != null)
					return best4;
			}
			var bestPw = bestEffectiveWeights.MaxByField(i => i.PeekedWeight.bestEffectiveWeight).ToList();
			if (bestPw.Count == 1 && bestEffectiveWeights.Count == 2)
			{
				var first = bestPw.Single();
				var sec = first == bestEffectiveWeights[0] ? bestEffectiveWeights[1] : bestEffectiveWeights[0];
				if (BothPointBelongToTheSameCheckResult(first,sec))
				{
					Log.AddInfo("");
					if (sec.GetBetterTotalWeight() > first.GetBetterTotalWeight())
					{
						useResult = true;
						return sec;
					}
				}
			}
			//Log.AddInfo("bestPw count =  " + bestPw.Count());

			var bestOpenThree = bestPw.MaxByField(i => i.GetBetterOpenThreeCount()).ToList();
			if (bestOpenThree.Count == 1)
				return bestOpenThree.Single();
			var bestSW = GetMaxSorroundedWeightElems(true, bestPw);
			if (bestSW.Count > 1)
			{

				var elemsWithMaxTW = bestSW.MaxByField(i => i.PeekedWeight.totalWeight).ToList();
				//Log.AddInfo("elemsWithMaxTW.Count " + elemsWithMaxTW.Count);
				var ma = elemsWithMaxTW.FirstOrDefault(i => i.GetBestNonContWeight().IsMatchAfterPattern == true);

				return ma ?? elemsWithMaxTW.First();
				//return GetBestPeekedWeight(bestSW);
			}
			var pw = bestSW.FirstOrDefault();// ();//hack lastOrDef not working!
			return pw;
		}

		private bool BothPointBelongToTheSameCheckResult(PointWeight pointWeight1, PointWeight pointWeight2)
		{
			var res = pointWeight1.GetBestConstWeight();
			if (res.elems.Any(i => i.X == pointWeight2.point.X && i.Y == pointWeight2.point.Y))
				return true;
			return false;
		}

		PointWeight GetBestFourAndHalfEffWeight(List<PointWeight> bestSW)
		{
			var fourAndHalf = bestSW.Where(i => i.PeekedWeight.bestEffectiveWeight == 4.5).ToList();
			if (fourAndHalf.Count > 1)
			{
				List<PointWeight> pwo = new List<PointWeight>();
				foreach (var pt in fourAndHalf)
				{
					pwo.Add(opponent.CreatePointWeight(pt.point));
				}
				var oppBest = pwo.MaxByField(i => i.PeekedWeight.bestEffectiveWeight).ToList();
				if (oppBest.Count == 1)
					return fourAndHalf.Where(i=> i.point == oppBest.First().point).Single();
			}

			return null;
		}

		PointWeight GetBestOfPointFromPeekedData(List<PointWeight> pointsWithMax, LineCheckResult res = null,bool blockBestOpponentPoint = false)
		{
			//Log.AddInfo("GetBestOfPointWeightsWithTheSameTotalWeight pointsWithMax Count = " + pointsWithMax.Count());

			if (pointsWithMax.Count > 1)
			{
				//PointWeight BestWithOpenThree = GetBestPeekedWithOpenThree(pointsWithMax, res, blockBestOpponentPoint);
				PointWeight BestPeekedWeight = GetBestPeekedWeight(pointsWithMax);
				//return GetBetterPoint(BestWithOpenThree, BestPeekedWeight);
				//if (BestWithOpenThree != null)//Test27
				//{
				//    if (IsPeekWeightBetterInSmallWeight(BestWithOpenThree, BestPeekedWeight))
				//    {
				//        return BestPeekedWeight;
				//    }
				//    if (BestPeekedWeight == null || !BestPeekedWeight.TotalWeightMuchBetter(BestWithOpenThree))//.totalWeight * 2 > BestPeekedWeight.totalWeight) || BestPeekedWeight.openTwoCount < 4)
				//        return BestWithOpenThree;
				//}
				return BestPeekedWeight;
			}

			return pointsWithMax.First();
		}

		private static bool IsPeekWeightBetterInSmallWeight(PointWeight BestWithOpenThree, PointWeight BestPeekedWeight)
		{
			var OpenThreeCount =  BestWithOpenThree.PeekedWeight.openThreeCount;
			if (OpenThreeCount == 0 && (BestPeekedWeight.openThreeCount > 0 || BestPeekedWeight.openTwoCount > 1))
				return true;
			if ( ((BestPeekedWeight.bestEffectiveWeight >= 4 && BestPeekedWeight.openTwoCount >= 2)
				|| (BestPeekedWeight.GetBetterOpenTwoCount() >= 3 && BestPeekedWeight.sorroundWeight > BestWithOpenThree.sorroundWeight)) 
				&& OpenThreeCount == 1)
				return true;
			return BestPeekedWeight.totalWeight > BestWithOpenThree.PeekedWeight.totalWeight &&
				BestPeekedWeight.openThreeCount >= OpenThreeCount &&
				BestPeekedWeight.openThreeCount == 1;
		}

        public PointWeight GetBetterPoint(PointWeight bestEffWeight, PointWeight bestTotalWeight, bool blockBestOpponent)
        {
            if (Assets.Scripts.GameSettings.Instance.DifficultyLevel != Assets.Scripts.GameSettings.Difficulty.Normal)
                return bestEffWeight;
            if (bestEffWeight.PeekedWeight.bestEffectiveWeight >= LineCheckResult.OpenThreeEffectiveWeight)
            {
                if (bestEffWeight.PeekedWeight.bestEffectiveWeight >= 4.5 && bestEffWeight.bestEffectiveWeight >= LineCheckResult.OpenThreeEffectiveWeight)
                {
                    if (bestTotalWeight.PeekedWeight.bestEffectiveWeight < 4.5 ||
                        bestTotalWeight.PeekedWeight.bestContWeight < bestEffWeight.PeekedWeight.bestContWeight ||
                        (bestTotalWeight.PeekedWeight.bestContWeight == bestEffWeight.PeekedWeight.bestContWeight
                        && opponent.GetPointWeight(bestEffWeight.point).totalWeight > opponent.GetPointWeight(bestTotalWeight.point).totalWeight))
                        return bestEffWeight;
                }
                var totalWeightOpenThreeCount = bestTotalWeight.GetBetterOpenThreeCount();
                if ((totalWeightOpenThreeCount > bestEffWeight.GetBetterOpenThreeCount()
                    && bestTotalWeight.GetBetterTotalWeight() >= bestEffWeight.GetBetterTotalWeight())
                    ||
                    bestTotalWeight.TotalWeightMuchBetter(bestEffWeight))
                    return bestTotalWeight;

                SetSorroundedWeight(bestEffWeight, blockBestOpponent);
                SetSorroundedWeight(bestTotalWeight, blockBestOpponent);

                if (IsPeekWeightBetterInSmallWeight(bestEffWeight, bestTotalWeight))
                    return bestTotalWeight;
                if (bestEffWeight.PeekedWeight.openThreeCount - totalWeightOpenThreeCount > 1)
                {
                    return bestEffWeight;
                }
                if (totalWeightOpenThreeCount > bestEffWeight.PeekedWeight.openThreeCount && totalWeightOpenThreeCount > 1
                    && bestTotalWeight.NumberOfOpenThreesWithOneHole() >= 2)
                    return bestTotalWeight;

                if (bestTotalWeight.PeekedWeight == null)
                    return bestEffWeight;


                if (bestEffWeight.PeekedWeight.bestEffectiveWeight == bestTotalWeight.PeekedWeight.bestEffectiveWeight)
                {
                    if (bestTotalWeight.PeekedWeight.totalWeight > bestEffWeight.PeekedWeight.totalWeight && bestTotalWeight.PeekedWeight.totalWeight > 8
                        && bestEffWeight.PeekedWeight.totalWeight > 8)
                    {
                        var pwTW = opponent.GetPointWeight(bestTotalWeight.point).PeekedWeight;
                        var pwEW = opponent.GetPointWeight(bestEffWeight.point).PeekedWeight;
                        return pwTW.totalWeight >= pwEW.totalWeight ? bestTotalWeight : bestEffWeight;
                    }
                    return bestEffWeight.PeekedWeight.sorroundWeight > bestTotalWeight.PeekedWeight.sorroundWeight ? bestEffWeight : bestTotalWeight;
                }
                if (bestEffWeight.PeekedWeight.openThreeCount == 0 && bestEffWeight.PeekedWeight.bestEffectiveWeight <= 4)
                {
                    if (bestTotalWeight.PeekedWeight.openTwoCount > 1)
                        return bestTotalWeight;
                }
                return bestEffWeight.PeekedWeight.bestEffectiveWeight > bestTotalWeight.PeekedWeight.bestEffectiveWeight ? bestEffWeight : bestTotalWeight;
            }
            else
            {
                if (!bestTotalWeight.TotalWeightMuchBetter(bestEffWeight))//.totalWeight * 2 > BestPeekedWeight.totalWeight) || BestPeekedWeight.openTwoCount < 4)
                    return bestEffWeight;
            }

            return bestTotalWeight;
        }

        private void SetSorroundedWeight(PointWeight bestEffWeight, bool blockBestOpponent = false)
        {
            if (bestEffWeight.PeekedWeight != null)
            {
                bestEffWeight.PeekedWeight.sorroundWeight = GetSorroundWeight(bestEffWeight.PeekedWeight.point, blockBestOpponent);
            }
            bestEffWeight.sorroundWeight = GetSorroundWeight(bestEffWeight.point, blockBestOpponent);
        }

        PointWeight GetBestPeekedWeight(IList<PointWeight> points)
		{
			var pointsWithMaxP = points.MaxByField(i => i.PeekedWeight.totalWeightWithOpenFill).ToList();
			//Log.AddInfo("GetBestWeightedPoint pointsWithMaxP Count = " + pointsWithMaxP.Count());
			return pointsWithMaxP.First();
		}

		PointWeight GetBestPeekedWithOpenThree(IList<PointWeight> points, LineCheckResult res = null, bool blockBestOpponentPoint = false)
		{
			var pointsWithMaxOpenThree = points.MaxByField(i => i.PeekedWeight.openThreeCount).ToList();
			//Log.AddInfo("GetBestWeightedPoint pointsWithMaxOpenThree Count = " + pointsWithMaxOpenThree.Count());
			if (pointsWithMaxOpenThree.Any())
			{
				var maxSorroundWeightElems = GetMaxSorroundedWeightElems(blockBestOpponentPoint, pointsWithMaxOpenThree);
				if (res != null && maxSorroundWeightElems.Count == 2)
				{
					if (res.effectiveWeight == LineCheckResult.OpenThreeEffectiveWeight)
					{
						if (res.indexBefore > 0)
						{
							if (res.gridElemsToPatternShort[res.indexBefore - 1] != GameDefines.PatternEmpty)
								return maxSorroundWeightElems.Last();
						}
					}
				}
				return maxSorroundWeightElems.First();
			}
			return null;
		}

		private List<PointWeight> GetMaxSorroundedWeightElems(bool blockBestOpponentPoint, List<PointWeight> pointsWithMaxOpenThree, int level = 0)
		{
			SetPeekedSorroundedWeight(blockBestOpponentPoint, pointsWithMaxOpenThree, level);
			var maxSorroundWeightElems = pointsWithMaxOpenThree.MaxByField(i => i.PeekedWeight.sorroundWeight).ToList();
			if (maxSorroundWeightElems.Count == 2 && level == 0)
			{
				var nextLevel = GetMaxSorroundedWeightElems(blockBestOpponentPoint, maxSorroundWeightElems, ++level);
				if (nextLevel.Count == 1)
					return nextLevel;
			}
			return maxSorroundWeightElems;
		}

		private void SetPeekedSorroundedWeight(bool blockBestOpponentPoint, List<PointWeight> pointsWithMaxOpenThree, int level = 0)
		{
			foreach (var pw in pointsWithMaxOpenThree)
			{
				pw.PeekedWeight.sorroundWeight = GetSorroundWeight(pw.PeekedWeight.point, blockBestOpponentPoint, level);
			}
		}

		private PointWeight GetResultsIfPointChecked(PointWeight pointWeight)
		{
			var orgRes = this.CalcCheckResults(pointWeight.point, GameDefines.InvalidPoint, false, false, true);
			var pw = new PointWeight(pointWeight.point);
			foreach (var res in orgRes)
			{
				if (res.gridElemsToPatternShort == null)
					continue;
				var indexOfPoint = -1;
				try
				{

					for (int i = 0; i < res.elems.Count; i++)
					{
						if (res.elems[i].X == pointWeight.point.X && res.elems[i].Y == pointWeight.point.Y)
						{
							indexOfPoint = i;
							break;
						}
					}
					if (indexOfPoint < 0)
						continue;

					var pat = new StringBuilder(res.gridElemsToPatternShort);
					pat[indexOfPoint] = GameDefines.PatternMatch;
					res.gridElemsToPatternShort = pat.ToString();
					res.CalculateWeight1();
					pw.Add(res);
				}
				catch (Exception exc)
				{
					Log.AddError(exc);//HACK to del ?
				}
			}

			pw.Recalculate();
			return pw;
		}

		private IEnumerable<PointWeight> GetEmptyPointWeights()
		{
			return resultsByPoint.Values.Where(i => i.IsEmpty);
		}

		public override string ToString()
		{
			return player.Name + " " + typeOfElem.ToString();
		}

		LineCheckResult CalcLineCheckResult(LineType line, Point checkPoint, bool addingForbidden = false)
		{

			LineCheckResult res = null;
			if (line == LineType.w12 || line == LineType.w9)
				res = grid.GetStraightLineNew(this, checkPoint.X, checkPoint.Y, line);
			else
				res = grid.GetDiagonalLineNew(this, checkPoint.X, checkPoint.Y, line);
			if (checkPoint.X == 0)
			{
				int kk = 0;
				//Debug.Log (kk);
			}
			res.CalculateWeight(this.typeOfElem, true);
			res.checkPoint = checkPoint;

			return res;
		}

		public Point GetClosestFreePoint(LineCheckResult res, bool lookInsidePattern = false)
		{
			var indexToUse = res.GetObviousBlockIndex();

			if (indexToUse < 0)
			{
				var str = res.gridElemsToPatternShort;
				//var 
				int indexBefore = -1;
				int indexAfter = -1;
				indexBefore = res.GetIndexBeforePattern(lookInsidePattern, str, indexBefore);
				int afterStartIndex = lookInsidePattern ? indexBefore + 1 : res.patternIndex + res.pattern.ToString().Length;
				indexAfter = res.GetIndexAfterPattern(lookInsidePattern, str, indexAfter, afterStartIndex);
				indexToUse = indexBefore >= 0 ? indexBefore : indexAfter;
				if (indexAfter >= 0 && indexBefore >= 0)
				{
					res.indexBefore = indexBefore;
					res.indexAfter = indexAfter;
					if (res.effectiveWeight == LineCheckResult.OpenThreeEffectiveWeight && res.contWeight == 2)
					{
						if (res.pattern.ToString()[0] == 'Z' || res.pattern == Pattern.EZZEZ)
							indexToUse = indexAfter;
						else
							indexToUse = indexBefore;
					}
					else
					{
						var p1 = new Point(res.elems[indexBefore].X, res.elems[indexBefore].Y);
						var pw1 = GetPointWeight(p1);
						var p2 = new Point(res.elems[indexAfter].X, res.elems[indexAfter].Y);
						var pw2 = GetPointWeight(p2);
						PointWeight better;
						if (pw1.totalWeight == pw2.totalWeight)
						{
							better = GetBestOfPointFromPeekedData(new List<PointWeight>() { pw1, pw2 }, res);
							indexToUse = better == pw1 ? indexBefore : indexAfter;
						}
						else
							indexToUse = pw1.totalWeight >= pw2.totalWeight ? indexBefore : indexAfter;
					}
				}
			}

			if (indexToUse >= 0)
				return new Point(res.elems[indexToUse].X, res.elems[indexToUse].Y);
			return GameDefines.InvalidPoint;
		}

		private int GetSorroundWeight(Point point, bool blockBestOpponentPoint = false, int level = 0)
		{
			int weight = 0;
			int sub = 1+level;
			int add = 3+level;
			int startX = point.X - sub;
			for (int x = startX; x < startX + add; x++)
			{
				for (int y = point.Y - sub; y < point.Y - 1 + add; y++)
				{ 
					var el = grid.GetElemAt(y,x);
					if (el != null && el.Type == typeOfElem)
						weight++;
					if (blockBestOpponentPoint)
					{
						if (el != null && el.Type != typeOfElem)
							weight--;
					}
				}
			}

			return weight;
		}

		public void CalcCheckResults(bool fullCheck)
		{
			this.CalcCheckResults(new Point(LastAddedX, LastAddedY), GameDefines.InvalidPoint, fullCheck);
		}
		LineType[] enums = new LineType[] { LineType.w10_30, LineType.w13_30, LineType.w9, LineType.w12 };
		List<LineCheckResult> chr = new List<LineCheckResult>();
		TimeTracker tr = new TimeTracker ();
		public List<LineCheckResult> CalcCheckResults(Point checkPoint, Point watchedPoint, bool fullCheck, bool clear = true, bool addingForbidden = false)
		{
			tr.Reset ();
			if(clear)
				ClearResults();
			
			chr.Clear ();
			PointWeight pw = null;
			foreach (var en in enums)
			{
				var res = CalcLineCheckResult(en, checkPoint, addingForbidden);
				res.watchedPoint = watchedPoint;
				chr.Add(res);
				if (res.nonContWeightFromPattern > 0 && !addingForbidden)
				{
					//Debug.Log ("pw "+checkPoint); 
					pw = AddResultToPointWeight(res);
				}
			}
			if(pw != null)
				pw.Recalculate();
			//Debug.Log ("CalcCheckResults " + checkPoint+ " lasted : " + tr.TotalSeconds);
			return chr;

		}

		internal PointWeight CreatePointWeight(Point pt)
		{
			var pw = new PointWeight(pt);
			var res = CalcCheckResults(pt, pt, false, false, true);
			foreach(var re in res)
				pw.Add(re);
			pw.Recalculate();
			pw.PeekedWeight = GetResultsIfPointChecked(pw);
			return pw;
		}

		internal Point GetOneMoveToWinPoint(PointWeight pw)
		{
			var res = pw.GetOneMoveToWin();
			return GetClosestFreePoint(res, true);
		}

		internal void ClearWatchedPoints()
		{
			resultsToWatch.Clear();
		}
	}
}
