﻿using Assets.Scripts.Game.Core;
using Assets.Scripts.Game.Core.Tests;
using System;
using UnityEngine;

namespace TicTacToeXAML.Tests
{
    class Advanced : TestBase
    {
        static void TestInGameScenarios()
		{
            for (int i = 1; i < 53; i++)
            {
                if (i == 19)//test sucks
                    continue;
                if (i == 22)//not sure why
                    continue;
                //
                if (i == 45 || i == 39 || 
                    i == 47 ||
                    i == 48 || 
                    i == 49 ||
                    i == 52
                    )
                    continue;//fails only in UNITY!
                string digit = i.ToString();
                if (i < 10)
                    digit = "0" + i;
                RunTest("Test"+ digit);
            }
			
        }

        public static void RunTest(string name)
        {
            testName = name;
            var meth = typeof(Advanced).GetMethod(name);
            if (meth != null)
                meth.Invoke(null, null);
            else
                Debug.LogError("method "+ testName + " not found");
        }
        //method TestOffence1 not found
        public static void RunAll()
        {
            //Test5Wins();
            TestInGameScenarios();

            for (int i = 0; i < 8; i++)
            {
                //2, 3, 4 - asserts are off! TODO
                RunTest("TestOffence" + i);
            }
            Debug.Log("!!!UT RunAll ended!!!");
        }
        /// <summary>
        /// run in it 6x6 grid!
        /// </summary>
        public static void Test5Wins()
        {
            Debug.Assert(GameDefines.BoardSize == 6);
            Init();
            int startX = 5;
            int startY = 0;
            int xStep = 1;
            if (xStep == 1)
                startX = 0;

            AddElem(circleGame, startX, startY);
            startX += xStep;
            startY += 1;
            AddElem(circleGame, startX, startY);
            startX += xStep;
            startY += 1;
            AddElem(circleGame, startX, startY);
            startX += xStep;
            startY += 1;
            AddElem(circleGame, startX, startY);
            startX += xStep;
            startY += 1;
            AddElem(circleGame, startX, startY);

            grid.OnMoveDone(circleGame);
            Assert(VikingChess.GameManager.Instance.IsGameOver());
            Point pt = RecalcAndMove();
            int k = 0;
            //LastAdded X: x= 6, y= 9     
            //Assert(IsPointEqual(pt, 14, 11) || IsPointEqual(pt, 11, 8));
        }


        public static void TestOffence0()
        {
            Init();
            pcPlayer.MakeComputerMove();
            Assert(grid.AnyMoveDone());
        }

        public static void TestOffence1()
        {
            Init();

            AddElem(circleGame, 8, 8);
            Recalc();

            Point pt = pcPlayer.MakeComputerMove();
            Assert(grid.AnyMoveDone());
        }

        public static void TestOffence2()
        {
            Init();

            AddElem(circleGame, 8, 8);
            AddElem(circleGame, 9, 8);
            //AddElem(circleGame, 10, 8);
            //AddElem(circleGame, 11, 8);

            //AddElem(crossGame, 8, 9);
            AddElem(crossGame, 9, 9);
            // AddElem(crossGame, 10, 9);

            Recalc();
            //grid.CrossTurn = true;
            Point pt = pcPlayer.MakeComputerMove();
            //assume offens style
            //Assert(IsPointEqual(pt, 8, 9) || IsPointEqual(pt, 10, 9));
        }

        public static void TestOffence3()
        {
            Init();

            AddElem(circleGame, 8, 8);
            AddElem(circleGame, 9, 8);
            //AddElem(circleGame, 10, 8);
            //AddElem(circleGame, 11, 8);

            AddElem(crossGame, 8, 9);
            AddElem(crossGame, 9, 9);
            // AddElem(crossGame, 10, 9);

            Recalc();

            Point pt = pcPlayer.MakeComputerMove();
            //assert attack
            
            //Assert(IsPointEqual(pt, 10, 9) || IsPointEqual(pt, 7, 9));
        }

        public static void TestOffence4()
        {
            Init();

            AddElem(circleGame, 8, 8);
            AddElem(circleGame, 9, 8);
            AddElem(circleGame, 10, 8);
            //AddElem(circleGame, 11, 8);

            AddElem(crossGame, 8, 9);
            AddElem(crossGame, 9, 9);
            AddElem(crossGame, 10, 9);

            Recalc();

            Point pt = pcPlayer.MakeComputerMove();
            //assert attack
            //Assert(IsPointEqual(pt, 7, 9) || IsPointEqual(pt, 11, 9));
        }

        public static void TestOffence5()
        {
            Init();

            AddElem(circleGame, 8, 8);
            AddElem(circleGame, 9, 8);
            AddElem(circleGame, 10, 8);
            //AddElem(circleGame, 11, 8);

            AddElem(circleGame, 6, 9);
            AddElem(crossGame, 8, 9);
            AddElem(crossGame, 9, 9);
            AddElem(crossGame, 10, 9);
            AddElem(circleGame, 11, 9);//sorround

            Recalc();

            Point pt = pcPlayer.MakeComputerMove();
            //assert attack
            Assert(!IsPointEqual(pt, 7, 9));//|| IsPointEqual(pt, 11, 9));
        }

        public static void TestOffence6()
        {
            Init();
            AddElem(circleGame, 8, 8);
            AddElem(circleGame, 9, 8);
            AddElem(circleGame, 10, 8);
            AddElem(circleGame, 11, 8);

            AddElem(crossGame, 8, 9);
            AddElem(crossGame, 9, 9);
            AddElem(crossGame, 10, 9);
            AddElem(crossGame, 11, 9);

            Recalc();
            Point pt = pcPlayer.MakeComputerMove();
            Assert(IsPointEqual(pt, 7, 9) || IsPointEqual(pt, 12, 9));
        }

       
        public static void TestOffence7()
        {
            Init();
            AddElem(circleGame, 8, 8);
            AddElem(circleGame, 9, 8);
            AddElem(circleGame, 10, 8);

            AddElem(crossGame, 8, 9);
            AddElem(crossGame, 9, 9);
            AddElem(crossGame, 10, 9);

            Recalc();

            Point pt = pcPlayer.MakeComputerMove();
            Assert(IsPointEqual(pt, 7, 9) || IsPointEqual(pt, 11, 9));

            //grid.CrossTurn = true;
        }

        public static void Test01()
        {
            Init();
            AddElem(circleGame, 8, 5);
            AddElem(circleGame, 8, 6);
            AddElem(circleGame, 8, 7);

            AddElem(circleGame, 9, 4);
            AddElem(circleGame, 11, 4);
            AddElem(circleGame, 12, 4);

            AddElem(crossGame, 10, 5);
            AddElem(crossGame, 11, 5);
            AddElem(crossGame, 10, 6);

            //var res1 = humanGame.CalcLineCheckResult(LineType.w9, new Point(8,4));
            Recalc();
            Point pt = pcPlayer.MakeComputerMove();
            Assert(IsPointEqual(pt, 8, 4));
        }

        public static void Test02()
        {
            Init();
            AddElem(circleGame, 8, 5);
            AddElem(circleGame, 8, 6);
            AddElem(circleGame, 8, 7);

            AddElem(circleGame, 9, 4);
            AddElem(crossGame, 10, 3);

            //RecalcPoints();
            //var res1 = humanGame.CalcCheckResult(LineType.w12, new Point(8, 5));
            Recalc();
            Point pt = pcPlayer.MakeComputerMove();

            Assert(IsPointEqual(pt, 8, 4));
            
        }

        public static void Test03()
        {
            Init();
            AddElem(circleGame, 8, 5);
            AddElem(circleGame, 8, 6);
            AddElem(circleGame, 8, 7);

            AddElem(circleGame, 9, 8);
            AddElem(crossGame, 10, 9);

            Point pt = RecalcAndMove();

            Assert(IsPointEqual(pt, 8, 8));

        }

        private static Point RecalcAndMove()
        {
            Recalc();
            Point pt = pcPlayer.MakeComputerMove();
            return pt;
        }

        public static void Test04()
        {
            Init();
            AddElem(circleGame, 7, 11);
            AddElem(circleGame, 8, 10);
            AddElem(circleGame, 8, 12);
            AddElem(circleGame, 9, 10);

            //AddElem(crossGame, 9, 7);
            AddElem(crossGame, 9, 8);
            AddElem(crossGame, 9, 9);
            AddElem(crossGame, 9, 11);

            Recalc();
            Point pt = pcPlayer.MakeComputerMove();

            Assert(IsPointEqual(pt, 6, 10));
        }

        public static void Test05()
        {
            Init();
            AddElem(circleGame, 8, 10);
            AddElem(circleGame, 8, 11);
            AddElem(circleGame, 8, 13);
            AddElem(circleGame, 9, 12);
            //AddElem(circleGame, 10, 11);
            // AddElem(circleGame, 9, 10);

            //AddElem(crossGame, 9, 7);
            AddElem(crossGame, 8, 8);
            //AddElem(crossGame, 9, 9);
            //AddElem(crossGame, 9, 11);

            Point pt = RecalcAndMove();

            Assert(IsPointEqual(pt, 8, 12));
        }

        public static void Test06()
        {
            Init();
            AddElem(circleGame, 7, 7);
            AddElem(circleGame, 8, 7);
            AddElem(circleGame, 9, 7);
            AddElem(circleGame, 8, 8);
            AddElem(circleGame, 9, 9);
            //AddElem(circleGame, 9, 12);
            //AddElem(circleGame, 10, 11);
            // AddElem(circleGame, 9, 10);

            AddElem(crossGame, 7, 6);
            AddElem(crossGame, 6, 6);
            AddElem(crossGame, 10, 10);
            AddElem(crossGame, 7, 9);

            Point pt = RecalcAndMove();

            Assert(IsPointEqual(pt, 6, 7) || IsPointEqual(pt, 10, 7));
        }
        public static void Test07()
        {
            Init();
            AddElem(circleGame, 10, 5);

            AddElem(circleGame, 12, 7);
            AddElem(circleGame, 14, 9);
            AddElem(circleGame, 11, 6);
            AddElem(circleGame, 11, 8);
            AddElem(circleGame, 11, 9);
            AddElem(circleGame, 12, 6);

            AddElem(crossGame, 11, 5);
            AddElem(crossGame, 9, 5);
            AddElem(crossGame, 10, 7);
            AddElem(crossGame, 12, 8);
            AddElem(crossGame, 13, 6);
            AddElem(crossGame, 12, 10);
            AddElem(crossGame, 11, 10);

            Point pt = RecalcAndMove();

            Assert(IsPointEqual(pt, 13, 8));

        }

        public static void Test08()
        {
            Init();
            AddElem(circleGame, 8, 10);
            AddElem(circleGame, 10, 10);
            AddElem(circleGame, 8, 11);
            AddElem(circleGame, 9, 11);
            AddElem(circleGame, 8, 12);
            AddElem(circleGame, 10, 12);
            AddElem(circleGame, 6, 13);
            AddElem(circleGame, 11, 13);

            AddElem(crossGame, 9, 9);
            AddElem(crossGame, 7, 9);
            AddElem(crossGame, 9, 13);
            AddElem(crossGame, 8, 13);
            AddElem(crossGame, 7, 13);
            AddElem(crossGame, 12, 14);
            AddElem(crossGame, 10, 11);

            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;

            Assert(IsPointEqual(pt, 7, 12) || IsPointEqual(pt, 9, 10)); //9,10
        }

        public static void Test09()
        {
            Init();
            AddElem(circleGame, 11, 9);
            AddElem(circleGame, 11, 11);
            AddElem(circleGame, 10, 10);
            AddElem(circleGame, 10, 12);
            AddElem(circleGame, 9, 11);
            AddElem(circleGame, 8, 12);
            AddElem(circleGame, 9, 13);//last

            AddElem(crossGame, 12, 8);
            AddElem(crossGame, 12, 10);
            AddElem(crossGame, 12, 12);
            AddElem(crossGame, 10, 11);
            AddElem(crossGame, 9, 9);
            AddElem(crossGame, 7, 13);

            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt, 9, 12));
        }

        //template
        public static void Test10()
        {
            Init();
            AddElem(circleGame, 11, 10);
            AddElem(circleGame, 11, 12);
            AddElem(circleGame, 12, 11);
            AddElem(circleGame, 13, 10);
            AddElem(circleGame, 13, 11);
            AddElem(circleGame, 14, 9);
            AddElem(circleGame, 14, 10);
            AddElem(circleGame, 14, 11);
            AddElem(circleGame, 15, 11);
            AddElem(circleGame, 16, 12);
            AddElem(circleGame, 13, 8);

            AddElem(crossGame, 10, 9);
            AddElem(crossGame, 10, 13);
            AddElem(crossGame, 11, 8);
            AddElem(crossGame, 11, 11);
            AddElem(crossGame, 12, 8);
            AddElem(crossGame, 12, 10);
            AddElem(crossGame, 13, 12);
            AddElem(crossGame, 14, 8);
            AddElem(crossGame, 15, 8);
            AddElem(crossGame, 16, 11);

            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            var wp = circleGame.CreatePointWeight(new Point(10, 12));
            var wp1 = circleGame.CreatePointWeight(new Point(13, 9));
            Assert(IsPointEqual(pt, 13, 9));
            //Assert(false);
        }
        public static void Test11()
        {
            Init();
            AddElem(circleGame, 10, 10);
            AddElem(circleGame, 10, 12);
            AddElem(circleGame, 11, 11);
            AddElem(circleGame, 12, 10);
            AddElem(circleGame, 13, 9);
            AddElem(circleGame, 13, 11);

            AddElem(crossGame, 9, 9);
            AddElem(crossGame, 9, 13);
            //AddElem(crossGame,10, 8);
            AddElem(crossGame, 11, 10);
            AddElem(crossGame, 12, 12);
            AddElem(crossGame, 14, 8);

            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            
            //var wp = circleGame.CreatePointWeight(new Point(9, 12));
            Assert(IsPointEqual(pt, 10, 11));
        }

        public static void Test12()
        {
            Init();

            AddElem(circleGame, 9, 9);
            AddElem(circleGame, 9, 11);
            AddElem(circleGame, 10, 10);
            AddElem(circleGame, 10, 12);
            AddElem(circleGame, 11, 9);

            AddElem(crossGame, 8, 8);
            AddElem(crossGame, 8, 12);
            AddElem(crossGame, 10, 9);
            AddElem(crossGame, 11, 11);
            //AddElem(crossGame,12, 8);

            Point pt = RecalcAndMove();
            //var wp = circleGame.CreatePointWeight(new Point(9, 12));
            Assert(IsPointEqual(pt, 9, 10) || IsPointEqual(pt,8,10));
        }

        public static void Test13()
        {
            Init();
            AddElem(circleGame, 7, 9);
            AddElem(circleGame, 8, 8);
            AddElem(circleGame, 9, 7);
            AddElem(circleGame, 9, 11);
            AddElem(circleGame, 10, 10);
            AddElem(circleGame, 11, 7);
            AddElem(circleGame, 11, 9);
            AddElem(circleGame, 11, 11);
            AddElem(circleGame, 12, 7);
            AddElem(circleGame, 12, 10);
            AddElem(circleGame, 13, 8);

            AddElem(crossGame, 6, 10);
            AddElem(crossGame, 7, 7);
            AddElem(crossGame, 8, 10);
            AddElem(crossGame, 9, 9);
            AddElem(crossGame, 10, 8);
            AddElem(crossGame, 10, 11);
            AddElem(crossGame, 11, 5);
            AddElem(crossGame, 11, 8);
            AddElem(crossGame, 11, 10);
            AddElem(crossGame, 12, 8);

            Point pt = RecalcAndMove();
            //LastAdded X: x= 8, y= 7     ->13,7
            Assert(IsPointEqual(pt, 13, 7) || IsPointEqual(pt, 10, 7));
        }

        public static void Test14()
        {
            Init();
            AddElem(circleGame, 7, 9);
            AddElem(circleGame, 8, 8);
            AddElem(circleGame, 9, 7);
            AddElem(circleGame, 9, 11);
            AddElem(circleGame, 10, 7);
            AddElem(circleGame, 10, 10);
            AddElem(circleGame, 11, 7);
            AddElem(circleGame, 11, 8);
            AddElem(circleGame, 11, 9);

            AddElem(crossGame, 6, 10);
            AddElem(crossGame, 7, 7);
            AddElem(crossGame, 8, 7);
            AddElem(crossGame, 8, 10);
            AddElem(crossGame, 9, 9);
            AddElem(crossGame, 10, 8);
            AddElem(crossGame, 11, 5);
            AddElem(crossGame, 12, 8);
            Recalc();
            var wp = circleGame.CreatePointWeight(new Point(11, 6));


            Point pt = RecalcAndMove();
            //LastAdded X: x= 11, y= 6 

            Assert(IsPointEqual(pt, 11, 10) || IsPointEqual(pt, 11, 11));
        }

        public static void Test15()
        {
            Init();
            AddElem(circleGame, 7, 9);
            AddElem(circleGame, 8, 8);
            AddElem(circleGame, 9, 7);
            AddElem(circleGame, 9, 11);
            AddElem(circleGame, 10, 7);
            AddElem(circleGame, 10, 10);
            AddElem(circleGame, 11, 7);
            AddElem(circleGame, 11, 8);
            AddElem(circleGame, 11, 9);

            AddElem(crossGame, 6, 10);
            AddElem(crossGame, 7, 7);
            AddElem(crossGame, 8, 7);
            AddElem(crossGame, 8, 10);
            AddElem(crossGame, 9, 9);
            AddElem(crossGame, 10, 8);
            AddElem(crossGame, 11, 11);
            AddElem(crossGame, 12, 8);

            Point pt = RecalcAndMove();
            //LastAdded X: x= 11, y= 6     
            Assert(IsPointEqual(pt, 11, 6) || IsPointEqual(pt, 11, 5));
        }

        public static void Test16()
        {
            Init();
            AddElem(circleGame, 10, 11);
            AddElem(circleGame, 11, 10);
            AddElem(circleGame, 12, 11);

            AddElem(crossGame, 9, 8);
            AddElem(crossGame, 10, 9);

            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt, 11, 11));
        }

        public static void Test17()
        {
            Init();

            AddElem(circleGame, 10, 11);
            AddElem(circleGame, 11, 10);
            AddElem(circleGame, 11, 12);
            AddElem(circleGame, 12, 11);

            AddElem(crossGame, 8, 9);
            AddElem(crossGame, 9, 10);
            AddElem(crossGame, 11, 11);

            //LastAdded X: x= 8, y= 10 
            Point pt = RecalcAndMove();
            //LastAdded X: x= 9, y= 13     
            Assert(IsPointEqual(pt, 9, 12) || IsPointEqual(pt,10, 13));
        }

        public static void Test18()
        {
            Init();
            AddElem(circleGame, 9, 10);
            AddElem(circleGame, 10, 9);
            AddElem(circleGame, 10, 11);
            AddElem(circleGame, 11, 10);
            AddElem(circleGame, 12, 9);

            AddElem(crossGame, 7, 8);
            AddElem(crossGame, 8, 9);
            AddElem(crossGame, 8, 11);
            AddElem(crossGame, 10, 10);

            Point pt = RecalcAndMove();
            //LastAdded X: x= 9, y= 12     
            Assert(IsPointEqual(pt, 13, 8) || IsPointEqual(pt, 9, 12));
        }


        public static void Test19()
        {
            Init();
            AddElem(circleGame, 9, 13);
            AddElem(circleGame, 9, 15);
            AddElem(circleGame, 10, 12);
            AddElem(circleGame, 10, 14);
            AddElem(circleGame, 10, 16);
            AddElem(circleGame, 11, 11);
            AddElem(circleGame, 11, 13);
            AddElem(circleGame, 11, 15);
            AddElem(circleGame, 12, 15);

            AddElem(crossGame, 8, 14);
            AddElem(crossGame, 9, 11);
            AddElem(crossGame, 10, 10);
            AddElem(crossGame, 10, 13);
            AddElem(crossGame, 11, 12);
            AddElem(crossGame, 12, 12);
            AddElem(crossGame, 12, 13);
           
            AddElem(crossGame, 12, 16);
            AddElem(crossGame, 12, 14);

            Point pt = RecalcAndMove();
            //LastAdded X: x= 10, y= 15     
            Assert(IsPointEqual(pt, 12,11));
        }

        public static void Test20()
        {
            Init();
            AddElem(circleGame, 10, 9);
            AddElem(circleGame, 10, 14);
            AddElem(circleGame, 11, 9);
            AddElem(circleGame, 11, 11);
            AddElem(circleGame, 12, 10);
            AddElem(circleGame, 13, 11);

            AddElem(crossGame, 10, 10);
            AddElem(crossGame, 10, 11);
            AddElem(crossGame, 10, 12);
            AddElem(crossGame, 10, 13);
            AddElem(crossGame, 12, 12);

            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt, 10, 8));
        }

        public static void Test21()
        {
            Init();
            AddElem(circleGame, 9, 7);
            AddElem(circleGame, 9, 12);
            AddElem(circleGame, 10, 10);
            AddElem(circleGame, 10, 11);
            AddElem(circleGame, 11, 10);
            AddElem(circleGame, 11, 11);
            AddElem(circleGame, 11, 12);
            AddElem(circleGame, 12, 7);
            AddElem(circleGame, 12, 9);
            AddElem(circleGame, 12, 11);

            AddElem(crossGame, 8, 13);
            AddElem(crossGame, 9, 8);
            AddElem(crossGame, 9, 9);
            AddElem(crossGame, 9, 10);
            AddElem(crossGame, 10, 9);
            AddElem(crossGame, 10, 12);
            AddElem(crossGame, 11, 8);
            AddElem(crossGame, 11, 9);
            AddElem(crossGame, 13, 8);

            Point pt = RecalcAndMove();
            //LastAdded X: x= 10, y= 8     
            Assert(IsPointEqual(pt, 13, 11) || IsPointEqual(pt, 9, 11));
        }
        public static void Test22()
        {
            Init();
            AddElem(circleGame, 9, 10);
            AddElem(circleGame, 9, 11);
            AddElem(circleGame, 10, 11);
            AddElem(circleGame, 10, 12);
            AddElem(circleGame, 11, 11);
            AddElem(circleGame, 11, 13);
            AddElem(circleGame, 12, 12);
            AddElem(circleGame, 12, 14);
            AddElem(circleGame, 13, 11);
            AddElem(circleGame, 13, 13);
            AddElem(circleGame, 14, 10);
            AddElem(circleGame, 14, 12);
            AddElem(circleGame, 14, 13);
            AddElem(circleGame, 14, 14);
            AddElem(circleGame, 15, 10);
            AddElem(circleGame, 15, 13);
            AddElem(circleGame, 16, 13);

            AddElem(crossGame, 8, 10);
            AddElem(crossGame, 9, 12);
            AddElem(crossGame, 10, 10);
            AddElem(crossGame, 10, 14);
            AddElem(crossGame, 11, 10);
            AddElem(crossGame, 11, 12);
            AddElem(crossGame, 12, 10);
            AddElem(crossGame, 12, 11);
            AddElem(crossGame, 12, 13);
            AddElem(crossGame, 13, 12);
            AddElem(crossGame, 13, 14);
            AddElem(crossGame, 13, 15);
            AddElem(crossGame, 14, 11);
            AddElem(crossGame, 15, 9);
            AddElem(crossGame, 15, 11);
            AddElem(crossGame, 15, 15);

            Point pt = RecalcAndMove();
            //LastAdded X: x= 14, y= 15     
            Assert(IsPointEqual(pt, 9, 121));
        }

        public static void Test23()
        {
            Init();
            AddElem(circleGame, 10, 10);
            AddElem(circleGame, 11, 9);
            AddElem(circleGame, 11, 10);

            AddElem(crossGame, 9, 10);
            AddElem(crossGame, 10, 9);
            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            //LastAdded X: x= 8, y= 11
            //Assert(false);
            Assert(IsPointEqual(pt, 11, 8) || IsPointEqual(pt, 11, 11) || false);
        }

        public static void Test24()
        {
            Init();
            AddElem(circleGame, 10, 10);
            AddElem(circleGame, 11, 11);
            AddElem(circleGame, 12, 8);
            AddElem(circleGame, 12, 10);
            AddElem(circleGame, 13, 9);
            AddElem(circleGame, 14, 10);

            AddElem(crossGame, 9, 9);
            AddElem(crossGame, 10, 11);
            AddElem(crossGame, 11, 9);
            AddElem(crossGame, 11, 10);
            AddElem(crossGame, 14, 8);

            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            //LastAdded X: x= 11, y= 7     
            Assert(IsPointEqual(pt, 15, 11));
        }

        public static void Test25()
        {
            Init();
            AddElem(circleGame, 10, 10);
            AddElem(circleGame, 10, 12);
            AddElem(circleGame, 11, 11);
            AddElem(circleGame, 12, 10);
            AddElem(circleGame, 12, 12);

            AddElem(crossGame, 9, 9);
            AddElem(crossGame, 10, 11);
            AddElem(crossGame, 11, 12);
            AddElem(crossGame, 13, 13);

            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            //LastAdded X: x= 9, y= 13     
            Assert(IsPointEqual(pt, 13, 9));
        }

        public static void Test26()
        {
            Init();
            AddElem(circleGame, 7, 8);
            AddElem(circleGame, 7, 12);
            AddElem(circleGame, 8, 8);
            AddElem(circleGame, 8, 9);
            AddElem(circleGame, 8, 10);
            AddElem(circleGame, 8, 11);
            AddElem(circleGame, 9, 8);
            AddElem(circleGame, 9, 10);
            AddElem(circleGame, 9, 11);
            AddElem(circleGame, 10, 10);
            AddElem(circleGame, 10, 11);
            AddElem(circleGame, 10, 12);
            AddElem(circleGame, 11, 10);
            AddElem(circleGame, 11, 11);
            AddElem(circleGame, 12, 9);
            AddElem(circleGame, 13, 10);

            AddElem(crossGame, 6, 7);
            AddElem(crossGame, 6, 8);
            AddElem(crossGame, 6, 13);
            AddElem(crossGame, 7, 9);
            AddElem(crossGame, 7, 10);
            AddElem(crossGame, 7, 11);
            AddElem(crossGame, 8, 7);
            AddElem(crossGame, 8, 12);
            AddElem(crossGame, 9, 9);
            AddElem(crossGame, 9, 12);
            AddElem(crossGame, 10, 9);
            AddElem(crossGame, 11, 9);
            AddElem(crossGame, 11, 12);
            AddElem(crossGame, 12, 10);
            AddElem(crossGame, 12, 11);

            Point pt = RecalcAndMove();
            //LastAdded X: x= 6, y= 9     
            Assert(IsPointEqual(pt, 14, 11) || IsPointEqual(pt, 11, 8));
        }

        public static void Test27()
        {
            Init();
            AddElem(circleGame, 10, 10);
            AddElem(crossGame, 9, 9);
            AddElem(circleGame, 11, 11);
            AddElem(crossGame, 10, 11);
            AddElem(circleGame, 12, 12);
            AddElem(crossGame, 13, 13);
            AddElem(circleGame, 13, 11);
            //AddElem(crossGame, 12, 10);

            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            //Assert(false);
            Assert(IsPointEqual(pt, 12, 10) ||IsPointEqual(pt, 11, 13));
        }

        public static void Test28()
        {
            Init();
            AddElem(circleGame, 10, 10);
            AddElem(crossGame, 9, 9);
            AddElem(circleGame, 11, 11);
            AddElem(crossGame, 10, 11);
            AddElem(circleGame, 11, 10);
            AddElem(crossGame, 12, 10);
            AddElem(circleGame, 11, 12);
            AddElem(crossGame, 11, 13);
            AddElem(circleGame, 11, 9);
            AddElem(crossGame, 11, 8);
            AddElem(circleGame, 12, 8);
            AddElem(crossGame, 13, 7);
            AddElem(circleGame, 13, 9);
            AddElem(crossGame, 12, 9);
            AddElem(circleGame, 14, 10);
            AddElem(crossGame, 15, 11);
            AddElem(circleGame, 12, 11);
            AddElem(crossGame, 14, 9);
            AddElem(circleGame, 13, 12);
            AddElem(crossGame, 10, 9);
            AddElem(circleGame, 13, 11);
            AddElem(crossGame, 13, 10);
            AddElem(circleGame, 10, 7);
            AddElem(crossGame, 11, 7);
            AddElem(circleGame, 14, 13);
            AddElem(crossGame, 15, 14);
            AddElem(circleGame, 12, 12);
            AddElem(crossGame, 14, 12);
            AddElem(circleGame, 13, 13);
            AddElem(crossGame, 14, 14);
            AddElem(circleGame, 13, 14);
            AddElem(crossGame, 13, 15);
            AddElem(circleGame, 12, 15);
            AddElem(crossGame, 11, 16);
            AddElem(circleGame, 10, 12);
            AddElem(crossGame, 9, 12);
            AddElem(circleGame, 9, 13);
            AddElem(crossGame, 12, 13);
            AddElem(circleGame, 8, 14);
            AddElem(crossGame, 7, 15);
            AddElem(circleGame, 9, 15);
            AddElem(crossGame, 9, 14);
            AddElem(circleGame, 7, 13);
            //AddElem(crossGame, 8, 12);
            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt, 6, 12) || IsPointEqual(pt, 10, 16));
        }

        public static void Test29()
        {
            Init();
            AddElem(circleGame, 10, 10);
            AddElem(crossGame, 9, 9);
            AddElem(circleGame, 11, 11);
            AddElem(crossGame, 10, 11);
            AddElem(circleGame, 12, 12);
            AddElem(crossGame, 13, 13);
            AddElem(circleGame, 13, 11);
            AddElem(crossGame, 12, 10);
            AddElem(circleGame, 14, 10);
            AddElem(crossGame, 15, 9);
            AddElem(circleGame, 14, 12);
            AddElem(crossGame, 14, 11);
            AddElem(circleGame, 13, 9);
            AddElem(crossGame, 16, 12);
            AddElem(circleGame, 13, 12);
            AddElem(crossGame, 11, 12);
            AddElem(circleGame, 13, 10);
            AddElem(crossGame, 13, 8);
            AddElem(circleGame, 12, 8);
            AddElem(crossGame, 11, 7);
            AddElem(circleGame, 11, 9);
            AddElem(crossGame, 13, 7);
            AddElem(circleGame, 12, 9);
            AddElem(crossGame, 14, 9);
            AddElem(circleGame, 9, 11);
            AddElem(crossGame, 8, 12);
            AddElem(circleGame, 11, 13);
            AddElem(crossGame, 10, 14);
            AddElem(circleGame, 10, 12);
            AddElem(crossGame, 12, 14);
            AddElem(circleGame, 8, 10);
            AddElem(crossGame, 7, 9);
            AddElem(circleGame, 9, 10);
            AddElem(crossGame, 7, 10);
            AddElem(circleGame, 15, 13);
            AddElem(crossGame, 16, 14);
            AddElem(circleGame, 9, 13);
            AddElem(crossGame, 13, 14);
            AddElem(circleGame, 11, 14);
            AddElem(crossGame, 10, 13);
            AddElem(circleGame, 8, 14);
            AddElem(crossGame, 7, 15);
            AddElem(circleGame, 9, 15);
            AddElem(crossGame, 9, 12);
            AddElem(circleGame, 8, 11);
            AddElem(crossGame, 7, 11);
            AddElem(circleGame, 7, 12);
            AddElem(crossGame, 10, 9);
            AddElem(circleGame, 7, 13);
            AddElem(crossGame, 10, 16);
            AddElem(circleGame, 6, 12);
            AddElem(crossGame, 5, 11);
            AddElem(circleGame, 6, 13);
            AddElem(crossGame, 5, 14);
            AddElem(circleGame, 5, 13);
            //AddElem(crossGame, 4, 13);
            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt, 8, 13));
        }

        public static void Test30()
        {
            Init();
            AddElem(circleGame, 14, 11);
            AddElem(crossGame, 13, 10);
            AddElem(circleGame, 15, 12);
            AddElem(crossGame, 14, 12);
            AddElem(circleGame, 15, 11);
            AddElem(crossGame, 16, 11);
            AddElem(circleGame, 16, 12);
            AddElem(crossGame, 14, 10);
            AddElem(circleGame, 15, 13);
            AddElem(crossGame, 15, 14);
            AddElem(circleGame, 17, 13);
            AddElem(crossGame, 16, 13);
            AddElem(circleGame, 17, 11);
            AddElem(crossGame, 18, 10);
            AddElem(circleGame, 18, 12);
            //AddElem(crossGame, 15, 10);
            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            //Recalc();
            //var pw1= crossGame.CreatePointWeight(new Point(15, 10));
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt, 17, 12) ||IsPointEqual(pt, 15,9));
        }
        public static void Test31()
        {
            Init();
            AddElem(circleGame, 13, 10);
            AddElem(crossGame, 12, 9);
            AddElem(circleGame, 14, 11);
            AddElem(crossGame, 13, 11);
            AddElem(circleGame, 14, 10);
            AddElem(crossGame, 15, 10);
            AddElem(circleGame, 15, 11);
            //AddElem(crossGame, 14, 9);
            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt, 14, 9));
        }


        public static void Test32()
        {
            Init();
            AddElem(circleGame, 12, 10);
            AddElem(crossGame, 11, 9);
            AddElem(circleGame, 13, 11);
            AddElem(crossGame, 12, 11);
            AddElem(circleGame, 13, 10);
            AddElem(crossGame, 14, 10);
            AddElem(circleGame, 13, 9);
            AddElem(crossGame, 13, 8);
            AddElem(circleGame, 14, 11);
            AddElem(crossGame, 12, 9);
            AddElem(circleGame, 14, 8);
            AddElem(crossGame, 15, 7);
            AddElem(circleGame, 11, 11);
            AddElem(crossGame, 10, 12);
            AddElem(circleGame, 13, 12);
            AddElem(crossGame, 13, 13);
            AddElem(circleGame, 15, 10);
            AddElem(crossGame, 16, 9);
            AddElem(circleGame, 12, 13);
            AddElem(crossGame, 11, 14);
            AddElem(circleGame, 14, 12);
            AddElem(crossGame, 15, 12);
            AddElem(circleGame, 15, 13);
            AddElem(crossGame, 16, 14);
            AddElem(circleGame, 16, 11);
            AddElem(crossGame, 17, 12);
            AddElem(circleGame, 15, 11);
            AddElem(crossGame, 17, 11);
            AddElem(circleGame, 17, 9);
            AddElem(crossGame, 16, 10);
            AddElem(circleGame, 14, 14);
            AddElem(crossGame, 13, 15);
            AddElem(circleGame, 14, 13);
            AddElem(crossGame, 14, 15);
            AddElem(circleGame, 15, 14);
            AddElem(crossGame, 16, 15);
            AddElem(circleGame, 15, 15);
            AddElem(crossGame, 15, 16);
            AddElem(circleGame, 16, 16);
            AddElem(crossGame, 17, 17);
            AddElem(circleGame, 17, 15);
            AddElem(crossGame, 18, 14);
            AddElem(circleGame, 18, 8);
            AddElem(crossGame, 17, 14);
            AddElem(circleGame, 18, 13);
            AddElem(crossGame, 16, 13);
            AddElem(circleGame, 19, 7);
            AddElem(crossGame, 20, 6);
            AddElem(circleGame, 19, 9);
            AddElem(crossGame, 17, 7);
            AddElem(circleGame, 19, 8);
            AddElem(crossGame, 19, 10);
            AddElem(circleGame, 20, 10);
            AddElem(crossGame, 20, 8);
            AddElem(circleGame, 16, 8);
            AddElem(crossGame, 17, 8);
            AddElem(circleGame, 18, 9);
            AddElem(crossGame, 18, 7);
            AddElem(circleGame, 21, 11);
            AddElem(crossGame, 22, 12);
            AddElem(circleGame, 18, 10);
            //AddElem(crossGame, 21, 6);
            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt, 19, 11));
        }

        public static void Test33()
        {
            Init();
            AddElem(circleGame, 14, 11);
            AddElem(crossGame, 13, 10);
            AddElem(circleGame, 14, 9);

            grid.CrossTurn = true;
            Point pt = RecalcAndMove();

            grid.CrossTurn = false;
            Assert(IsPointEqual(pt, 14, 8) || IsPointEqual(pt, 14, 12));
        }

        public static void Test34()
        {
            Init();
            AddElem(circleGame, 13, 12);
            AddElem(crossGame, 12, 11);
            AddElem(circleGame, 14, 13);
            AddElem(crossGame, 13, 13);
            AddElem(circleGame, 16, 11);
            AddElem(crossGame, 15, 12);
            AddElem(circleGame, 15, 10);
            AddElem(crossGame, 14, 11);
            AddElem(circleGame, 17, 12);
            AddElem(crossGame, 18, 13);
            AddElem(circleGame, 14, 9);
            AddElem(crossGame, 13, 8);
            AddElem(circleGame, 15, 8);
            AddElem(crossGame, 15, 9);
            AddElem(circleGame, 16, 9);

            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt, 17, 10));
        }

        public static void Test35()
        {
            Init();
            AddElem(circleGame, 13, 13);
            AddElem(crossGame, 12, 12);
            AddElem(circleGame, 11, 11);
            AddElem(crossGame, 10, 10);
            AddElem(circleGame, 13, 11);
            AddElem(crossGame, 13, 9);
            AddElem(circleGame, 13, 12);
            AddElem(crossGame, 13, 14);
            AddElem(circleGame, 12, 11);
            AddElem(crossGame, 14, 11);
            AddElem(circleGame, 14, 13);
            AddElem(crossGame, 15, 14);
            AddElem(circleGame, 11, 10);
            AddElem(crossGame, 10, 9);
            AddElem(circleGame, 11, 9);
            AddElem(crossGame, 11, 12);
            AddElem(circleGame, 12, 10);
            AddElem(crossGame, 14, 12);
            AddElem(circleGame, 11, 8);
            AddElem(crossGame, 11, 7);
            AddElem(circleGame, 10, 8);
            AddElem(crossGame, 9, 7);
            AddElem(circleGame, 12, 8);
            AddElem(crossGame, 13, 8);
            AddElem(circleGame, 12, 9);
            AddElem(crossGame, 12, 7);
            AddElem(circleGame, 10, 7);
            AddElem(crossGame, 9, 6);
            AddElem(circleGame, 13, 10);
            AddElem(crossGame, 10, 11);
            AddElem(circleGame, 14, 10);
            AddElem(crossGame, 15, 10);
            AddElem(circleGame, 12, 13);
            //AddElem(crossGame, 11, 13);
            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt, 14, 9));
        }

        public static void Test36()
        {
          
            Init();
         AddElem(circleGame,14, 13);
         AddElem(crossGame,13, 12);
         AddElem(circleGame,13, 13);
         AddElem(crossGame,15, 13);
         AddElem(circleGame,14, 12);
         AddElem(crossGame,14, 14);
         AddElem(circleGame,15, 11);
         AddElem(crossGame,16, 10);
         AddElem(circleGame,12, 14);
         AddElem(crossGame,11, 15);
         AddElem(circleGame,13, 15);
         AddElem(crossGame,11, 13);
         AddElem(circleGame,14, 16);
         AddElem(crossGame,15, 17);
         AddElem(circleGame,12, 16);
         AddElem(crossGame,12, 15);
         AddElem(circleGame,13, 16);
         AddElem(crossGame,15, 16);
         AddElem(circleGame,13, 17);
         AddElem(crossGame,13, 14);
         AddElem(circleGame,13, 18);
         AddElem(crossGame,13, 19);
         AddElem(circleGame,12, 18);
         //AddElem(crossGame,15, 14);
         grid.CrossTurn = true;
         Point pt = RecalcAndMove();
         grid.CrossTurn = false;
            Assert(IsPointEqual(pt,15, 15));     
 
        }

        public static void Test37()
        {
            Init();
            AddElem(circleGame, 13, 10);
            AddElem(crossGame, 12, 9);
            AddElem(circleGame, 12, 11);
            AddElem(crossGame, 14, 9);
            AddElem(circleGame, 14, 11);
            AddElem(crossGame, 13, 11);
            AddElem(circleGame, 13, 12);
            AddElem(crossGame, 14, 13);
            AddElem(circleGame, 12, 13);
            AddElem(crossGame, 11, 14);
            AddElem(circleGame, 15, 10);
            AddElem(crossGame, 16, 9);
            AddElem(circleGame, 14, 10);
            AddElem(crossGame, 12, 10);
            AddElem(circleGame, 16, 10);
            AddElem(crossGame, 17, 10);
            AddElem(circleGame, 15, 11);
            AddElem(crossGame, 13, 9);
            AddElem(circleGame, 15, 9);
            AddElem(crossGame, 11, 9);
            AddElem(circleGame, 10, 9);
            AddElem(crossGame, 10, 8);
            AddElem(circleGame, 9, 7);
            //AddElem(crossGame, 12, 6);
            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt, 14, 12));
        }

        public static void Test38()
        {
            Init();
            AddElem(circleGame, 13, 12);
            AddElem(crossGame, 12, 11);
            AddElem(circleGame, 11, 12);
            AddElem(crossGame, 10, 12);
            AddElem(circleGame, 13, 13);
            AddElem(crossGame, 13, 14);
            AddElem(circleGame, 11, 13);
            AddElem(crossGame, 12, 13);
            AddElem(circleGame, 12, 12);
            AddElem(crossGame, 11, 11);
            AddElem(circleGame, 13, 11);
            AddElem(crossGame, 14, 10);
            AddElem(circleGame, 10, 14);
            AddElem(crossGame, 9, 15);
            AddElem(circleGame, 12, 14);
            AddElem(crossGame, 14, 12);
            AddElem(circleGame, 11, 15);
            AddElem(crossGame, 11, 14);
            AddElem(circleGame, 12, 16);
            AddElem(crossGame, 13, 17);
            AddElem(circleGame, 14, 14);
            AddElem(crossGame, 13, 15);
            AddElem(circleGame, 10, 16);
            AddElem(crossGame, 9, 17);
            AddElem(circleGame, 10, 15);
            AddElem(crossGame, 10, 17);
            AddElem(circleGame, 9, 13);
            AddElem(crossGame, 8, 12);
            AddElem(circleGame, 11, 16);
            AddElem(crossGame, 13, 16);
            AddElem(circleGame, 13, 18);
            //AddElem(crossGame, 11, 17);

            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt, 12, 17));
        }

        public static void Test39()
        {
            Init();
            AddElem(circleGame, 13, 12);
            AddElem(crossGame, 12, 11);
            AddElem(circleGame, 11, 12);
            AddElem(crossGame, 10, 12);
            AddElem(circleGame, 13, 13);
            AddElem(crossGame, 13, 14);
            AddElem(circleGame, 11, 13);
            AddElem(crossGame, 12, 13);
            AddElem(circleGame, 12, 12);
            AddElem(crossGame, 11, 11);
            AddElem(circleGame, 13, 11);
            AddElem(crossGame, 14, 10);
            AddElem(circleGame, 10, 14);
            AddElem(crossGame, 9, 15);
            AddElem(circleGame, 12, 14);
            AddElem(crossGame, 14, 12);
            AddElem(circleGame, 11, 15);
            AddElem(crossGame, 11, 14);
            AddElem(circleGame, 12, 16);
            AddElem(crossGame, 13, 17);
            AddElem(circleGame, 14, 14);
            AddElem(crossGame, 13, 15);
            AddElem(circleGame, 10, 16);
            AddElem(crossGame, 9, 17);
            AddElem(circleGame, 10, 15);
            AddElem(crossGame, 10, 17);
            AddElem(circleGame, 9, 13);
            AddElem(crossGame, 8, 12);
            AddElem(circleGame, 11, 16);
            AddElem(crossGame, 13, 16);
            AddElem(circleGame, 13, 18);
            AddElem(crossGame, 12, 17);
            AddElem(circleGame, 11, 17);
            AddElem(crossGame, 15, 15);
            AddElem(circleGame, 11, 18);
            AddElem(crossGame, 11, 19);
            AddElem(circleGame, 10, 18);
            AddElem(crossGame, 12, 18);
            AddElem(circleGame, 14, 16);
            AddElem(crossGame, 14, 18);
            AddElem(circleGame, 9, 19);
            AddElem(crossGame, 8, 20);
            AddElem(circleGame, 10, 20);
            AddElem(crossGame, 8, 18);
            AddElem(circleGame, 11, 21);
            AddElem(crossGame, 12, 22);
            AddElem(circleGame, 12, 20);
            AddElem(crossGame, 11, 20);
            AddElem(circleGame, 10, 22);
            AddElem(crossGame, 9, 23);
            AddElem(circleGame, 10, 21);
            AddElem(crossGame, 10, 19);
            AddElem(circleGame, 12, 21);
            AddElem(crossGame, 9, 21);
            AddElem(circleGame, 13, 20);
            AddElem(crossGame, 11, 22);
            AddElem(circleGame, 14, 21);
            AddElem(crossGame, 13, 21);
            AddElem(circleGame, 12, 19);
            AddElem(crossGame, 15, 22);
            AddElem(circleGame, 14, 20);
            AddElem(crossGame, 15, 20);
            AddElem(circleGame, 14, 22);
            AddElem(crossGame, 14, 23);
            AddElem(circleGame, 15, 18);
            AddElem(crossGame, 14, 19);
            AddElem(circleGame, 16, 19);
            AddElem(crossGame, 17, 20);
            AddElem(circleGame, 16, 21);
            AddElem(crossGame, 16, 18);
            AddElem(circleGame, 16, 20);
            AddElem(crossGame, 15, 21);
            AddElem(circleGame, 15, 23);
            AddElem(crossGame, 16, 22);
            AddElem(circleGame, 16, 24);
            AddElem(crossGame, 17, 25);
            AddElem(circleGame, 17, 23);
            AddElem(crossGame, 18, 22);
            AddElem(circleGame, 15, 16);
            AddElem(crossGame, 14, 17);
            AddElem(circleGame, 16, 16);
            AddElem(crossGame, 17, 16);
            AddElem(circleGame, 18, 17);
            AddElem(crossGame, 17, 17);
            AddElem(circleGame, 18, 18);
            AddElem(crossGame, 17, 18);
            AddElem(circleGame, 17, 19);
            AddElem(crossGame, 17, 15);
            AddElem(circleGame, 17, 14);
            AddElem(crossGame, 16, 15);
            AddElem(circleGame, 14, 15);
            AddElem(crossGame, 18, 19);
            AddElem(circleGame, 19, 17);
            AddElem(crossGame, 20, 16);
            AddElem(circleGame, 19, 15);
            AddElem(crossGame, 19, 20);
            AddElem(circleGame, 20, 21);
            AddElem(crossGame, 16, 17);
            AddElem(circleGame, 15, 17);
            AddElem(crossGame, 12, 15);
            AddElem(circleGame, 10, 13);
            AddElem(crossGame, 8, 13);
            AddElem(circleGame, 18, 15);
            AddElem(crossGame, 19, 16);
            AddElem(circleGame, 18, 16);
            AddElem(crossGame, 18, 14);
            AddElem(circleGame, 20, 19);
            AddElem(crossGame, 20, 18);
            AddElem(circleGame, 19, 22);
            AddElem(crossGame, 18, 23);
            AddElem(circleGame, 21, 20);
            AddElem(crossGame, 23, 22);
            AddElem(circleGame, 21, 22);

            AddElem(crossGame, 22, 21);
            grid.CrossTurn = false;
            Assert(false);
            //grid.CrossTurn = true;
            //var pt = RecalcAndMove();
            //grid.CrossTurn = false;
            //Assert(IsPointEqual(pt, 22, 21));
        }
        public static void Test40()
        {
            Init();
            AddElem(circleGame, 13, 14);
            AddElem(crossGame, 12, 13);
            AddElem(circleGame, 12, 15);
            AddElem(crossGame, 14, 13);
            AddElem(circleGame, 13, 13);
            AddElem(crossGame, 13, 15);
            AddElem(circleGame, 14, 14);
            AddElem(crossGame, 12, 14);
            AddElem(circleGame, 15, 15);
            AddElem(crossGame, 16, 16);
            AddElem(circleGame, 14, 16);
            AddElem(crossGame, 16, 14);
            AddElem(circleGame, 13, 17);
            AddElem(crossGame, 12, 18);
            AddElem(circleGame, 12, 16);
            AddElem(crossGame, 13, 16);
            AddElem(circleGame, 14, 18);
            AddElem(crossGame, 15, 19);
            AddElem(circleGame, 14, 17);
            AddElem(crossGame, 14, 15);
            AddElem(circleGame, 15, 17);
            AddElem(crossGame, 16, 17);
            AddElem(circleGame, 16, 15);
            //AddElem(crossGame, 11, 15);

            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt, 15, 16) || IsPointEqual(pt,13,18));
        }
        public static void Test41()
        {
            Init();
            AddElem(circleGame, 13, 13);
            AddElem(crossGame, 12, 12);
            AddElem(circleGame, 11, 11);
            AddElem(crossGame, 10, 10);
            AddElem(circleGame, 12, 10);
            AddElem(crossGame, 13, 9);
            AddElem(circleGame, 13, 11);
            AddElem(crossGame, 14, 12);
            AddElem(circleGame, 12, 11);
            AddElem(crossGame, 14, 11);
            AddElem(circleGame, 10, 11);
            AddElem(crossGame, 9, 11);
            AddElem(circleGame, 10, 12);
            AddElem(crossGame, 9, 13);
            AddElem(circleGame, 11, 12);
            //AddElem(crossGame, 11, 10);
            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt, 13, 14));
        }

        public static void Test42()
        {
            Init();
            AddElem(circleGame, 12, 13);
            AddElem(crossGame, 11, 12);
            AddElem(circleGame, 10, 12);
            AddElem(crossGame, 11, 13);
            AddElem(circleGame, 11, 11);
            AddElem(crossGame, 9, 13);
            AddElem(circleGame, 12, 12);
            AddElem(crossGame, 12, 10);
            AddElem(circleGame, 13, 13);
            AddElem(crossGame, 14, 14);
            AddElem(circleGame, 10, 10);
            AddElem(crossGame, 9, 9);
            AddElem(circleGame, 10, 11);
            AddElem(crossGame, 10, 9);
            AddElem(circleGame, 12, 14);
            AddElem(crossGame, 12, 15);
            AddElem(circleGame, 14, 12);
            AddElem(crossGame, 15, 11);
            AddElem(circleGame, 13, 11);
            AddElem(crossGame, 12, 11);
            AddElem(circleGame, 10, 14);
            AddElem(crossGame, 10, 13);
            AddElem(circleGame, 9, 14);
            AddElem(crossGame, 11, 14);
            AddElem(circleGame, 9, 12);
            AddElem(crossGame, 13, 12);
            AddElem(circleGame, 11, 10);
            AddElem(crossGame, 8, 13);
            AddElem(circleGame, 7, 13);
            AddElem(crossGame, 9, 11);
            AddElem(circleGame, 12, 9);
            AddElem(crossGame, 13, 8);
            AddElem(circleGame, 11, 15);
            AddElem(crossGame, 10, 16);
            AddElem(circleGame, 7, 14);
            AddElem(crossGame, 7, 10);
            AddElem(circleGame, 7, 15);
            AddElem(crossGame, 7, 16);
            AddElem(circleGame, 8, 14);
            AddElem(crossGame, 6, 14);
            AddElem(circleGame, 9, 15);
            AddElem(crossGame, 6, 12);
            AddElem(circleGame, 8, 15);
            AddElem(crossGame, 10, 15);
            AddElem(circleGame, 9, 16);
            AddElem(crossGame, 10, 17);
            AddElem(circleGame, 9, 17);
            AddElem(crossGame, 9, 18);
            AddElem(circleGame, 11, 16);
            AddElem(crossGame, 6, 15);
            AddElem(circleGame, 6, 13);
            AddElem(crossGame, 5, 12);
            AddElem(circleGame, 8, 16);
            AddElem(crossGame, 5, 14);
            AddElem(circleGame, 8, 17);
           // AddElem(crossGame, 5, 13);
            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt, 8, 18));
        }

        public static void Test43()
        {
            Init();
            AddElem(circleGame, 13, 12);
            AddElem(crossGame, 12, 11);
            AddElem(circleGame, 10, 12);
            AddElem(crossGame, 9, 12);
            AddElem(circleGame, 12, 13);
            AddElem(crossGame, 11, 14);
            AddElem(circleGame, 11, 13);
            AddElem(crossGame, 13, 13);
            AddElem(circleGame, 13, 14);
            AddElem(crossGame, 11, 12);
            AddElem(circleGame, 14, 15);
            AddElem(crossGame, 13, 15);
            AddElem(circleGame, 16, 14);
            AddElem(crossGame, 15, 14);
            AddElem(circleGame, 16, 16);
            AddElem(crossGame, 16, 17);
            AddElem(circleGame, 15, 16);
            AddElem(crossGame, 14, 16);
            AddElem(circleGame, 14, 13);
            AddElem(crossGame, 15, 12);
            AddElem(circleGame, 17, 15);
            AddElem(crossGame, 18, 14);
            AddElem(circleGame, 18, 16);
            AddElem(crossGame, 19, 17);
            AddElem(circleGame, 17, 16);
            AddElem(crossGame, 19, 16);
            AddElem(circleGame, 17, 14);
            AddElem(crossGame, 17, 17);
            AddElem(circleGame, 18, 17);
            AddElem(crossGame, 16, 15);
            AddElem(circleGame, 17, 13);
            AddElem(crossGame, 17, 12);
            AddElem(circleGame, 18, 12);
            AddElem(crossGame, 19, 11);
            AddElem(circleGame, 18, 18);
            AddElem(crossGame, 18, 19);
            AddElem(circleGame, 19, 18);
            AddElem(crossGame, 20, 19);
            AddElem(circleGame, 20, 18);
            AddElem(crossGame, 21, 18);
            AddElem(circleGame, 17, 18);
            AddElem(crossGame, 16, 18);
            AddElem(circleGame, 15, 17);
            AddElem(crossGame, 15, 20);
            AddElem(circleGame, 12, 15);
            AddElem(crossGame, 12, 14);
            AddElem(circleGame, 12, 17);
            AddElem(crossGame, 11, 16);
            AddElem(circleGame, 14, 18);
            AddElem(crossGame, 13, 19);
            AddElem(circleGame, 13, 17);
            AddElem(crossGame, 14, 17);
            AddElem(circleGame, 12, 16);
            AddElem(crossGame, 11, 15);
            AddElem(circleGame, 15, 19);
            AddElem(crossGame, 16, 20);
            AddElem(circleGame, 15, 18);
            AddElem(crossGame, 15, 15);
            AddElem(circleGame, 12, 18);
            AddElem(crossGame, 12, 19);
            AddElem(circleGame, 11, 18);
            AddElem(crossGame, 13, 18);
            AddElem(circleGame, 10, 19);
            AddElem(crossGame, 13, 16);
            AddElem(circleGame, 10, 16);
            AddElem(crossGame, 10, 18);
            AddElem(circleGame, 11, 17);
            AddElem(crossGame, 10, 17);
            AddElem(circleGame, 9, 15);
            AddElem(crossGame, 8, 14);
            AddElem(circleGame, 10, 14);
            AddElem(crossGame, 8, 16);
            AddElem(circleGame, 13, 11);
            AddElem(crossGame, 12, 12);
            AddElem(circleGame, 14, 7);
            AddElem(crossGame, 8, 15);
            AddElem(circleGame, 8, 13);
            AddElem(crossGame, 10, 13);
            AddElem(circleGame, 9, 14);
            AddElem(crossGame, 7, 15);
            AddElem(circleGame, 9, 17);
            AddElem(crossGame, 9, 16);
            AddElem(circleGame, 13, 7);
            AddElem(crossGame, 12, 20);
            AddElem(circleGame, 13, 9);

            //AddElem(crossGame, 13, 20);
            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt, 13, 20) || IsPointEqual(pt, 14, 20));
            //Assert(circleGame.GetWatchedPoints().Where(i=>i.point == new Point(13,10)).Any());
            if (IsPointEqual(pt, 13, 20))
                AddElem(circleGame, 14, 20);
            else
                AddElem(circleGame, 13, 20);

            grid.CrossTurn = true;
            pt = RecalcAndMove();
            grid.CrossTurn = false;

            Assert(IsPointEqual(pt, 14, 8) || IsPointEqual(pt, 13, 10) || IsPointEqual(pt, 13, 8));
        }

        public static void Test44()
        {
            try
            {
                Init();
                AddElem(circleGame, 15, 14);
                AddElem(crossGame, 14, 13);
                AddElem(circleGame, 16, 13);
                AddElem(crossGame, 17, 12);
                AddElem(circleGame, 16, 12);
                AddElem(crossGame, 16, 14);
                AddElem(circleGame, 17, 14);
                AddElem(crossGame, 15, 12);
                AddElem(circleGame, 18, 15);
                //AddElem(crossGame, 19, 16);
                //AddElem(circleGame, 17, 15);
                //AddElem(crossGame, 17, 16);
                //AddElem(circleGame, 18, 14);
                //AddElem(crossGame, 19, 15);
                //AddElem(circleGame, 18, 13);
                //AddElem(crossGame, 18, 12);
                //AddElem(circleGame, 19, 13);
                //AddElem(crossGame, 17, 13);
                grid.CrossTurn = true;
                Point pt = RecalcAndMove();
                grid.CrossTurn = false;
                Assert(IsPointEqual(pt, 18, 14) || IsPointEqual(pt, 18, 13));
            }
            catch (Exception exc)
            {
                //Log.AddError(exc);
				Debug.Log (exc.Message);
                throw;
            }
        }

        public static void Test45()
        {
            Init();
            AddElem(circleGame, 10, 12);
            AddElem(crossGame, 9, 11);
            AddElem(circleGame, 13, 12);
            AddElem(crossGame, 9, 12);
            AddElem(circleGame, 15, 12);
            AddElem(crossGame, 14, 12);
            AddElem(circleGame, 11, 12);
            //AddElem(crossGame, 9, 10);
            //AddElem(circleGame, 9, 13);
            //AddElem(crossGame, 9, 9);
            //AddElem(circleGame, 9, 8);
            //AddElem(crossGame, 12, 11);
            //AddElem(circleGame, 13, 13);
            //AddElem(crossGame, 13, 14);
            //AddElem(circleGame, 12, 13);
            //AddElem(crossGame, 11, 13);
            //AddElem(circleGame, 11, 14);
            //AddElem(crossGame, 14, 11);
            //AddElem(circleGame, 10, 14);
            //AddElem(crossGame, 8, 14);
            //AddElem(circleGame, 11, 15);
            //AddElem(crossGame, 12, 16);
            //AddElem(circleGame, 10, 15);
            //AddElem(crossGame, 9, 16);
            //AddElem(circleGame, 10, 13);
            //AddElem(crossGame, 10, 16);
            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt,12, 12));
        }

        public static void Test46()
        {
            Init();
            AddElem(circleGame, 10, 12);
            AddElem(crossGame, 9, 11);
            AddElem(circleGame, 13, 12);
            AddElem(crossGame, 9, 12);
            AddElem(circleGame, 15, 12);
            AddElem(crossGame, 14, 12);
            AddElem(circleGame, 11, 12);
            AddElem(crossGame, 9, 10);
            AddElem(circleGame, 9, 13);
            //AddElem(crossGame, 8, 11);

            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt, 11, 11) || IsPointEqual(pt,10, 13));
        }

		//TODO
        public static void Test47()
        {
            Init();
			AddElem(circleGame, grid.Width-1, grid.Height-1);//29
			AddElem(crossGame, grid.Width-2, grid.Height-2);
			AddElem(circleGame, grid.Width-1, grid.Height-1-3);
			AddElem(crossGame, grid.Width-2, grid.Height-1-4);
			AddElem(circleGame, grid.Width-1, grid.Height-1-2);//27
			AddElem(crossGame, grid.Width-1, grid.Height-1-6);
			AddElem(circleGame, grid.Width-1, grid.Height-1-4);
			AddElem(crossGame, grid.Width-1, grid.Height-1-1);
			AddElem(circleGame, grid.Width-2, grid.Height-1-2);
			AddElem(crossGame, grid.Width-2, grid.Height-1-5);
			AddElem(circleGame, grid.Height-1-2, grid.Height-1-2);
			AddElem(crossGame, grid.Width-2, grid.Height-1-6);
			AddElem(circleGame, grid.Width-2, 26);
			AddElem(crossGame, grid.Width-2, 22);
			AddElem(circleGame, grid.Width-2, 21);
			AddElem(crossGame, grid.Width-1, grid.Height-1-5);
			AddElem(circleGame, grid.Width-1-3, grid.Width-2);
            //AddElem(crossGame, 27, 23);

            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt, 25, 29));
        }

        public static void Test48()
        {
            Init();
            AddElem(circleGame, 29, 0);
            AddElem(crossGame, 28, 0);
            AddElem(circleGame, 28, 1);
            AddElem(crossGame, 29, 1);
            AddElem(circleGame, 27, 2);
            AddElem(crossGame, 27, 0);
            AddElem(circleGame, 26, 3);
            //AddElem(crossGame, 27, 3);
            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt, 25, 4));
        }

        public static void Test49()
        {
            Init();
            AddElem(circleGame, 13, 12);
            AddElem(circleGame, 16, 12);
            AddElem(crossGame, 12, 11);
            AddElem(circleGame, 16, 15);
            AddElem(crossGame, 12, 12);
            AddElem(circleGame, 13, 15);
            AddElem(crossGame, 16, 11);
            AddElem(circleGame, 16, 13);
            AddElem(crossGame, 12, 15);
            AddElem(circleGame, 12, 14);
            AddElem(crossGame, 12, 13);
            AddElem(circleGame, 13, 14);
            AddElem(crossGame, 15, 13);
            AddElem(circleGame, 14, 14);
            AddElem(crossGame, 13, 13);
            AddElem(circleGame, 16, 14);
            AddElem(crossGame, 15, 14);
            AddElem(circleGame, 17, 13);
            AddElem(crossGame, 16, 16);
            AddElem(circleGame, 18, 12);
            //AddElem(crossGame, 14, 16);
            //AddElem(circleGame, 18, 14);
            //AddElem(crossGame, 19, 11);
            //AddElem(circleGame, 18, 13);
            //AddElem(crossGame, 19, 15);
            //AddElem(circleGame, 18, 11);
            //AddElem(crossGame, 18, 15);
            //AddElem(circleGame, 19, 12);

            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt, 9, 121));
        }

        public static void Test50()
        {
            Init();
            AddElem(circleGame, 13, 12);
            AddElem(circleGame, 16, 12);
            AddElem(crossGame, 12, 11);
            AddElem(circleGame, 16, 15);
            AddElem(crossGame, 12, 12);
            AddElem(circleGame, 13, 15);
            AddElem(crossGame, 16, 11);
            AddElem(circleGame, 16, 13);
            AddElem(crossGame, 12, 15);
            AddElem(circleGame, 12, 14);
            AddElem(crossGame, 12, 13);
            AddElem(circleGame, 13, 14);
            AddElem(crossGame, 15, 13);
            AddElem(circleGame, 14, 14);
            AddElem(crossGame, 13, 13);
            AddElem(circleGame, 16, 14);
            AddElem(crossGame, 15, 14);
            AddElem(circleGame, 17, 13);
            AddElem(crossGame, 16, 16);
            AddElem(circleGame, 18, 12);
            AddElem(crossGame, 15, 15);
            AddElem(circleGame, 15, 16);
            AddElem(crossGame, 19, 12);
            AddElem(circleGame, 18, 14);
            AddElem(crossGame, 19, 15);
            AddElem(circleGame, 17, 14);
           // AddElem(crossGame, 14, 17);

            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt, 18,13));
        }

        public static void Test51()
        {
            Init();
            AddElem(circleGame, 14, 13);
            AddElem(crossGame, 13, 12);
            AddElem(circleGame, 14, 12);
            AddElem(crossGame, 14, 11);
            AddElem(circleGame, 15, 12);
            //AddElem(crossGame, 16, 11);
            //AddElem(circleGame, 15, 13);
            //AddElem(crossGame, 13, 13);
            //AddElem(circleGame, 16, 14);
            //AddElem(crossGame, 17, 15);
            //AddElem(circleGame, 15, 14);
            //AddElem(crossGame, 15, 15);
            //AddElem(circleGame, 14, 14);
            //AddElem(crossGame, 13, 14);
            //AddElem(circleGame, 13, 11);
            //AddElem(crossGame, 12, 10);
            //AddElem(circleGame, 16, 12);
            //AddElem(crossGame, 17, 11);
            //AddElem(circleGame, 18, 14);
            //AddElem(crossGame, 17, 14);
            //AddElem(circleGame, 17, 13);
            //AddElem(crossGame, 15, 11);
            //AddElem(circleGame, 18, 11);
            //AddElem(crossGame, 18, 12);
            //AddElem(circleGame, 16, 13);
            //AddElem(crossGame, 18, 13);


            //AddElem(crossGame, 15, 13);
            //grid.CrossTurn = false;
            //Assert(false);

            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt,15, 13));
        }

        public static void Test52()
        {
            Init();
            //AddElem(circleGame, 16, 16);
            AddElem(circleGame, 15, 18);
            AddElem(crossGame, 15, 15);
            AddElem(circleGame, 15, 16);
            AddElem(crossGame, 15, 17);
            AddElem(circleGame, 14, 17);
            AddElem(crossGame, 16, 17);
            AddElem(circleGame, 17, 16);
            AddElem(crossGame, 13, 16);
            AddElem(circleGame, 17, 17);
            AddElem(crossGame, 14, 16);
            AddElem(circleGame, 16, 18);
            AddElem(crossGame, 17, 14);
            AddElem(circleGame, 17, 18);
            AddElem(crossGame, 13, 18);
            AddElem(circleGame, 17, 19);
            AddElem(crossGame, 18, 18);
            AddElem(circleGame, 17, 15);
            //AddElem(crossGame, 17, 20);
            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            Assert(IsPointEqual(pt, 9, 121));
        }

        public static void Test53()
        {
            Init();
            AddElem(circleGame, 14, 14);
            AddElem(circleGame, 16, 16);
            AddElem(crossGame, 13, 13);
            AddElem(circleGame, 16, 18);
            AddElem(crossGame, 15, 15);
            AddElem(circleGame, 15, 17);
            AddElem(crossGame, 16, 17);
            AddElem(circleGame, 18, 16);
            AddElem(crossGame, 14, 16);
            AddElem(circleGame, 18, 17);
            AddElem(crossGame, 14, 18);
            AddElem(circleGame, 20, 20);
            AddElem(crossGame, 18, 18);
            AddElem(circleGame, 20, 22);
            AddElem(crossGame, 15, 19);
            AddElem(circleGame, 19, 21);
            //AddElem(crossGame, 18, 20);
            //AddElem(circleGame, 22, 20);
            //AddElem(crossGame, 21, 21);
            //AddElem(circleGame, 22, 21);
            //AddElem(crossGame, 22, 18);
           // AddElem(circleGame, 21, 22);

            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;
            //AddElem(crossGame, 20, 23);

            Assert(IsPointEqual(pt, 13, 17));
        }

        //template
        public static void Test0sss9()
        {
            Init();
            
            grid.CrossTurn = true;
            Point pt = RecalcAndMove();
            grid.CrossTurn = false;

            Assert(IsPointEqual(pt, 9, 12));
        }

    }
}
