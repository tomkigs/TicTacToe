﻿//using Microsoft.Xna.Framework;
using Assets.Scripts.Game;
using Assets.Scripts.Game.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts;
using VikingChess;
using Assets.Scripts.Game.Core.Tests;

namespace TicTacToeXAML.Tests
{
	class Simple : TestBase
	{
		public static void RunAll()
		{
			//Test0();//3d
			TestMatchAfter();
			TestWeighs2();
			TestWeighs3();
			TestDeffence3();
			TestDeffence4();
			TestDeffence5();
			TestWeighs4();
		}

		public static void TestWeighs2()
		{
			Init();
			AddElem(circleGame, 8, 8);
			AddElem(circleGame, 9, 8);
			//AddElem(circleGame, 10, 8);

			Recalc();
			var pw = circleGame.GetPointWeight(new Point(7,8));
			var res9 = pw.GetResult(LineType.w9);
			Assert(res9.contWeight == 2);
			Assert(res9.effectiveWeight == LineCheckResult.OpenTwoEffectiveWeight);
			Assert(pw.totalWeight == LineCheckResult.OpenTwoEffectiveWeight);
			var res9Peeked = pw.PeekedWeight.GetResult(LineType.w9);
			Assert(res9Peeked.contWeight == 3);
			Assert(res9Peeked.effectiveWeight == LineCheckResult.OpenThreeEffectiveWeight);
			Assert(pw.PeekedWeight.totalWeight == LineCheckResult.OpenThreeEffectiveWeight + 3);
			int kk = 0;
			Debug.Log (kk);
		}

		public static void TestWeighs3()
		{
			Init();
			AddElem(circleGame, 8, 8);
			AddElem(circleGame, 9, 8);
			AddElem(circleGame, 10, 8);

			Recalc();

			var pw = circleGame.GetPointWeight(new Point(11, 8));
			var res9 = pw.GetResult(LineType.w9);
			Assert(res9.contWeight == 3);
			Assert(res9.effectiveWeight == LineCheckResult.OpenThreeEffectiveWeight);
			Assert(pw.totalWeight == LineCheckResult.OpenThreeEffectiveWeight);
			var res9Peeked = pw.PeekedWeight.GetResult(LineType.w9);
			Assert(res9Peeked.contWeight == 4);
			Assert(res9Peeked.effectiveWeight == 4.5f);
			Assert(pw.PeekedWeight.totalWeight == 7.5);
			int kk = 0;
			Debug.Log (kk);
		}

		private static void TestDeffence3()
		{
			Init();

			AddElem(circleGame, 8, 8);
			AddElem(circleGame, 9, 8);
			AddElem(circleGame, 10, 8);
			//AddElem(circleGame, 11, 8);

			AddElem(crossGame, 8, 9);
			AddElem(crossGame, 9, 9);
			// AddElem(crossGame, 10, 9);

			Recalc();

			Point pt = pcPlayer.MakeComputerMove();
			//assert block
			Assert(IsPointEqual(pt, 7, 8) || IsPointEqual(pt, 11, 8));
			//Assert(IsPointEqual(pt, 7, 9) || IsPointEqual(pt, 10, 9));
		}

		private static void TestDeffence4()
		{
			Init();

			AddElem(circleGame, 8, 8);
			AddElem(circleGame, 9, 8);
			AddElem(circleGame, 10, 8);
			AddElem(circleGame, 11, 8);

			AddElem(crossGame, 8, 9);
			AddElem(crossGame, 9, 9);
			AddElem(crossGame, 10, 9);

			Recalc();

			Point pt = pcPlayer.MakeComputerMove();
			//assert block
			Assert(IsPointEqual(pt, 7, 8) || IsPointEqual(pt, 12, 8));
		}

		public static void TestMatchAfter()
		{
			Init();
			//AddElem(circleGame, 9, 9);
			//AddElem(circleGame, 10, 8);
			//AddElem(circleGame, 10, 10);
			//AddElem(circleGame, 11, 9);

			AddElem(crossGame, 13, 7);
			AddElem(crossGame, 13, 9);
			AddElem(crossGame, 13, 11);
			AddElem(crossGame, 13, 12);

			Recalc();
			var pw = compGame.CreatePointWeight(new Point(13, 10));
			var res = pw.GetBestNonContWeight();
			//Assert(res.pattern == Pattern.ZEZEZ);

			//LastAdded X: x= 10, y= 12     
			//Point pt = pcPlayer.MakeComputerMove();
			//Assert(IsPointEqual(pt, 10, 9));
		}

		public static void TestDeffence5()
		{
			Init();
			AddElem(circleGame, 9, 9);
			AddElem(circleGame, 10, 8);
			AddElem(circleGame, 10, 10);
			AddElem(circleGame, 11, 9);

			AddElem(crossGame, 8, 8);
			AddElem(crossGame, 9, 10);
			AddElem(crossGame, 9, 11);

			Recalc();
			//LastAdded X: x= 10, y= 12     
			Point pt = pcPlayer.MakeComputerMove();
			Assert(IsPointEqual(pt, 10, 9));
		}
		public static void TestWeighs4()
		{
			Init();
			AddElem(circleGame, 8, 8);
			AddElem(circleGame, 10, 8);
			AddElem(circleGame, 12, 8);

			Recalc();

			var pw = circleGame.GetPointWeight(new Point(12, 8));
			var res9 = pw.GetResult(LineType.w9);
			Assert(res9.effectiveWeight == LineCheckResult.SlicedThreeEffectiveWeight);

		}

		public static bool Test0On = false;
		public static void Test0()
		{
			Test0On = true;
			Init();
			AddElem(circleGame, 14, 10);
			AddElem(crossGame, 13, 9);
			AddElem(circleGame, 15, 11);
			//AddElem(crossGame, 16, 12);

			Recalc();
			//LastAdded X: x= 10, y= 12     
			Point pt = pcPlayer.MakeComputerMove();
			Assert(IsPointEqual(pt, 14, 11));
			Test0On = false;
		}

	}
}

    