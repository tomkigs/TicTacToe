﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TicTacToeXAML;
using UnityEngine;

namespace Assets.Scripts.Game.Core.Tests
{
    class TestBase
    {
        public static string testName;
        public static TicTacToeGrid grid;
        public static PlayerGame crossGame;// = new PlayerGame();
        public static PlayerGame circleGame;// = new PlayerGame();
        public static PlayerGame compGame;
        public static PcPlayer pcPlayer;

        public static void InitMembers(TicTacToeGrid grid, PlayerGame crossGame, PlayerGame circleGame, PlayerGame compGame, PcPlayer pcPlayer)
        {
            TestBase.grid = grid;
            TestBase.crossGame = crossGame;
            TestBase.circleGame = circleGame;
            TestBase.compGame = compGame;
            TestBase.pcPlayer = pcPlayer;
        }

        public static GridElem AddElem(PlayerGame player, int x, int y)
        {
            var el = grid.AddElem(player, x, y);
            return new GridElem(el);
        }

        public static void Init()
        {
            Debug.Log("<<<Init test " + testName + ">>>");
            testName = "";
            GameSettings.Instance.DifficultyLevel = GameSettings.Difficulty.Normal;
            //grid.OnNewGame();
            VikingChess.GameManager.Instance.OnNewGame();// grid.Width, grid.Height, GameType.OnePlayer, Assets.Scripts.GameSettings.Difficulty.Normal);

        }

        public static bool IsPointEqual(Point pt, int p1, int p2)
        {
            return pt.X == p1 && pt.Y == p2;
        }

        public static void Assert(bool val)
        {
            try
            {
                if (!val)
                    throw new Exception("UT failed ");
            }
            catch (Exception exc)
            {
                Debug.LogError(exc.Message + " !DETAILS!:");
                Debug.Log(Environment.StackTrace);
                throw;
            }
        }

        public static void Recalc()
        {
            grid.RecalcPoints();
            //compGame.CalcCheckResults(false);
        }
    }
}
