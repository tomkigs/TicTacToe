﻿using UnityEngine;
using System.Collections;

public class GameStats  {
    public float TimeLeft = 1;
    float OrginalTimeLeft = 0;
    public GameStats(float timeLeft)
    {
        OrginalTimeLeft = timeLeft;
        TimeLeft = timeLeft;
    }

    public float TimeSpend { get { return OrginalTimeLeft - TimeLeft; } }

    public float PercentJobDone;

}
