﻿using UnityEngine;
using Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using Lean;
using Commons.Grid;
using Assets.Scripts.Game;
using TicTacToeXAML.Tests;
using Assets.Scripts;
using Assets.Scripts.Game.Core;
using TicTacToeXAML;
using Assets.Scripts.Common.Logging;
using TicTacToeXAML.Core;
using Zybex.GUI;
using CommonsPC;
using GooglePlayGames.BasicApi.Multiplayer;
using UnityEngine.EventSystems;

namespace VikingChess
{
  public class MaxSpecialHint
  {
    GameSettings.Difficulty difficulty;
    int Max;
    public int Current;

    public MaxSpecialHint(GameSettings.Difficulty difficulty, int max)
    {
      this.difficulty = difficulty;
      Max = max;
      Current = max;
    }

    public bool CanUse()
    {
      return Current > 0;
    }
  }

  public class GameManager : MonoBehaviour
  {
    public static GameManager Instance;
    public List<Entity> Entities = new List<Entity>();
    TicTacToeGrid grid;//hack public 
    public GameObject crossPrefab;
    public GameObject oPrefab;
    List<EntitiesFactory> imageFactories;
    List<GameObject> floatingImages = new List<GameObject>();
    public TimeTracker GameLasting = new TimeTracker();
    public Point suggestedMove = GameDefines.InvalidPoint;
    MaxSpecialHint maxAdviceMoveNormal;
    MaxSpecialHint maxUndoNormal;
    MaxSpecialHint maxAdviceMoveEasy;
    MaxSpecialHint maxUndoEasy;
    WinInfo winInfo;
    GameObject[] drawings;

    public void ClearEntities()
    {
      Entities.ForEach(i => Destroy(i.gameObject));
      Entities.Clear();
    }

    public void OnNewGame()
    {
      Debug.Log("OnNewGame!");
      ClearEntities();

      floatingImages.ForEach(i => Destroy(i));
      floatingImages.Clear();

      gameOver = false;
      winInfo = null;
      grid.OnNewGame();
      suggestedMove = GameDefines.InvalidPoint;
      GameLasting.Reset();

      maxAdviceMoveNormal = new MaxSpecialHint(GameSettings.Difficulty.Normal, 3);
      maxAdviceMoveEasy = new MaxSpecialHint(GameSettings.Difficulty.Easy, 5);
      maxUndoNormal = new MaxSpecialHint(GameSettings.Difficulty.Normal, 3);
      maxUndoEasy = new MaxSpecialHint(GameSettings.Difficulty.Easy, 5);
      if (drawings != null)
        drawings.ForEach(i => Destroy(i.gameObject));

      int maxImgOffset = 8;

      if (imageFactories.Count > 1)
      {
        drawings = EntitiesFactory.CreateRandomArray(new[] { imageFactories[0], imageFactories[1] }, new Vector2(maxImgOffset, maxImgOffset));
      }
      if (newGameCounter > 1)
        GoogleMobileAdsDemoScript.Instance.ShowInterstitial();

      newGameCounter++;

      if (InGamePanel.Instance != null)
        InGamePanel.Instance.Refresh();

      if (GameSettings.Instance.InitNetworkGame)
      {
#if UNITY_ANDROID
				GamePlayService.InviteFriend ();
				GameSettings.Instance.InitNetworkGame = false;
#endif
      }
    }

    void Awake()
    {
      Instance = this;
#if UNITY_ANDROID
      if (!LiderboardManager.Instance.EverLoaded)
        LiderboardManager.Instance.ReloadAsync();

      GoogleMobileAdsDemoScript.Instance.RequestInterstitial();
#endif
    }

    void Start()
    {
      var inf = GameObject.Find("4 - Infrastructure");

      GoogleMobileAdsDemoScript.Instance.InterstitialOpened += HandleInterstitialOpened;
      GoogleMobileAdsDemoScript.Instance.InterstitialOpening += HandleInterstitialOpening;
      grid = GameObject.FindGameObjectWithTag("grid").GetComponent<TicTacToeGrid>();
      grid.SetBackgroundColor(true);
      imageFactories = inf.GetComponents<EntitiesFactory>().ToList();
      OnNewGame();

      GamePlayService.InvitationHandler = (object sender, EventArgs<Invitation, bool> e) =>
      {
        //todo ask...

        GameSettings.Instance.Gametype = TicTacToeXAML.GameType.TwoPlayersPlayServices;
        OnNewGame();

      };
    }


    public int GetNumberOfElems()
    {
      return this.Entities.Count;
    }

    public MaxSpecialHint GetUndoHintInfo()
    {
      return GameSettings.Instance.DifficultyLevel == GameSettings.Difficulty.Normal ? maxUndoNormal : maxUndoEasy;
    }
    public void UndoMove()
    {
      try
      {
        if (Entities.Count == 0)
        {
          PlayBeep();
          return;
        }
        MaxSpecialHint hintUndo = GetUndoHintInfo();
        if (!hintUndo.CanUse())
        {
          PlayBeep();
          return;
        }

        if (grid.UndoLastMove())
          hintUndo.Current--;

        MakeCompMoveIfNeeded();
      }
      catch (Exception exc)
      {
        Log.AddError(exc);
      }
    }
    public static int newGameCounter = 0;

    public Entity GetEntityAt(int x, int y)
    {
      var item = grid.entitiesBuffer[y, x];
      return item;
    }

    private void SnapEntityToGrid(Entity en)
    {
      en.SnappedTo = grid.FindSnappingItem(en.transform.position);
      if (en.SnappedTo == null)
        Debug.LogError("en.snappedTo == null name = " + en.name);
      else
        en.transform.MoveToXY(en.SnappedTo.transform.position.x, en.SnappedTo.transform.position.y);
    }

    public Entity getEntityAt(int x, int y)
    {
      return Entities.Where(i => i.SnappedTo != null && i.SnappedTo.x == x && i.SnappedTo.y == y).FirstOrDefault();
    }

    public void HandleInterstitialOpening(object sender, EventArgs args)
    {
      try
      {
        print("HandleInterstitialOpened event received");
        if (GameSettings.Instance.Gametype == GameType.OnePlayer)
          Forms.Instance.Pause();
        //ApplicationService.Instance.Pause();
      }
      catch (Exception exc)
      {
        Debug.Log(exc);
      }
    }

    public void HandleInterstitialOpened(object sender, EventArgs args)
    {
    }

    public List<Entity> GetAdjustingOnX(Entity entity, IEnumerable<Entity> enemies)
    {
      var x = entity.SnappedTo.x;
      var y = entity.SnappedTo.y;

      var adjustingOnX = enemies.Where(i =>
     (i.SnappedTo.x == x + 1 || i.SnappedTo.x == x - 1)
     && i.SnappedTo.y == y)
     .ToList();

      return adjustingOnX;
    }

    protected virtual void OnEnable()
    {
      LeanTouch.OnFingerTap += OnFingerTap;
      LeanTouch.OnFingerSwipe += OnFingerSwipe;
      LeanTouch.OnPinch += OnPinch;
    }

    protected virtual void OnDisable()
    {
      LeanTouch.OnFingerTap -= OnFingerTap;
      LeanTouch.OnPinch -= OnPinch;
      LeanTouch.OnFingerSwipe -= OnFingerSwipe;
    }

    void OnPinch(float pinch)
    {
      Debug.Log("OnPinch DeltaScreenPosition = " + pinch);
      if (pinch < 1)
        InGamePanel.Instance.OnZoomOut();
      else
        InGamePanel.Instance.OnZoomIn();
    }

    void OnFingerSwipe(LeanFinger finger)
    {
      var fc = LeanTouch.Fingers.Count;
      if (fc > 1)
        return;
      var swipe = finger.SwipeDelta;

      var pos = Camera.main.transform.position;
      var move = new Vector3(-1 * swipe.x / 50, -1 * swipe.y / 50, 0);
      pos = pos.MoveBy(move);
      Camera.main.transform.position = pos;
    }

    bool IsMyTurnInNetworkGame()
    {
      return GamePlayService.CanPlay() && grid.MatchEntity.DeserializeCount > 0;
    }
    public LayerMask LayerMask = UnityEngine.Physics.DefaultRaycastLayers;
    void OnFingerTap(LeanFinger finger)
    {
      if (GameLasting.TotalSeconds < 0.5f)
      {
        Debug.Log("GameLasting.TotalSeconds < 0.3f = " + GameLasting.TotalSeconds);
        return;//accident?
      }
      if (Forms.TrSinceClosed.TotalSeconds < 0.4f)
      {
        Debug.Log("Forms.TrSinceClosed.TotalSeconds < 0.4f = " + Forms.TrSinceClosed.TotalSeconds);
        return;//accident?
      }

      //RaycastHit2D[] hit = null;
      //Physics2D.Raycast(transform.position, -Vector2.up, LayerMask, hit);
      //if (hit.collider != null)
      //{
      //  int k = 0;
      //}
      //Debug.Log ("OnFingerTap");
      PointerEventData pointerData = new PointerEventData(EventSystem.current)
      {
        pointerId = -1,
      };
      List<RaycastResult> results = new List<RaycastResult>();
      EventSystem.current.RaycastAll(pointerData, results);

      bool mustReturn = false;
      if (IsGameOver())
        return;
      if (Forms.Instance.AnyFormShown())
        mustReturn = true;
      else if (Entity.moventIsLasting)
        mustReturn = true;//let one finish move
      else if (grid.waitingForRecalcPoints)
        mustReturn = true;
      if (!mustReturn && GameSettings.Instance.Gametype == GameType.TwoPlayersPlayServices)
      {
        if (!IsMyTurnInNetworkGame())
          mustReturn = true;
      }
      Item item = null;
      if (!mustReturn)
      {
        var worldPoint = Camera.main.ScreenToWorldPoint(finger.ScreenPosition);
        Vector2 worldPoint2D = new Vector2(worldPoint.x, worldPoint.y);
        item = grid.getItemAt(worldPoint);
        if (item == null)
          return;//do not beep as panel item could be tapped, so no problem
        else if (getEntityAt(item.x, item.y))
          mustReturn = true;
      }
      if (mustReturn)
      {
        PlayBeep();
        return;
      }

      suggestedMove = GameDefines.InvalidPoint;
      var player = grid.GetPlayerGameForMove();

      grid.AddElem(player, item.x, item.y);
      grid.OnMoveDone(player);

      if (!gameOver && GameSettings.Instance.Gametype == GameType.TwoPlayersPlayServices)
      {
        GamePlayService.OnPlayTurn();
      }

      MakeCompMoveIfNeeded();
    }

    void MakeCompMoveIfNeeded()
    {
      if (grid.ComputerMove())
      {
        grid.MakeDeviceMove();
      }

    }

    bool IsItemBusy(Item item)
    {
      return Entities.Any(i => i.SnappedTo == item);
    }

    public void PlayBeep()
    {
      AudioService.Instance.PlaySound("beep");
    }

    public void SuggestMove()
    {
      try
      {
        bool canGo = !IsGameOver();
        if (!canGo) { PlayBeep(); return; }

        var hint = GetSuggestMoveHintInfo();
        if (!hint.CanUse())
        {
          PlayBeep();
          return;
        }

        if (GameSettings.Instance.Gametype == GameType.OnePlayer)
        {
          if (!grid.ComputerMove())
          {
            PointWeight bestOpponent;
            grid.RecalcPoints();
            var sm = grid.pcPlayer.GetBestHumanMove(out bestOpponent);
            if (grid.MakePlayerMoveToPoint(grid.humanGame, sm.X > 0 ? sm : bestOpponent.point))
              hint.Current--;
          }
        }

        MakeCompMoveIfNeeded();
      }
      catch (Exception exc)
      {
        Log.AddError(exc);
      }
    }

    public MaxSpecialHint GetSuggestMoveHintInfo()
    {
      return GameSettings.Instance.DifficultyLevel == GameSettings.Difficulty.Normal ? maxAdviceMoveNormal : maxAdviceMoveEasy;
    }

    internal void OnGameEnd(PlayerGame wininingPlayer, PlayerGame humanGame, LineCheckResult lineCheckResult)
    {
      Debug.Log("OnGameEnd starts");
      gameOver = true;

      if (GameSettings.Instance.Gametype == GameType.TwoPlayersPlayServices)
      {
        FinishNetworkGame(wininingPlayer);
      }
      CreateWinInfo(wininingPlayer);

      var res = lineCheckResult;
      var stPoint = res.elems[res.patternIndex].elem.Sprite.SnappedTo;
      var endPoint = res.elems[res.patternIndex + 4].elem.Sprite.SnappedTo;
      var pt = GetEndGameStartItem(stPoint, endPoint, lineCheckResult.lineType);
      var x = pt.x;
      var y = pt.y;
      grid.ShowCrossLine(grid.entitiesBuffer[y, x].SnappedTo, GetCrossLineRotation(lineCheckResult), lineCheckResult.lineType);
      //AudioService.Instance.PlaySound("game_over");
      Forms.Instance.GameOver(winInfo, humanGame, wininingPlayer);
    }

    internal WinInfo CreateWinInfo(PlayerGame wininingPlayer)
    {
      winInfo = new WinInfo(wininingPlayer.typeOfElem == GridElemType.Cross, GameLasting.GetTotalTime(), grid.GetNumberOfElems());
      winInfo.Time = (float)GameLasting.TotalSeconds;
      return winInfo;
    }

    void FinishNetworkGame(PlayerGame wininingPlayer)
    {
      GamePlayService.FinishMatch(wininingPlayer.typeOfElem);
    }

    Item GetEndGameStartItem(Item i1, Item i2, LineType type)
    {
      if (type == LineType.w9 || type == LineType.w10_30)
      {
        return i1.x < i2.x ? i1 : i2;
      }
      else if (type == LineType.w13_30)
      {
        return i1.x > i2.x ? i1 : i2; ;

      }
      return i1.y < i2.y ? i1 : i2; //12 ?
    }

    int GetCrossLineRotation(LineCheckResult winInfo)
    {
      Debug.Log("GetCrossLineRotation winInfo.lineType = " + winInfo.lineType);
      if (winInfo.lineType == LineType.w9)
      {
        return 90;
      }
      else if (winInfo.lineType == LineType.w10_30)
      {
        return 45;
      }

      else if (winInfo.lineType == LineType.w13_30)
      {
        return -45;

      }
      return 0;
    }

    bool gameOver;
    public bool IsGameOver()
    {
      return gameOver;
    }

    public Entity CreateEntity(int x, int y, bool cross)
    {
      var gridItem = grid.itemsBuffer[y, x];
      return CreateEntity(gridItem, cross);
    }

    private Entity CreateEntity(Item gridItem, bool cross)
    {
      GameObject entity = null;
      Entity entityScript = null;
      try
      {
        entity = Factory.Instantiate(cross ? crossPrefab : oPrefab, Vector3.zero, 0);
        entity.transform.position = gridItem.transform.position;
        entity.transform.MoveTo(gridItem.transform.position.x, gridItem.transform.position.y, -10);//HACK +1, -1

        entityScript = entity.GetComponent<Entity>();
        entityScript.SnappedTo = gridItem;
        Entities.Add(entityScript);
      }
      catch (Exception ex)
      {
        Debug.LogError(ex);
        Debug.LogError("entity = " + entity + " entityScript = " + entityScript);
      }

      return entityScript;
    }

    //TimeTracker tr = new TimeTracker();
    // Update is called once per frame
    void Update()
    {
      GameLasting.Update();

      if (Input.GetKeyUp(KeyCode.T))
      {
        if (Forms.Instance.enterNameForm.Visible)
          return;
        //Simple.RunAll();
        Advanced.RunAll();
      }
      if (GameSettings.Instance.Gametype == GameType.TwoPlayersPlayServices)
      {
        if (!IsMyTurnInNetworkGame())
        {
          grid.SetBackgroundColor(false);
        }
        else
          grid.SetBackgroundColor(true);
      }
    }
  }

}